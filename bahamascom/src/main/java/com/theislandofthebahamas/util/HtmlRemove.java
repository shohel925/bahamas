package com.theislandofthebahamas.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlRemove {
	public static String getPerticularHtmlValue(String html, String start,
			String end) {
		String result="";
		final Pattern ptrn = Pattern.compile(start + "(.+?)" + end);
		final Matcher mtchr = ptrn.matcher(html);
		while (mtchr.find()) {
			  result = html.substring(mtchr.start(), mtchr.end());
		}
		result=html.replace(result, "") ;
		return result;
	}
}
