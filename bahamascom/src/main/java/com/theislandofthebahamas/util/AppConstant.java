package com.theislandofthebahamas.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;
import com.theislandofthebahamas.datamodel.FeaturePropertyInfo;
import com.theislandofthebahamas.datamodel.GeneralActivityIslandInfo;
import com.theislandofthebahamas.datamodel.GeneralActivityTypesOfActivityInfo;
import com.theislandofthebahamas.datamodel.GeneralActvityThinksToKnowInfo;
import com.theislandofthebahamas.datamodel.GeneralEventInfo;
import com.theislandofthebahamas.datamodel.GeneralHotelInfo;
import com.theislandofthebahamas.datamodel.IslandList;
import com.theislandofthebahamas.datamodel.TempIslandInfo;
import com.theislandofthebahamas.response.GeneralActivityList;

public class AppConstant {
	
	public  static String getDaysFromDateAndTime(String times){
		long time =Long.parseLong(times);
			time = 1367832568 * (long) 1000;
		    Date date = new Date(time);
		    SimpleDateFormat format = new SimpleDateFormat("EEEE");
		    format.setTimeZone(TimeZone.getTimeZone("GMT"));
		    String result=format.format(date);
		    return result;
	}
	public static long checkHours(String startdate,String endDate){
		Date previousDate = new Date ();
		previousDate.setTime((Long.parseLong(startdate))*1000);

		Date currentDate = new Date ();
		currentDate .setTime((long)(Long.parseLong(endDate))*1000);

		//To calculate the days difference between two dates 
		long diffInhours = (long)( (currentDate.getTime() - previousDate.getTime()) 
		                 / (1000 * 60 * 60));
		 return diffInhours;
	}
	public static String dateConvert(String times){
		long time =Long.parseLong(times);
			time = time * (long) 1000;
		    Date date = new Date(time);
		    SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd");
		    format.setTimeZone(TimeZone.getTimeZone("GMT"));
		    String result=format.format(date);
		    return result;
	}
	
	public static String dateConvertWithYear(String times){
		long time =Long.parseLong(times);
			time = time * (long) 1000;
		    Date date = new Date(time);
		    SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd yyyy");
		    format.setTimeZone(TimeZone.getTimeZone("GMT"));
		    String result=format.format(date);
		    return result;
	}
	//=========== Convert Array to String ===============
	public static String  strSeparator = ",";
	public static String convertArrayToString(List<String> array){
	    String str = "";
	    for (int i = 0;i<array.size(); i++) {
	        str = str+array.get(i);
	        // Do not append comma at the end of last element
	        if(i<array.size()-1){
	            str = str+strSeparator;
	        }
	    }
	    return str;
	}

	//Constants for Intent data passing
	public static final String SELECTED_EVENT_ID = "SelectedEventId";
	public static final String SELECTED_MORE_INFO_ID = "SelectedMoreInfoId";
	public static final String SELECTED_HOTEL_ID = "SelectedHotelId";
	public static final String SELECTED_GENERAL_ACTIVITY_ID = "SelectedGeneralActivityId";
	public static String firstHead = "firstHead";
	public static String previousTime = "previousTime";
	public static String Email = "testappuser3";
	public static String Password = "11111";
	public static String transportFirstHeat = "transportFirstHeat";

	public static String Token = "9cfef61d13e679d41acdafbc494f3dbe";
	public static String tempIsland = "";
	public static String islandName = "";
	public static String islandColor = "";
	public static GeneralActivityList mGeneralActivityList=null;
//	public static String activityId="";
	public	static Vector<GeneralActivityTypesOfActivityInfo>typeOfActivityList=null;
	public static Vector<GeneralActvityThinksToKnowInfo>generalThinksToKnow=null;
	public static GeneralHotelInfo mGeneralHotelInfo=null;
	public static GeneralEventInfo mGeneralEventInfo=null;
	public static String tempData="";
	public static String about_us="about_us";
	public static String government="government";
	public static String history="history";
	public static String holidays="holidays";
	public static String local_customs="local_customs";
	public static String language="language";
	public static String people="people";
	public static String proximity="proximity";
	public static String travel_tips="travel_tips";
	public static String more_discription="";
	public static String islandSerialNo="islandSerialNo";
	public static String isHotelHead="isHotelHead";
	public static String directoryFirstHeat="directoryFirstHeat";
	
	public static String islandID="islandID";
	public static String islandOrDirectoryId="";
	public static String objectType="";
	public static DivingDirectoryInfo mDivingDirectoryInfo=null;
	public static GeneralActivityIslandInfo mGeneralActivityIslandInfo;
	
	//=========== add island image ===================
	public static List<TempIslandInfo>tempIslandList(){
		List<TempIslandInfo> tempList=new ArrayList<TempIslandInfo>();
		if(tempList!=null){
			tempList.clear();
		}
		TempIslandInfo tmp=new TempIslandInfo();
		tmp.setId("16");
		tmp.setColor("#EEB040");
		tmp.setName("The Abacos");
		tmp.setImage(R.drawable.a_16);
		tempList.add(tmp);
		
		TempIslandInfo tmp2=new TempIslandInfo();
		tmp2.setId("25");
		tmp2.setColor("#4F95B8");
		tmp2.setName("Acklins & Crooked Island");
		tmp2.setImage(R.drawable.a_25);
		tempList.add(tmp2);
		
		TempIslandInfo tmp3=new TempIslandInfo();
		tmp3.setId("3");
		tmp3.setColor("#FFCF01");
		tmp3.setName("Andros");
		tmp3.setImage(R.drawable.a_3);
		tempList.add(tmp3);
		
		TempIslandInfo tmp4=new TempIslandInfo();
		tmp4.setId("17");
		tmp4.setColor("#EAA8C1");
		tmp4.setName("The Berry Islands");
		tmp4.setImage(R.drawable.a_17);
		tempList.add(tmp4);
		
		TempIslandInfo tmp5=new TempIslandInfo();
		tmp5.setId("1");
		tmp5.setColor("#49A942");
		tmp5.setName("Bimini");
		tmp5.setImage(R.drawable.a_1);
		tempList.add(tmp5);
		
		TempIslandInfo tmp6=new TempIslandInfo();
		tmp6.setId("19");
		tmp6.setColor("#0194D3");
		tmp6.setName("Cat Island");
		tmp6.setImage(R.drawable.a_19);
		tempList.add(tmp6);
		
		TempIslandInfo tmp7=new TempIslandInfo();
		tmp7.setId("20");
		tmp7.setColor("#739DD2");
		tmp7.setName("Eleuthera & Harbour Island");
		tmp7.setImage(R.drawable.a_20);
		tempList.add(tmp7);
		
		TempIslandInfo tmp8=new TempIslandInfo();
		tmp8.setId("22");
		tmp8.setColor("#D15980");
		tmp8.setName("The Exumas");
		tmp8.setImage(R.drawable.a_22);
		tempList.add(tmp8);
		
		TempIslandInfo tmp9=new TempIslandInfo();
		tmp9.setId("18");
		tmp9.setColor("#F58025");
		tmp9.setName("Grand Bahama Island");
		tmp9.setImage(R.drawable.a_18);
		tempList.add(tmp9);
		
		TempIslandInfo tmp10=new TempIslandInfo();
		tmp10.setId("28");
		tmp10.setColor("#66B360");
		tmp10.setName("Inagua");
		tmp10.setImage(R.drawable.a_28);
		tempList.add(tmp10);
		
		
		TempIslandInfo tmp11=new TempIslandInfo();
		tmp11.setId("23");
		tmp11.setColor("#2CAFA4");
		tmp11.setName("Long Island");
		tmp11.setImage(R.drawable.a_23);
		tempList.add(tmp11);
		
		
		TempIslandInfo tmp12=new TempIslandInfo();
		tmp12.setId("27");
		tmp12.setColor("#BB8D0B");
		tmp12.setName("Mayaguana");
		tmp12.setImage(R.drawable.a_27);
		tempList.add(tmp12);
		
		
		TempIslandInfo tmp13=new TempIslandInfo();
		tmp13.setId("2");
		tmp13.setColor("#EE3524");
		tmp13.setName("Nassau & Paradise Island");
		tmp13.setImage(R.drawable.a_2);
		tempList.add(tmp13);
		
		TempIslandInfo tmp14=new TempIslandInfo();
		tmp14.setId("30");
		tmp14.setColor("#BC73B0");
		tmp14.setName("Ragged Island");
		tmp14.setImage(R.drawable.a_30);
		tempList.add(tmp14);
		
		
		TempIslandInfo tmp15=new TempIslandInfo();
		tmp15.setId("24");
		tmp15.setColor("#7874B6");
		tmp15.setName("Rum Cay");
		tmp15.setImage(R.drawable.a_24);
		tempList.add(tmp15);
		
		TempIslandInfo tmp16=new TempIslandInfo();
		tmp16.setId("31");
		tmp16.setColor("#026CB6");
		tmp16.setName("San Salvador");
		tmp16.setImage(R.drawable.a_31);
		tempList.add(tmp16);
		return tempList;
	}
	
	public static final int IMAGE_SLIDE_ANIMATION_SPEED = 4;
	public static IslandList mIslandList=null;
}
