package com.theislandofthebahamas.util;

import com.aapbd.utils.network.UrlUtils;

public class BaseURL {
	
	public static String HTTP = "http://www.bahamas.com/api/bahamas/";


	/**
	 * @return the hTTP
	 */
	public static String makeHTTPURL(String param) {
		return BaseURL.HTTP + UrlUtils.encode(param);
	}

	/**
	 * @param hTTP
	 *            the hTTP to set
	 */
	public static void setHTTP(final String hTTP) {
		BaseURL.HTTP = hTTP;
	}

	public static final String BASE_URL_STATIC_MAP = "https://maps.googleapis.com/maps/api/staticmap?size=800x600&zoom=13&markers=color:blue%7C";
}
