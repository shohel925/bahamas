package com.theislandofthebahamas;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.theislandofthebahamas.util.AppConstant;


public class WebActivity extends Activity implements OnClickListener {

	Context con;
	WebView webview;
	ProgressBar progress;

	LinearLayout webBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.web);

		con = this;

		initUi();
	}

	private void initUi() {
		// TODO Auto-generated method stub
		webBack = (LinearLayout) findViewById(R.id.webBack);
		webview = (WebView) findViewById(R.id.webview);
		progress = (ProgressBar) findViewById(R.id.progress);
		webBack.setOnClickListener(this);

		getWebView(AppConstant.more_discription);

		// //---- For Profile Info-------------------
	}


	@SuppressLint("NewApi")
	public void getWebView(String url) {

		final CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);
		CookieSyncManager.createInstance(this);

		// homebtn=(Button)findViewById(R.id.homebtn);
		webview = (WebView) findViewById(R.id.webview);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.getSettings().setDefaultZoom(ZoomDensity.MEDIUM);
		webview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
		webview.getSettings().setDomStorageEnabled(true);
		 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			 webview.getSettings().setLayoutAlgorithm(LayoutAlgorithm.TEXT_AUTOSIZING);
		    } else {
		    	webview.getSettings().setLayoutAlgorithm(LayoutAlgorithm.NORMAL);
		    }

		 webview.loadUrl("file:///android_asset/raw/"+AppConstant.more_discription);

		webview.setWebViewClient(new WebViewClient() {

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				progress.setVisibility(View.VISIBLE);
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				progress.setVisibility(View.INVISIBLE);

				AppConstant.more_discription = url;

				Log.d("Finish URL  ", ": " + url);

			}

			/*
			 * (non-Javadoc)
			 *
			 * @see
			 * android.webkit.WebViewClient#shouldOverrideUrlLoading(android
			 * .webkit.WebView, java.lang.String)
			 */
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				CookieSyncManager.getInstance().sync();

				AppConstant.more_discription = url;

				Log.d("WebApp", "URL is:" + url);
				// Toast.makeText(con, "URL : "+url, 2000).show();
				// homebtn.setBackgroundResource(R.drawable.backbtn);

				if (url.startsWith("tel:")) {

					Log.d("Where : ", "Tel");

					final Intent intent = new Intent(Intent.ACTION_DIAL, Uri
							.parse(url));
					startActivity(intent);
					return true;
				}

				view.loadUrl(url);
				return true;
			}

		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.getId() == R.id.webBack) {
			finish();
		}

	}

}
