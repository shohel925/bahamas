package com.theislandofthebahamas.response;

import com.theislandofthebahamas.datamodel.MoreInfo;

public class GeneralMoreRespone {
	String status="";
	String message="";
	MoreInfo result;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public MoreInfo getResult() {
		return result;
	}
	public void setResult(MoreInfo result) {
		this.result = result;
	}
	
	

}
