package com.theislandofthebahamas.response;

public class WeatherInfo {
	String acid="";
	String aid="";
	String created="";
	String phrase="";
	String temp_f="";
	String temp_c="";
	String aptemp_f="";
	String aptemp_c="";
	String wndchl_f="";
	String wndchl_c="";
	String rhumid="";
	String wind_dir="";
	String windspeed="";
	String pres="";
	String vis="";
	String icon="";
	String location="";
	String tid="";
	String featured="";
	String city="";
	String island="";
	String weight="";
	String url="";
	String machine_name="";
	String image="";
	public String getAcid() {
		return acid;
	}
	public void setAcid(String acid) {
		this.acid = acid;
	}
	public String getAid() {
		return aid;
	}
	public void setAid(String aid) {
		this.aid = aid;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getPhrase() {
		return phrase;
	}
	public void setPhrase(String phrase) {
		this.phrase = phrase;
	}
	public String getTemp_f() {
		return temp_f;
	}
	public void setTemp_f(String temp_f) {
		this.temp_f = temp_f;
	}
	public String getTemp_c() {
		return temp_c;
	}
	public void setTemp_c(String temp_c) {
		this.temp_c = temp_c;
	}
	public String getAptemp_f() {
		return aptemp_f;
	}
	public void setAptemp_f(String aptemp_f) {
		this.aptemp_f = aptemp_f;
	}
	public String getAptemp_c() {
		return aptemp_c;
	}
	public void setAptemp_c(String aptemp_c) {
		this.aptemp_c = aptemp_c;
	}
	public String getWndchl_f() {
		return wndchl_f;
	}
	public void setWndchl_f(String wndchl_f) {
		this.wndchl_f = wndchl_f;
	}
	public String getWndchl_c() {
		return wndchl_c;
	}
	public void setWndchl_c(String wndchl_c) {
		this.wndchl_c = wndchl_c;
	}
	public String getRhumid() {
		return rhumid;
	}
	public void setRhumid(String rhumid) {
		this.rhumid = rhumid;
	}
	public String getWind_dir() {
		return wind_dir;
	}
	public void setWind_dir(String wind_dir) {
		this.wind_dir = wind_dir;
	}
	public String getWindspeed() {
		return windspeed;
	}
	public void setWindspeed(String windspeed) {
		this.windspeed = windspeed;
	}
	public String getPres() {
		return pres;
	}
	public void setPres(String pres) {
		this.pres = pres;
	}
	public String getVis() {
		return vis;
	}
	public void setVis(String vis) {
		this.vis = vis;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getFeatured() {
		return featured;
	}
	public void setFeatured(String featured) {
		this.featured = featured;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getIsland() {
		return island;
	}
	public void setIsland(String island) {
		this.island = island;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMachine_name() {
		return machine_name;
	}
	public void setMachine_name(String machine_name) {
		this.machine_name = machine_name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	

}
