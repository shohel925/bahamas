package com.theislandofthebahamas.response;

public class WeatherResponse {
	String status="";
	String message="";
	WeatherResult result=new WeatherResult();
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public WeatherResult getResult() {
		return result;
	}
	public void setResult(WeatherResult result) {
		this.result = result;
	}
	
	

}
