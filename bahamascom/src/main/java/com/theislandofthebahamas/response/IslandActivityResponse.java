package com.theislandofthebahamas.response;

import java.util.List;

import com.theislandofthebahamas.datamodel.IslandActivityInfo;

public class IslandActivityResponse {
	String status = "";
	String message = "";
	List<IslandActivityInfo> result;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<IslandActivityInfo> getResult() {
		return result;
	}

	public void setResult(List<IslandActivityInfo> result) {
		this.result = result;
	}

}
