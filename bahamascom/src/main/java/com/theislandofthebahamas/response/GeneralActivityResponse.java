package com.theislandofthebahamas.response;

import java.util.List;

public class GeneralActivityResponse {
	String status = "";
	String message = "";
	List<GeneralActivityList> result;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<GeneralActivityList> getResult() {
		return result;
	}

	public void setResult(List<GeneralActivityList> result) {
		this.result = result;
	}

}
