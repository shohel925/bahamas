package com.theislandofthebahamas.response;

import java.util.List;

import com.theislandofthebahamas.datamodel.FeaturePropertyInfo;

public class FeaturePropertyResponse {
	private String status="";
	private String message="";
	
	List<FeaturePropertyInfo> result;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<FeaturePropertyInfo> getResult() {
		return result;
	}

	public void setResult(List<FeaturePropertyInfo> result) {
		this.result = result;
	}
	
	
	

}
