package com.theislandofthebahamas.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.theislandofthebahamas.datamodel.SubcategoryInfo;

public class CategoryInfo {
	@SerializedName("id")
	 String id="";
	@SerializedName("name")
	 String name="";
	@SerializedName("subcategorylist")
	List<SubcategoryInfo> subcategorylist;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<SubcategoryInfo> getSubcategorylist() {
		return subcategorylist;
	}
	public void setSubcategorylist(List<SubcategoryInfo> subcategorylist) {
		this.subcategorylist = subcategorylist;
	}
	
	
	

}
