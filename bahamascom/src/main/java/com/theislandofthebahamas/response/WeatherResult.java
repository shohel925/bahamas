package com.theislandofthebahamas.response;

import java.util.List;

public class WeatherResult {
	List<WeatherInfo>weather;

	public List<WeatherInfo> getWeather() {
		return weather;
	}

	public void setWeather(List<WeatherInfo> weather) {
		this.weather = weather;
	}
	

}
