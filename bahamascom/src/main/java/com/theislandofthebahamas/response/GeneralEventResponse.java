package com.theislandofthebahamas.response;

import java.util.List;

import com.theislandofthebahamas.datamodel.GeneralEventInfo;

public class GeneralEventResponse {
	String status = "";
	String message = "";
	List<GeneralEventInfo> result;

	public List<GeneralEventInfo> getResult() {
		return result;
	}

	public void setResult(List<GeneralEventInfo> result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
