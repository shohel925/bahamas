package com.theislandofthebahamas.response;

import java.util.List;

import com.theislandofthebahamas.datamodel.IslandList;

public class IslandListRespone {
	String status="";
	String message="";
	List<IslandList> result;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<IslandList> getResult() {
		return result;
	}
	public void setResult(List<IslandList> result) {
		this.result = result;
	}
	
	

}
