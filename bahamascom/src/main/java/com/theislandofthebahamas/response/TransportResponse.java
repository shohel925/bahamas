
package com.theislandofthebahamas.response;

import java.util.List;

import com.theislandofthebahamas.datamodel.TransportResult;



public class TransportResponse {
	
	private String status;
	private String message;
	
	private TransportResult result;

	public TransportResult getResult() {
		return result;
	}

	public void setResult(TransportResult result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	

	
	
	

}
