package com.theislandofthebahamas;

import java.util.ArrayList;

import com.aapbd.utils.nagivation.StartActivity;
import com.theislandofthebahamas.datamodel.TransportDetailsDataModel;
import com.theislandofthebahamas.datamodel.TransportTableData;
import com.theislandofthebahamas.util.ParseFromString;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TransportDetails extends Activity {
	TransportTableData data; 
	Context con, context;
	private ImageButton btBack;
	ArrayList <TransportDetailsDataModel> datalist ;
	Typeface tf ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_transport_details);
		con = context = this;
		tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		data = (TransportTableData) getIntent().getSerializableExtra("TransportTableData");
		String transportType = data.getTransport_info();
		
		btBack =  (ImageButton) findViewById(R.id.imgBt_back_transport_detail);
		
		TextView actionBarName = (TextView) findViewById(R.id.tv_appName_transport_detail);
		actionBarName.setText(data.getTransport_type().toUpperCase());
		actionBarName.setTypeface(tf);
		//Parsing Html string to object
		ParseFromString parseString = new ParseFromString(transportType);
		datalist = (ArrayList<TransportDetailsDataModel>) parseString.getParseHtmlData();
		
		ListView lvTranportDetails = (ListView) findViewById(R.id.lv_transportDetails);
		lvTranportDetails.setAdapter(new TransportDetailAdapter());
		
		
		
	}

	public void btBackOnClick(View v) {
		this.finish();
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.transport_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	private class TransportDetailAdapter extends BaseAdapter{

		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return datalist.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return datalist.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_transport_details_item, parent, false);
			}  
			
			//all layout initialization
			//layouts will be visible depends on data
			// if data exists than set to visible
			//otherwise visibility gone
			LinearLayout layAddress = (LinearLayout) convertView.findViewById(R.id.address);
			LinearLayout layPhone = (LinearLayout) convertView.findViewById(R.id.phone);
			LinearLayout layFax = (LinearLayout) convertView.findViewById(R.id.fax);
			LinearLayout layEmail = (LinearLayout) convertView.findViewById(R.id.email);
			LinearLayout layWebsite = (LinearLayout) convertView.findViewById(R.id.website);
			
			
			
			//all title initialization
			//all TextView are initializing to set a custom font on text
			TextView txtTitle = (TextView) convertView.findViewById(R.id.transport_detail_row_iteamName);
			TextView txtaddresTitle= (TextView) convertView.findViewById(R.id.address_title);
			TextView txtPhoneTitle= (TextView) convertView.findViewById(R.id.phone_title);
			TextView txtFaxTitle= (TextView) convertView.findViewById(R.id.fax_title);
			TextView txtEmailTitle= (TextView) convertView.findViewById(R.id.email_title);
			TextView txtWebsiteTitle= (TextView) convertView.findViewById(R.id.website_title);
			
			txtTitle.setTypeface(tf, Typeface.BOLD);
			txtaddresTitle.setTypeface(tf, Typeface.BOLD);
			txtaddresTitle.setTypeface(tf, Typeface.BOLD);
			txtPhoneTitle.setTypeface(tf, Typeface.BOLD);
			txtFaxTitle.setTypeface(tf, Typeface.BOLD);
			txtEmailTitle.setTypeface(tf, Typeface.BOLD);
			txtWebsiteTitle.setTypeface(tf, Typeface.BOLD);
			
			//text content view initialization
			TextView txtaddresContent = (TextView) convertView.findViewById(R.id.address_name);
			final TextView txtPhoneContent = (TextView) convertView.findViewById(R.id.phone_number);
			TextView txtFaxContent = (TextView) convertView.findViewById(R.id.fax_number);
			final TextView txtEmailContent = (TextView) convertView.findViewById(R.id.email_address);
			final TextView txtWebsiteContent = (TextView) convertView.findViewById(R.id.website_address);
			
			txtaddresContent.setTypeface(tf);
			txtPhoneContent.setTypeface(tf);
			txtFaxContent.setTypeface(tf);
			txtEmailContent.setTypeface(tf);
			txtWebsiteContent.setTypeface(tf);
			
			
			//Getting model data for row
			TransportDetailsDataModel datamodel =datalist.get(position);
			
			
			txtTitle.setText(datamodel.getName());
			
			
			//setting visible or gone to layout 
			//and setting the data to text view
			if(!datamodel.getAddress().equals(""))
			{
				txtaddresContent.setText(datamodel.getAddress());
			}
			else
				layAddress.setVisibility(View.GONE);
			
			
			//phone layout
			if(!datamodel.getPhone().equals(""))
			{
				//getting the string value
				//and if the string have multiple values 
				//than enter a new line in every ","
				//to show a new line
				String temp = datamodel.getPhone();
				temp = temp.replace(",", "\n");
				txtPhoneContent.setText(temp);
			}
			else
				layPhone.setVisibility(View.GONE);
			
			
			
			//Fax layout
			if(!datamodel.getFax().equals(""))
			{
				txtFaxContent.setText(datamodel.getFax());
			}
			else
				layFax.setVisibility(View.GONE);
			
			
			//Email layout
			if(!datamodel.getEmail().equals(""))
			{
				txtEmailContent.setText(datamodel.getEmail());
			}
			else
				layEmail.setVisibility(View.GONE);
			
			
			//WebSite layout
			if(!datamodel.getWebsite().equals(""))
			{
				txtWebsiteContent.setText(datamodel.getWebsite());
			}
			else
				layWebsite.setVisibility(View.GONE);
			
	
			//Setting on click listener of listview item(Website)
			layWebsite.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//StartActivity.toebsite(con, (String) txtWebsiteContent.getText());
					String url = (String) txtWebsiteContent.getText();
					//adding http:// or https://
					if (!url.startsWith("https://") && !url.startsWith("http://")){
					    url = "http://" + url;
					}
					
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(url));
					startActivity(i);
				}
			});

			
			//Setting on click listener of listview item(Email)
			layEmail.setOnClickListener(new OnClickListener() {
							
				@Override
				public void onClick(View v) {
					
					//StartActivity.toMail(context, txtEmailContent.getText().toString() , " ", " ");
			
					final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
					emailIntent.setType("plain/text");
					emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{txtEmailContent.getText().toString()});
					startActivity(emailIntent);
					
				}
			});
			
			//Setting on click listener of listview item(Phone)
			layPhone.setOnClickListener(new OnClickListener() {
							
				@Override
				public void onClick(View v) {
					

					 String phoneNo = (String) txtPhoneContent.getText();
					 
					 //Getting one phone number from multiple numbers
					 if(phoneNo.contains("\n"))
						 phoneNo = phoneNo.substring(1, phoneNo.indexOf("\n"));
					
					 String uri = "tel:" + phoneNo.trim() ;
					 
					 
					 Intent intent = new Intent(Intent.ACTION_DIAL);
					 intent.setData(Uri.parse(uri));
					 startActivity(intent);
			
					
				}
			});

			
			
			return convertView;
		}
		
	}
}
