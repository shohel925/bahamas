package com.theislandofthebahamas;

import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;

import com.aapbd.utils.encryption.KeyHashManager;
import com.aapbd.utils.network.AAPBDHttpClient;
import com.aapbd.utils.network.NetInfo;
import com.aapbd.utils.notification.AlertMessage;
import com.aapbd.utils.notification.BusyDialog;
import com.aapbd.utils.storage.PersistData;
import com.google.gson.Gson;
import com.theislandofthebahamas.datamodel.IslandList;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.GeneralActivityList;
import com.theislandofthebahamas.response.GeneralActivityResponse;
import com.theislandofthebahamas.response.IslandListRespone;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AppConstant;

public class SplashActivity extends Activity {

	Handler handler = new Handler();
	Context con;
	IslandListRespone mIslandListRespone;
	GeneralActivityResponse mGeneralActivityResponse;
	DatabaseHandler db;
	BusyDialog busyNow;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		con=this;
		db=new DatabaseHandler(con);
		KeyHashManager.getKeyHash(con);
		final String time = "" + System.currentTimeMillis();
		
		if (!PersistData.getStringData(con, AppConstant.firstHead).equalsIgnoreCase("Yes")) {
			getIslandData(AllURL.getAllIslandUrl());
			PersistData.setStringData(con, AppConstant.previousTime, time);

		} else {
			final long timecurrent = Long.parseLong(time);
			final long previousTime = Long.parseLong(PersistData.getStringData(con, AppConstant.previousTime));

			if ((timecurrent - previousTime) > (30L*24L*60L*60L*1000L)) {
				if (NetInfo.isOnline(con)) {
						PersistData.setStringData(con, AppConstant.previousTime, time);
						getIslandData(AllURL.getAllIslandUrl());
					}else{
						
						handler.postDelayed(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								Intent intent = new Intent(con, MainActivity.class);
								startActivity(intent);
								finish();
							}
						}, 2000);
						
					}
				

			}else{
				
				handler.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						Intent intent = new Intent(con, MainActivity.class);
						startActivity(intent);
						finish();
					}
				}, 2000);
				
			}
		}
		
		
	}
	
	private void getIslandData(final String url) {
		// TODO Auto-generated method stub
		 if (!NetInfo.isOnline(con)) {
				AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
					return;
				}
		 
		 	Log.e("URL : ", url);

	        busyNow = new BusyDialog(con, true,false);
	        busyNow.show();

		        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
		        	
		        	String response="";
					
					@Override
					public void run() {
						try {
							response=AAPBDHttpClient.post(url).body();
						}
						catch (Exception e) {
							// TODO: handle exception
							 Log.e("MYAPP", "exception", e);
							 if(busyNow!=null){
								 busyNow.dismis();
							 }
						}

						runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								if(busyNow!=null){
									 busyNow.dismis();
								 }
								try {
				                    Log.e("Response", ">>" + new String(response));
				                    if (!TextUtils.isEmpty(new String(response))) {
				    					Gson g = new Gson();
				    					mIslandListRespone = g.fromJson(new String(response),IslandListRespone.class);
				    					
				    					if (mIslandListRespone.getStatus().equalsIgnoreCase("true")) {
											
											int islandNo=1;
											for(IslandList ilist:mIslandListRespone.getResult()){
												if(!db.ifIslandExist(ilist.getId())){
													ilist.setIslandNo(islandNo);
													db.addIsland(ilist);
													for(int i=0; i<ilist.getDiscover_more_images().size(); i++){
														db.addIslandImage(ilist.getId(), ilist.getDiscover_more_images().get(i), (i+1), "discover_more_images");
														
													}
													for(int i=0; i<ilist.getIsland_details().size(); i++){
														ilist.getIsland_details().get(i).setIslandId(ilist.getId());
														ilist.getIsland_details().get(i).setSerialid(""+(i+1));
														db.addIslandDetails(ilist.getIsland_details().get(i));
														
													}
												}
												islandNo++;
											}
											
											getGeneralActivityData(AllURL.getGeneralActivityUrl());
											
										} else {
											AlertMessage.showMessage(con, getString(R.string.app_name), mIslandListRespone.getMessage() + "");

										}
				                    }


				                } catch (final Exception e) {

				                    e.printStackTrace();
				                }
								
								
							}
						});
					}
				});
		
		
	}
	
	 
	 private void getGeneralActivityData(final String url) {
			// TODO Auto-generated method stub
			 if (!NetInfo.isOnline(con)) {
					AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
						return;
					}
			 
			 	Log.e("URL : ", url);

		        busyNow = new BusyDialog(con, true,false);
		        busyNow.show();

			        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
			        	
			        	String response="";
						
						@Override
						public void run() {
							try {
								response=AAPBDHttpClient.post(url).body();
							}
							catch (Exception e) {
								// TODO: handle exception
								 Log.e("MYAPP", "exception", e);
								 if(busyNow!=null){
									 busyNow.dismis();
								 }
							}

							runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									if(busyNow!=null){
										 busyNow.dismis();
									 }
									try {
					                    Log.e("Response", ">>" + new String(response));
					                    if (!TextUtils.isEmpty(new String(response))) {
					    					Gson g = new Gson();
					    					mGeneralActivityResponse = g.fromJson(new String(response),GeneralActivityResponse.class);
					    					
					    					if (mGeneralActivityResponse.getStatus().equalsIgnoreCase("true")) {
					    						PersistData.setStringData(con, AppConstant.firstHead, "Yes");
					    						int skip=0;
					    						
					    						for(GeneralActivityList ilist:mGeneralActivityResponse.getResult()){
					    							
					    							
													if(!db.ifGeneralActivityExist(ilist.getId())){
														db.addGeneralActivity(ilist);
														int tempSize=ilist.getTop_images().size();
														
														int j=1;
														
														for(int i=skip; i<ilist.getTop_images().size(); i++){
															
																db.addIslandImage(ilist.getId(), ilist.getTop_images().get(i), j, "General_activity");
																j++;
													}
														skip=ilist.getTop_images().size();
														for(int i=0; i<ilist.getIsland_list().size(); i++){
															ilist.getIsland_list().get(i).setActivityId(ilist.getId());
															db.addGeneralActivityFeatureIsland(ilist.getIsland_list().get(i));
															
														}
														for(int i=0; i<ilist.getThings_to_know().size(); i++){
															ilist.getThings_to_know().get(i).setActivityId(ilist.getId());
															db.addThinksToKnow(ilist.getThings_to_know().get(i));
															
														}
														for(int i=0; i<ilist.getTypes_of_activity().size(); i++){
															ilist.getTypes_of_activity().get(i).setActivityId(ilist.getId());
															db.addTypesOfActivity(ilist.getTypes_of_activity().get(i));
															
														}
//														
													}
													

													
												}
					    						
					    						Intent intent = new Intent(SplashActivity.this, MainActivity.class);
												startActivity(intent);
												finish();
												
											} else {
												AlertMessage.showMessage(con, getString(R.string.app_name), mGeneralActivityResponse.getMessage() + "");

											}
					                    }


					                } catch (final Exception e) {

					                    e.printStackTrace();
					                }
									
									
								}
							});
						}
					});
			
			
		}
}
