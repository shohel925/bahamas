package com.theislandofthebahamas.adapter;

import java.util.Vector;

import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.IslandDetailsList;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class IslandHomePagerAdapter extends PagerAdapter {

	private LayoutInflater inflater;
	public Context con;
	Vector<IslandDetailsList> detailList;

	// constructor
	public IslandHomePagerAdapter(Context con, Vector<IslandDetailsList> detailList) {
		this.con = con;
		this.detailList=detailList;
	}

	@Override
	public int getCount() {
		return this.detailList.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == (RelativeLayout) object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView ImageViewIsland;
		TextView textViewIslandHomeTitle, textViewIslandDes;

		// AppConstant.fitnesspost= position;

		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View viewLayout = inflater.inflate(R.layout.island_home_pager_rows,container, false);

		ImageViewIsland = (ImageView) viewLayout.findViewById(R.id.ImageViewIsland);

		textViewIslandHomeTitle = (TextView) viewLayout.findViewById(R.id.textViewIslandHomeTitle);
		textViewIslandDes = (TextView) viewLayout.findViewById(R.id.textViewIslandDes);

		Typeface demi = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Demi.otf");
		Typeface tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");

		textViewIslandHomeTitle.setTypeface(demi);
		textViewIslandDes.setTypeface(tf);

		try {
			textViewIslandHomeTitle.setText(detailList.get(position).getTitle().toUpperCase());
			textViewIslandDes.setText(Html.fromHtml(detailList.get(position).getDescription()));
			 Picasso.with(con) .load(detailList.get(position).getImage_url()).error(R.drawable.ic_launcher).into(ImageViewIsland);
		} catch (final Exception e) {
			e.printStackTrace();
		}

		((ViewPager) container).addView(viewLayout);

		return viewLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((RelativeLayout) object);

	}
}