package com.theislandofthebahamas.adapter;

import java.util.List;
import java.util.Vector;

import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.IslandDetailsList;
import com.theislandofthebahamas.response.WeatherInfo;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class WeatherPagerAdapter extends PagerAdapter {

	private LayoutInflater inflater;
	public Context con;
	List<WeatherInfo> detailList;

	// constructor
	public WeatherPagerAdapter(Context con, List<WeatherInfo> detailList) {
		this.con = con;
		this.detailList = detailList;
	}

	@Override
	public int getCount() {
		return this.detailList.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == (RelativeLayout) object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView imgWeatherIcon;
		TextView txtCurrentTemp, txtVisibility, txtPressure, txtWind, txtHumidity;

		// AppConstant.fitnesspost= position;

		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View viewLayout = inflater.inflate(R.layout.weather_rows, container, false);
		TextView textviewWeather=(TextView)viewLayout.findViewById(R.id.textviewWeather);
		imgWeatherIcon = (ImageView) viewLayout.findViewById(R.id.imgWeatherIcon);

		txtCurrentTemp = (TextView) viewLayout.findViewById(R.id.txtCurrentTemp);
		txtVisibility = (TextView) viewLayout.findViewById(R.id.txtVisibility);
		txtPressure = (TextView) viewLayout.findViewById(R.id.txtPressure);
		txtWind = (TextView) viewLayout.findViewById(R.id.txtWind);
		txtHumidity = (TextView) viewLayout.findViewById(R.id.txtHumidity);
		TextView weatherTv1=(TextView)viewLayout.findViewById(R.id.weatherTv1);
		TextView visibility=(TextView)viewLayout.findViewById(R.id.visibility);
		TextView presser=(TextView)viewLayout.findViewById(R.id.presser);
		TextView humidity=(TextView)viewLayout.findViewById(R.id.humidity);
		
		Typeface tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Regular.otf");
		Typeface gotham = Typeface.createFromAsset(con.getAssets(), "font/ufonts.com_gotham-bold.ttf");
		Typeface demi = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Demi.otf");
		textviewWeather.setTypeface(gotham);
		txtCurrentTemp.setTypeface(tf);

		weatherTv1.setTypeface(demi);
		visibility.setTypeface(demi);
		presser.setTypeface(demi);
		humidity.setTypeface(demi);
		
		txtVisibility.setTypeface(tf);
		txtPressure.setTypeface(tf);
		txtWind.setTypeface(tf);
		txtHumidity.setTypeface(tf);

		WeatherInfo query = detailList.get(position);

		try {
			txtCurrentTemp.setText(query.getTemp_f() + "\u00B0F / " + query.getTemp_c() + "\u00B0C");
			txtVisibility.setText(query.getVis() + " miles");
			txtPressure.setText(query.getPres() + " in/HG");
			txtWind.setText(query.getWind_dir() + " " + query.getWindspeed() + " kmph");
			txtHumidity.setText(query.getRhumid() + "%");
			Picasso.with(con).load(query.getImage()).error(R.drawable.ic_launcher).into(imgWeatherIcon);
		}
		catch (final Exception e) {
			e.printStackTrace();
		}

		((ViewPager) container).addView(viewLayout);

		return viewLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((RelativeLayout) object);

	}
}