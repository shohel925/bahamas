package com.theislandofthebahamas.adapter;

import java.util.Vector;

import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.IslandDetailsList;
import com.theislandofthebahamas.datamodel.IslandImageInfo;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GeneralDetailsPagerAdapter extends PagerAdapter {

	private LayoutInflater inflater;
	public Context con;
	Vector<IslandImageInfo> detailList;

	// constructor
	public GeneralDetailsPagerAdapter(Context con, Vector<IslandImageInfo> detailList) {
		this.con = con;
		this.detailList=detailList;
	}

	@Override
	public int getCount() {
		return this.detailList.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == (RelativeLayout) object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView imagePager;

		// AppConstant.fitnesspost= position;

		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View viewLayout = inflater.inflate(R.layout.detail_pager_rows,container, false);

		imagePager = (ImageView) viewLayout.findViewById(R.id.imagePager);


		try {
			 Picasso.with(con) .load(detailList.get(position).getImageUrl()).error(R.drawable.ic_launcher).into(imagePager);
		} catch (final Exception e) {
			e.printStackTrace();
		}

		((ViewPager) container).addView(viewLayout);

		return viewLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((RelativeLayout) object);

	}
}