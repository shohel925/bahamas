package com.theislandofthebahamas.adapter;

import java.util.Vector;

import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.GeneralActivityTypesOfActivityInfo;
import com.theislandofthebahamas.datamodel.GeneralActvityThinksToKnowInfo;
import com.theislandofthebahamas.datamodel.IslandDetailsList;
import com.theislandofthebahamas.util.HtmlRemove;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TypesOfActivityAdapter extends PagerAdapter {

	private LayoutInflater inflater;
	public Context con;
	Vector<GeneralActivityTypesOfActivityInfo> detailList;

	// constructor
	public TypesOfActivityAdapter(Context con, Vector<GeneralActivityTypesOfActivityInfo> detailList) {
		this.con = con;
		this.detailList=detailList;
	}

	@Override
	public int getCount() {
		return this.detailList.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == (RelativeLayout) object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView ImageViewThinks;
		TextView textviewTitle, textviewDescription;

		// AppConstant.fitnesspost= position;

		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View viewLayout = inflater.inflate(R.layout.thinks_to_know_row,container, false);

		ImageViewThinks = (ImageView) viewLayout.findViewById(R.id.ImageViewThinks);

		textviewTitle = (TextView) viewLayout.findViewById(R.id.textviewTitle);
		textviewDescription = (TextView) viewLayout.findViewById(R.id.textviewDescription);

		Typeface medium = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		Typeface gotham = Typeface.createFromAsset(con.getAssets(), "font/ufonts.com_gotham-bold.ttf");

		textviewTitle.setTypeface(gotham);
		textviewDescription.setTypeface(medium);

		try {
			textviewTitle.setText(detailList.get(position).getTitle().toUpperCase());
			String removeDes=HtmlRemove.getPerticularHtmlValue(detailList.get(position).getDescription(), "<h3>", "</h3>");
			textviewDescription.setText(removeDes.replaceAll("\\<[^>]*>",""));
			 Picasso.with(con) .load(detailList.get(position).getImage()).error(R.drawable.ic_launcher).into(ImageViewThinks);
		} catch (final Exception e) {
			e.printStackTrace();
		}

		((ViewPager) container).addView(viewLayout);

		return viewLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((RelativeLayout) object);

	}
}