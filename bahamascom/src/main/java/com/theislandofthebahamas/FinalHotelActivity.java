package com.theislandofthebahamas;

import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aapbd.utils.storage.PersistData;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.datamodel.GeneralHotelInfo;
import com.theislandofthebahamas.datamodel.IslandList;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.util.AppConstant;

public class FinalHotelActivity extends Activity {

	Context con;
	DatabaseHandler db;
	Vector<GeneralHotelInfo> hotelList;
	ListView listviewFinalHotel;
	TextView textviewCros,textviewfinal;
	Vector<IslandList> tempList;
	public static FinalHotelActivity instance;
	public static FinalHotelActivity getInstance(){
		return instance;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.final_hotel);
		con = this;
		db=new DatabaseHandler(con);
		instance=this;
		initUI();

	}
	
	private void initUI() {
		if(hotelList!=null){
			hotelList.removeAllElements();
		}
		tempList= new Vector<IslandList>();
		if(tempList!=null){
			tempList.removeAllElements();
		}
		tempList=db.getIsland(PersistData.getStringData(con, AppConstant.islandID));
		textviewfinal=(TextView)findViewById(R.id.textviewfinal);
		
		textviewfinal.setText(tempList.get(0).getName());
		textviewCros=(TextView)findViewById(R.id.textviewCros);
		listviewFinalHotel = (ListView)findViewById(R.id.listviewFinalHotel);
		hotelList=db.getIslandHotelListFromIslandId(PersistData.getStringData(con, AppConstant.islandID));
		if(hotelList.size()>0){
			listviewFinalHotel.setAdapter(new HotelAdapter());
		}
		
		textviewCros.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
	}
	public void cancelBtn(View v){
		finish();
	}
	
	private class HotelAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return hotelList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return hotelList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			if(convertView == null){
				convertView = ((LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.island_hotel_rows, parent, false);
			}
			
			GeneralHotelInfo query=hotelList.get(position);
			
			ImageView imageViewIslandHotelRows = (ImageView) convertView.findViewById(R.id.imageViewIslandHotelRows);
			TextView textViewIslandHotelTitle = (TextView) convertView.findViewById(R.id.textViewIslandHotelTitle);
			TextView textviewIslandAddress = (TextView) convertView.findViewById(R.id.textviewIslandAddress);
			
			Picasso.with(con).load(query.getImage_url()).into(imageViewIslandHotelRows);
			textViewIslandHotelTitle.setText(query.getName());
			textviewIslandAddress.setText(query.getAddress());
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					GeneralHotelInfo query=hotelList.get(position);
					AppConstant.mGeneralHotelInfo=query;
					Intent intent = new Intent(con, GeneralHotelDetailActivity.class);
					startActivity(intent);
				}
			});
			
			return convertView;
		}
		
	}
	


}
