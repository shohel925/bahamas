package com.theislandofthebahamas.datamodel;

public class IslandDM {

	private int islandId;
	private String islandName;
	private int islandPicId;
	private int islandLogoId;
	public int getIslandId() {
		return islandId;
	}
	public void setIslandId(int islandId) {
		this.islandId = islandId;
	}
	public String getIslandName() {
		return islandName;
	}
	public void setIslandName(String islandName) {
		this.islandName = islandName;
	}
	public int getIslandPicId() {
		return islandPicId;
	}
	public void setIslandPicId(int islandPicId) {
		this.islandPicId = islandPicId;
	}
	public int getIslandLogoId() {
		return islandLogoId;
	}
	public void setIslandLogoId(int islandLogoId) {
		this.islandLogoId = islandLogoId;
	}
	
}
