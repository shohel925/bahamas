package com.theislandofthebahamas.datamodel;

public class TransportDetailsDataModel {

	private String name = "";
	private String address = "";
	private String phone = "";
	private String fax = "";
	private String email = "";
	private String website = "";
	

	/**
	 * @param name
	 * @param address
	 * @param phone
	 * @param fax
	 * @param email
	 * @param website
	 */
	public TransportDetailsDataModel(String name, String address, String phone, String fax,
			String email, String website) {
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.fax = fax;
		this.email = email;
		this.website = website;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public TransportDetailsDataModel() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Override
	public String toString() {
		return "DataModel [\nname=" + name + ",\n address=" + address + ",\n phone="
				+ phone + ",\n fax=" + fax + ",\n email=" + email + ",\n website="
				+ website + "]";
	}
	

}
