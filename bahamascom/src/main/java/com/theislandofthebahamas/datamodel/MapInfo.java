package com.theislandofthebahamas.datamodel;

public class MapInfo {
	double lat=0;
	double lon=0;
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	

}
