package com.theislandofthebahamas.datamodel;

import java.util.List;

public class GeneralEventInfo {
	String id="";
	String name="";
	String description="";
	String islandid="";
	String isfeature="";
	String startdate="";
	String enddate="";
	String yearly="";
	List<String>phone;
	String phoneStr="";
	
	
	public String getPhoneStr() {
		return phoneStr;
	}
	public void setPhoneStr(String phoneStr) {
		this.phoneStr = phoneStr;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIslandid() {
		return islandid;
	}
	public void setIslandid(String islandid) {
		this.islandid = islandid;
	}
	public String getIsfeature() {
		return isfeature;
	}
	public void setIsfeature(String isfeature) {
		this.isfeature = isfeature;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getYearly() {
		return yearly;
	}
	public void setYearly(String yearly) {
		this.yearly = yearly;
	}
	public List<String> getPhone() {
		return phone;
	}
	public void setPhone(List<String> phone) {
		this.phone = phone;
	}
	
	

}
