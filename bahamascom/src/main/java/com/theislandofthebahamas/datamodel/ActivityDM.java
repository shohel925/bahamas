package com.theislandofthebahamas.datamodel;

public class ActivityDM {

	private int activityId;
	private String activityName;
	private int activityPicId;
	private String activityDesc;
	private int[] islandIds;
	public int getActivityId() {
		return activityId;
	}
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public int getActivityPicId() {
		return activityPicId;
	}
	public void setActivityPicId(int activityPicId) {
		this.activityPicId = activityPicId;
	}
	public String getActivityDesc() {
		return activityDesc;
	}
	public void setActivityDesc(String activityDesc) {
		this.activityDesc = activityDesc;
	}
	public int[] getIslandIds() {
		return islandIds;
	}
	public void setIslandIds(int[] islandIds) {
		this.islandIds = islandIds;
	}
	
}
