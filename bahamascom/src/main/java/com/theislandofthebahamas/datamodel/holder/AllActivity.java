package com.theislandofthebahamas.datamodel.holder;

import java.util.ArrayList;

import com.theislandofthebahamas.datamodel.ActivityDM;

public class AllActivity {

	private static ArrayList<ActivityDM> activityList = new ArrayList<ActivityDM>();
	
	public static void removeAll(){
		activityList.clear();
	}

	public static ArrayList<ActivityDM> getActivityList() {
		return activityList;
	}

	public static void setActivityList(ArrayList<ActivityDM> activityList) {
		AllActivity.activityList = activityList;
	}
	
	public static void addActivity(ActivityDM activityDm){
		if(activityList == null){
			activityList = new ArrayList<ActivityDM>();
		}
		
		activityList.add(activityDm);
	}
	
	public static ActivityDM getActivityById(int activityId){
		if(activityList == null){
			return null;
		}
		
		if(activityList.size() == 0){
			return null;
		}
		
		for (ActivityDM activityDM : activityList) {
			if(activityDM.getActivityId() == activityId){
				return activityDM;
			}
		}
		
		return null;
	}
	
	public static ActivityDM getActivityByPisition(int position){
		if(activityList == null){
			return null;
		}
		
		if(activityList.size() <= position){
			return null;
		}
		
		return activityList.get(position);
	}
	
}
