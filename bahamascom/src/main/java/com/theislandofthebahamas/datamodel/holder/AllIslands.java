package com.theislandofthebahamas.datamodel.holder;

import java.util.ArrayList;

import com.theislandofthebahamas.datamodel.IslandDM;

public class AllIslands {

	private static ArrayList<IslandDM> islandList = new ArrayList<IslandDM>();
	
	public static void removeAll(){
		islandList.clear();
	}

	public static ArrayList<IslandDM> getIslandList() {
		return islandList;
	}

	public static void setIslandList(ArrayList<IslandDM> islandList) {
		AllIslands.islandList = islandList;
	}
	
	public static void addIsland(IslandDM island){
		if(islandList == null){
			islandList = new ArrayList<IslandDM>();
		}
		
		islandList.add(island);
	}
	
	public static IslandDM getIslandByIslandId(int islandId){
		if(islandList == null){
			return null;
		}
		
		if(islandList.size() == 0){
			return null;
		}
		
		for (IslandDM islandDM : islandList) {
			if(islandDM.getIslandId() == islandId){
				return islandDM;
			}
		}
		
		return null;
	}
	
	public static IslandDM getIslandByPosition(int position){
		if(islandList == null){
			return null;
		}
		
		if(islandList.size() <= position){
			return null;
		}
		
		return islandList.get(position);
	}
	
}
