package com.theislandofthebahamas.datamodel.holder;

import com.theislandofthebahamas.datamodel.HotelDM;

import java.util.ArrayList;

public class AllHotels {

	private static ArrayList<HotelDM> allHotels = new ArrayList<HotelDM>();
	
	public static void remove(){
		allHotels.clear();
	}

	public static ArrayList<HotelDM> getAllHotels() {
		return allHotels;
	}

	public static void setAllHotels(ArrayList<HotelDM> allHotels) {
		AllHotels.allHotels = allHotels;
	}
	
	public static void addHotel(HotelDM hotelDm){
		if(allHotels == null){
			allHotels = new ArrayList<HotelDM>();
		}
		
		allHotels.add(hotelDm);
	}
	
	public static HotelDM getHotelById(int hotelId){
		if(allHotels == null){
			return null;
		}
		
		if(allHotels.size() == 0){
			return null;
		}
		
		for (HotelDM hotelDM : allHotels) {
			if(hotelDM.getHotelId() == hotelId){
				return hotelDM;
			}
		}
		
		return null;
	}
}
