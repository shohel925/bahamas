package com.theislandofthebahamas.datamodel.holder;

import java.util.ArrayList;

import com.theislandofthebahamas.datamodel.MoreInformationDM;

public class AllMoreInfo {

	private static ArrayList<MoreInformationDM> moreInfoList = new ArrayList<MoreInformationDM>();
	
	public static void removeAll(){
		moreInfoList.clear();
	}

	public static ArrayList<MoreInformationDM> getMoreInfoList() {
		return moreInfoList;
	}

	public static void setMoreInfoList(ArrayList<MoreInformationDM> moreInfoList) {
		AllMoreInfo.moreInfoList = moreInfoList;
	}
	
	public static void addMoreInfo(MoreInformationDM moreInfoDm){
		if(moreInfoList == null){
			moreInfoList = new ArrayList<MoreInformationDM>();
		}
		
		moreInfoList.add(moreInfoDm);
	}
	
	public static MoreInformationDM getMoreInfoById(int moreInfoId){
		if(moreInfoList == null){
			return null;
		}
		
		if(moreInfoList.size() == 0){
			return null;
		}
		
		for (MoreInformationDM moreInformationDM : moreInfoList) {
			if(moreInformationDM.getMoreInfoId() == moreInfoId){
				return moreInformationDM;
			}
		}
		
		return null;
	}
	
	public static MoreInformationDM getMoreInfoByPosition(int position){
		if(moreInfoList == null){
			return null;
		}
		
		if(moreInfoList.size() <= position){
			return null;
		}
		
		return moreInfoList.get(position);
	}
}
