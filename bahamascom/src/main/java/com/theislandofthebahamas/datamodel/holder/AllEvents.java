package com.theislandofthebahamas.datamodel.holder;

import java.util.ArrayList;

import com.theislandofthebahamas.datamodel.EventDM;

public class AllEvents {

	private static ArrayList<EventDM> allEvents = new ArrayList<EventDM>();
	
	public static void removeAll(){
		allEvents.clear();
	}

	public static ArrayList<EventDM> getAllEvents() {
		return allEvents;
	}

	public static void setAllEvents(ArrayList<EventDM> allEvents) {
		AllEvents.allEvents = allEvents;
	}
	
	public static void addEvent(EventDM eventDm){
		if(allEvents == null){
			allEvents = new ArrayList<EventDM>();
		}
		
		allEvents.add(eventDm);
	}
	
	public static EventDM getEventById(int eventId){
		if(allEvents == null){
			return null;
		}
		
		if(allEvents.size() == 0){
			return null;
		}
		
		for (EventDM eventDM : allEvents) {
			if(eventDM.getEventId() == eventId){
				return eventDM;
			}
		}
		
		return null;
	}
	
}
