package com.theislandofthebahamas.datamodel;

public class MoreInfo {
	String about_us="";
	String government="";
	String history="";
	String holidays="";
	String local_customs="";
	String language="";
	String people="";
	String proximity="";
	String travel_tips="";
	public String getAbout_us() {
		return about_us;
	}
	public void setAbout_us(String about_us) {
		this.about_us = about_us;
	}
	public String getGovernment() {
		return government;
	}
	public void setGovernment(String government) {
		this.government = government;
	}
	public String getHistory() {
		return history;
	}
	public void setHistory(String history) {
		this.history = history;
	}
	public String getHolidays() {
		return holidays;
	}
	public void setHolidays(String holidays) {
		this.holidays = holidays;
	}
	public String getLocal_customs() {
		return local_customs;
	}
	public void setLocal_customs(String local_customs) {
		this.local_customs = local_customs;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getPeople() {
		return people;
	}
	public void setPeople(String people) {
		this.people = people;
	}
	public String getProximity() {
		return proximity;
	}
	public void setProximity(String proximity) {
		this.proximity = proximity;
	}
	public String getTravel_tips() {
		return travel_tips;
	}
	public void setTravel_tips(String travel_tips) {
		this.travel_tips = travel_tips;
	}
	
	

}
