package com.theislandofthebahamas.datamodel;

public class HotelDM {

	private int hotelId;
	private String hotelName;
	private String hotelAddress;
	private String hotelAddressDetail;
	private double lat;
	private double lng;
	private String phoneNumber;
	private String webAddress;
	private String isandName;
	private int hotelPicId;
	public int getHotelId() {
		return hotelId;
	}
	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelAddress() {
		return hotelAddress;
	}
	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}
	public int getHotelPicId() {
		return hotelPicId;
	}
	public void setHotelPicId(int hotelPicId) {
		this.hotelPicId = hotelPicId;
	}
	public String getHotelAddressDetail() {
		return hotelAddressDetail;
	}
	public void setHotelAddressDetail(String hotelAddressDetail) {
		this.hotelAddressDetail = hotelAddressDetail;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getWebAddress() {
		return webAddress;
	}
	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}
	public String getIsandName() {
		return isandName;
	}
	public void setIsandName(String isandName) {
		this.isandName = isandName;
	}
	
}
