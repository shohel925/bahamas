/**
 * 
 */
package com.theislandofthebahamas.datamodel;

import java.io.Serializable;

public class TransportTableData implements Serializable {
	
	private String splittype;
	private String islandid;
	private String transport_type;
	private String transport_info;
	

	public TransportTableData(String splittype, String islandid,
			String transport_type, String transport_info) {
		super();
		this.splittype = splittype;
		this.islandid = islandid;
		this.transport_type = transport_type;
		this.transport_info = transport_info;
	}
	/**
	 * 
	 */
	public TransportTableData() {
		// TODO Auto-generated constructor stub
	}
	public String getSplittype() {
		return splittype;
	}
	public void setSplittype(String splittype) {
		this.splittype = splittype;
	}
	public String getIslandid() {
		return islandid;
	}
	public void setIslandid(String islandid) {
		this.islandid = islandid;
	}
	public String getTransport_type() {
		return transport_type;
	}
	public void setTransport_type(String transport_type) {
		this.transport_type = transport_type;
	}
	public String getTransport_info() {
		return transport_info;
	}
	public void setTransport_info(String transport_info) {
		this.transport_info = transport_info;
	}
	

}
