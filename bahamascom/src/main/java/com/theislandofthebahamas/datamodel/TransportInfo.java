
package com.theislandofthebahamas.datamodel;


public class TransportInfo {
	
	private String transport_type;
	private String transport_info;
	
	public TransportInfo(String transportType, String transportInfo) {
		
		this.transport_type = transportType;
		this.transport_info = transportInfo;
	}
	
	public String getTransportType() {
		return transport_type;
	}
	public void setTransportType(String transportType) {
		this.transport_type = transportType;
	}
	public String getTransportInfo() {
		return transport_info;
	}
	public void setTransportInfo(String transportInfo) {
		this.transport_info = transportInfo;
	}
	

}
