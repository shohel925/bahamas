package com.theislandofthebahamas.datamodel;

public class GeneralHotelInfo {
	String id="";
	String name="";
	String image_url="";
	String address="";
	String phone="";
	String lat="";
	String lon="";
	String islandid="";
	String isfeature="";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getIslandid() {
		return islandid;
	}
	public void setIslandid(String islandid) {
		this.islandid = islandid;
	}
	public String getIsfeature() {
		return isfeature;
	}
	public void setIsfeature(String isfeature) {
		this.isfeature = isfeature;
	}
	
	

}
