package com.theislandofthebahamas;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.theislandofthebahamas.fragment.GeneralActivitiesFragment;
import com.theislandofthebahamas.fragment.GeneralEventsFragment;
import com.theislandofthebahamas.fragment.GeneralHomeFragment;
import com.theislandofthebahamas.fragment.GeneralHotelsFragment;
import com.theislandofthebahamas.fragment.GeneralMoreFragment;
import com.theislandofthebahamas.util.Helper;

public class MainActivity extends FragmentActivity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the sections. We use a {@link FragmentPagerAdapter} derivative,
	 * which will keep every loaded fragment in memory. If this becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v13.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	Context context, con;

	// Fragment list
	GeneralHomeFragment generalHomeFragment;
	GeneralActivitiesFragment generalActivitiesFragment;
	GeneralHotelsFragment mainHotelFragment;
	GeneralEventsFragment generalEventsFragment;
	GeneralMoreFragment generalMoreFragment;

	Button btnOpenHome;
	Button btnOpenActivities;
	Button btnOpenHotels;
	Button btnOpenEvents;
	Button btnOpenMore;
	TextView indicatorTitle;
	
	public static android.support.v4.app.FragmentManager fragmentManagers;
    private static final String[] CONTENT = new String[] { "Home", "Activities", "Hotels", "Events", "More"};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = con = this;
		updateCustomActionBar();
		
		fragmentManagers= getSupportFragmentManager();
		btnOpenHome = (Button) findViewById(R.id.btnOpenHome);
		btnOpenActivities = (Button) findViewById(R.id.btnOpenActivities);
		btnOpenHotels = (Button) findViewById(R.id.btnOpenHotels);
		btnOpenEvents = (Button) findViewById(R.id.btnOpenEvents);
		btnOpenMore = (Button) findViewById(R.id.btnOpenMore);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.islandMainPager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
//		TabPageIndicator indicator = (TabPageIndicator)findViewById(R.id.indicator);
//        indicator.setViewPager(mViewPager);
//
//       
		//mViewPager.setOnPageChangeListener(mainPageChangeListener);
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				changePos(arg0);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
//		Typeface tf = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Demi.otf");
//        ViewGroup vg = (ViewGroup) indicator.getChildAt(0);
//        int vgChildCount = vg.getChildCount();
//        for (int j = 0; j < vgChildCount; j++) {
//            View vgChild = vg.getChildAt(j);
//            if (vgChild instanceof TextView) {
//            	indicatorTitle=((TextView) vgChild);
//            	indicatorTitle.setTypeface(tf);
//            	if(j==0){
//            		indicatorTitle.setTextColor(R.color.menu1);
//            		}
//            	if(j==1){
//            		indicatorTitle.setTextColor(R.color.menu2);
//            		}
//            	if(j==2){
//            		indicatorTitle.setTextColor(R.color.menu3);
//            		}
//            	if(j==3){
//            		indicatorTitle.setTextColor(R.color.menu4);
//            		}
//            	if(j==4){
//            		indicatorTitle.setTextColor(R.color.menu5);
//            		}
//            }
//        }
//		//Demo
//		btnOpenHome.setBackgroundResource(R.drawable.events_yellow_bg);
//		btnOpenActivities.setBackgroundResource(0x0);
//		btnOpenHotels.setBackgroundResource(0x0);
//		btnOpenEvents.setBackgroundResource(0x0);
//		btnOpenMore.setBackgroundResource(0x0);
//		btnOpenHome.setTypeface(tf);
//		btnOpenActivities.setTypeface(tf);
//		btnOpenHotels.setTypeface(tf);
//		btnOpenEvents.setTypeface(tf);
//		btnOpenMore.setTypeface(tf);
		
		changePos(0);
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}*/

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).

			switch (position) {
			case 0:
				if (generalHomeFragment == null) {
					generalHomeFragment = new GeneralHomeFragment();
				}
				return generalHomeFragment;
			case 1:
				if (generalActivitiesFragment == null) {
					generalActivitiesFragment = new GeneralActivitiesFragment();
				}
				return generalActivitiesFragment;
			case 2:
				if (mainHotelFragment == null) {
					mainHotelFragment = new GeneralHotelsFragment();
				}
				return mainHotelFragment;
			case 3:
				if (generalEventsFragment == null) {
					generalEventsFragment = new GeneralEventsFragment();
				}
				return generalEventsFragment;
			case 4:
				if (generalMoreFragment == null) {
					generalMoreFragment = new GeneralMoreFragment();
				}
				return generalMoreFragment;

			default:
				return null;
			}
			// return PlaceholderFragment.newInstance(position + 1);

		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 5;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Log.e("Call", ">>"+position);
//			Locale l = Locale.getDefault();
//			switch (position) {
//			case 0:
//				return getString(R.string.title_section1).toUpperCase(l);
//			case 1:
//				return getString(R.string.title_section2).toUpperCase(l);
//			case 2:
//				return getString(R.string.title_section3).toUpperCase(l);
//			case 3:
//				return "a";
//			case 4:
//				return "b";
//			}
			return CONTENT[position % CONTENT.length];
		}
	}
	
	public void changePos(int position)
	{
		switch (position) {
		case 0:
			btnOpenHome.setBackgroundResource(R.drawable.events_yellow_bg);
			btnOpenActivities.setBackgroundResource(0x0);
			btnOpenHotels.setBackgroundResource(0x0);
			btnOpenEvents.setBackgroundResource(0x0);
			btnOpenMore.setBackgroundResource(0x0);
			
			Log.e("tag", "...."+position);
			break;
		case 1:
			btnOpenHome.setBackgroundResource(0x0);
			btnOpenActivities.setBackgroundResource(R.drawable.events_red_bg);
			btnOpenHotels.setBackgroundResource(0x0);
			btnOpenEvents.setBackgroundResource(0x0);
			btnOpenMore.setBackgroundResource(0x0);
			break;
		case 2:
			btnOpenHome.setBackgroundResource(0x0);
			btnOpenActivities.setBackgroundResource(0x0);
			btnOpenHotels.setBackgroundResource(R.drawable.events_green_bg);
			btnOpenEvents.setBackgroundResource(0x0);
			btnOpenMore.setBackgroundResource(0x0);
			break;
		case 3:
			btnOpenHome.setBackgroundResource(0x0);
			btnOpenActivities.setBackgroundResource(0x0);
			btnOpenHotels.setBackgroundResource(0x0);
			btnOpenEvents.setBackgroundResource(R.drawable.events_bg);
			btnOpenMore.setBackgroundResource(0x0);
			break;
		case 4:
			btnOpenHome.setBackgroundResource(0x0);
			btnOpenActivities.setBackgroundResource(0x0);
			btnOpenHotels.setBackgroundResource(0x0);
			btnOpenEvents.setBackgroundResource(0x0);
			btnOpenMore.setBackgroundResource(R.drawable.events_brick_bg);
			break;
		default:
			btnOpenHome.setBackgroundResource(0x0);
			btnOpenActivities.setBackgroundResource(0x0);
			btnOpenHotels.setBackgroundResource(0x0);
			btnOpenEvents.setBackgroundResource(0x0);
			btnOpenMore.setBackgroundResource(0x0);
			break;
		}
	}

	public void openMainHome(View v) {
		mViewPager.setCurrentItem(0);
	}

	public void openMainActivities(View v) {
		mViewPager.setCurrentItem(1);
	}

	public void openMainHotels(View v) {
		mViewPager.setCurrentItem(2);
	}

	public void openMainEvents(View v) {
		mViewPager.setCurrentItem(3);
	}

	public void openMainMore(View v) {
		mViewPager.setCurrentItem(4);
	}

	private void updateCustomActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.ab_main_home);
		
		Button btnCallHelp = (Button)actionBar.getCustomView().findViewById(R.id.btnCallHelp);
		Button btnSearch = (Button)actionBar.getCustomView().findViewById(R.id.btnSearch);
		
		btnCallHelp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Helper.CustomPhoneCall(con, "+18002242627");
			}
		});
		
		btnSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(context, "Will open search option", Toast.LENGTH_SHORT).show();
			}
		});

	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(mViewPager.getCurrentItem() == 0){
			super.onBackPressed();
		}
		else{
			mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
		}
	}
}
