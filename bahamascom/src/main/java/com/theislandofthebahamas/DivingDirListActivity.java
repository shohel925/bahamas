package com.theislandofthebahamas;

import java.util.HashMap;
import java.util.Vector;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aapbd.utils.nagivation.StartActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;
import com.theislandofthebahamas.datamodel.SubcategoryInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.util.AppConstant;

public class DivingDirListActivity extends Activity {
	
    private ListView divingDetailListView; 
    private Vector<DivingDirectoryInfo> divingDirList;
    private Context context, con;
	private DatabaseHandler db;
	private ImageButton btBack, btSearch;
	// Google Map
    private GoogleMap googleMap;
    RelativeLayout mapView ;
    TextView textViewFilter,tv_appName_divingDirDetail;
    Vector<SubcategoryInfo> subCategoryList,finalSubList;
    String acticityId;
    Dialog dialog;
    ImageView imgBt_geo_divingDirDetail,ImageList;
   HashMap<Marker, DivingDirectoryInfo> tempMarker;
   Typeface medium,gotham,demiBold;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_diving_dir_detail);
		context = con = this;
		db=new DatabaseHandler(con);
		medium = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Medium.otf");
		gotham = Typeface.createFromAsset(getAssets(), "font/ufonts.com_gotham-bold.ttf");
		demiBold = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Demi.otf");
		ImageList=(ImageView)findViewById(R.id.ImageList);
		tempMarker=new HashMap<Marker, DivingDirectoryInfo>();
		mapView = (RelativeLayout)findViewById(R.id.diving_dir_mapView);
		tv_appName_divingDirDetail=(TextView)findViewById(R.id.tv_appName_divingDirDetail);
		divingDetailListView = (ListView) findViewById(R.id.listView_DivingDirDetail);
		tv_appName_divingDirDetail.setText(AppConstant.mGeneralActivityList.getName());
		tv_appName_divingDirDetail.setTypeface(gotham);
		
		btBack = (ImageButton)findViewById(R.id.imgBt_back_divingDirDetail);
		btSearch = (ImageButton)findViewById(R.id.imgBt_search_divingDirDetail);
		textViewFilter=(TextView)findViewById(R.id.textViewFilter);
		imgBt_geo_divingDirDetail=(ImageView)findViewById(R.id.imgBt_geo_divingDirDetail);
		textViewFilter.setTypeface(demiBold);
		divingDirList=new Vector<DivingDirectoryInfo>();
		if(divingDirList!=null){
			divingDirList.removeAllElements();
		}
		subCategoryList=new Vector<SubcategoryInfo>();
		if(subCategoryList!=null){
			subCategoryList.removeAllElements();
		}
		
		
		Log.e("Name","??"+AppConstant.mGeneralActivityList.getName());
		//=========== Get id from Category table =================
		acticityId =db.getCategoryIdFromTitle(AppConstant.mGeneralActivityList.getName());
		Log.e("id","??"+acticityId);
		subCategoryList=db.getCategoryListFromParentId(acticityId);
		//=========== DirectoryList from Directory list from activity id =================
		divingDirList = db.getDivingListFrmAcitivityId(acticityId);
		finalSubList=new Vector<SubcategoryInfo>();
		if(finalSubList!=null){
			finalSubList.removeAllElements();
		}
		// =========== add subCategory ===================
		SubcategoryInfo sinfo=new SubcategoryInfo();
		sinfo.setSubcategoryname("All");
		finalSubList.add(sinfo);
		for(int i=0; i<subCategoryList.size(); i++){
			finalSubList.add(subCategoryList.get(i));
		}
		Log.e("divingDirList", ">>"+divingDirList.size());
		 initilizeMap();
	       
		if(divingDirList.size()>0){
//			//initializing and setting the adapter to show list		
			DivingDirAdapter dda = new DivingDirAdapter();
			divingDetailListView.setAdapter(dda);
		}
		googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker marker) {
				// TODO Auto-generated method stub
				AppConstant.mDivingDirectoryInfo=tempMarker.get(marker);
				Log.e("lat", ">>"+AppConstant.mDivingDirectoryInfo.getLat());
				Log.e("lng", ">>"+AppConstant.mDivingDirectoryInfo.getLon());
				Log.e("Name", ">>"+AppConstant.mDivingDirectoryInfo.getName());
				StartActivity.toActivity(con, DrivingDirDetailActivity.class);
				
				return false;
			}
		});
		 
		 textViewFilter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog = new Dialog(con);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.filder_layout);
				dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
				ListView listviewFilter=(ListView)dialog.findViewById(R.id.listviewFilter);
				TextView dialogeTv=(TextView)dialog.findViewById(R.id.dialogeTv);
				dialogeTv.setTypeface(medium);
				listviewFilter.setAdapter(new FilterAdapter());
				// set the custom dialog components - text, image and button
				TextView filterCros = (TextView) dialog.findViewById(R.id.filterCros);
				filterCros.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});

				dialog.show();
			}
		});
		 
		 ImageList.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mapView.setVisibility(View.GONE);
	            divingDetailListView.setVisibility(View.VISIBLE);
	            ImageList.setVisibility(View.GONE);
	            imgBt_geo_divingDirDetail.setVisibility(View.VISIBLE);
			}
		});
		 
		 
		 imgBt_geo_divingDirDetail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				  mapView.setVisibility(View.VISIBLE);
		          divingDetailListView.setVisibility(View.GONE);
		          ImageList.setVisibility(View.VISIBLE);
		          imgBt_geo_divingDirDetail.setVisibility(View.GONE);
		          setMarkerOnMap();
			}
		});
		 
		
	}

	public void btBackOnClick(View view){
		finish();
	}
	//custom_action_bar search button press
	public void btSearchOnClick(View v)
	{
		
	}
	
	//custom_action_bar geo button press
	public void btGeoOnClick(View v){
		if(mapView.getVisibility() == View.VISIBLE){
			mapView.setVisibility(View.GONE);
            divingDetailListView.setVisibility(View.VISIBLE);
            imgBt_geo_divingDirDetail.setImageResource(R.drawable.island_hotel_directions_map_06);
		} else {
			try {
	            mapView.setVisibility(View.VISIBLE);
	            divingDetailListView.setVisibility(View.GONE);
	            imgBt_geo_divingDirDetail.setImageResource(R.drawable.island_hotel_directions_05);
	            Handler h=new Handler();
	            h.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						setMarkerOnMap();
					}
				}, 1000);
	            
	 
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
		}
		
		
	}

	
	private void initilizeMap() {
		if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.divingDirDetail_map)).getMap();
 
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
	
	private class FilterAdapter extends BaseAdapter{
		
		private class ViewHolder {
			TextView textviewFilterName;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return finalSubList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return finalSubList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder;
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.filter_rows, parent, false);
				
				holder = new ViewHolder();
				holder.textviewFilterName = (TextView)convertView.findViewById(R.id.textviewFilterName);
//				holder.imgVwIslandPic.setImageResource(imagaList.get(position));
//				
				convertView.setTag(holder);
			}else{
				holder = (ViewHolder) convertView.getTag();
			}
//			Picasso.with(context).load(filterNameList.get(position)).into(holder.imgVwIslandPic);
			holder.textviewFilterName.setText(finalSubList.get(position).getSubcategoryname());
			holder.textviewFilterName.setTypeface(medium);
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if(position==0){
						divingDirList = db.getDivingListFrmAcitivityId(acticityId);
						
					}else{
						divingDirList = db.getDivingListFrmAcitivityId(finalSubList.get(position).getId());
					}
					Log.e("divingDirList size", ">>"+divingDirList.size());
					if(divingDirList.size()>0){
						dialog.dismiss();
						setMarkerOnMap();
						textViewFilter.setText(getString(R.string.View)+":"+finalSubList.get(position).getSubcategoryname());
//						//initializing and setting the adapter to show list		
						divingDetailListView.setAdapter(new DivingDirAdapter());
					}
				}
			});
			
			return convertView;
		}
		
	}
 
	
	private void setMarkerOnMap() {
			if(googleMap!=null){
				googleMap.clear();
			}
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		//for(int i=0; i<divingDirList.size(); i++)
		for(int i=0; i<divingDirList.size(); i++){
			DivingDirectoryInfo dd=divingDirList.get(i);
			if((dd.getLat().equalsIgnoreCase("0.000000000000"))&&(dd.getLon().equalsIgnoreCase("0.000000000000"))){
				
			}else if((!dd.getLat().equalsIgnoreCase("false"))&&(!dd.getLon().equalsIgnoreCase("false"))){
				 LatLng currentPosition = new LatLng(Double.parseDouble(dd.getLat()),Double.parseDouble(dd.getLon()));
					// create marker
					MarkerOptions markerOption = new MarkerOptions().position(currentPosition).title(dd.getName()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
					Marker marker= googleMap.addMarker(markerOption);
					tempMarker.put(marker, divingDirList.get(i));
					builder.include(currentPosition);
			}
			
		}
		try {
			LatLngBounds bounds = builder.build();
			int padding = 100;
			CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
			googleMap.animateCamera(cu);	
		}
		catch (Exception e) {
			// TODO: handle exception
		e.printStackTrace();
		}
	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.diving_dir_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class DivingDirAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return divingDirList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return divingDirList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_diving_dir_detail, parent, false);
			}
			DivingDirectoryInfo query=divingDirList.get(position);
			ImageView imgDivingDirPic = (ImageView) convertView.findViewById(R.id.imgDivingDetaisRow);
			TextView txtDivingDirName = (TextView) convertView.findViewById(R.id.tv_diving_dir_name);
			TextView txtDivingDirLocation = (TextView) convertView.findViewById(R.id.tv_diving_dir_location);
			TextView txtDivingDirPhone = (TextView) convertView.findViewById(R.id.tv_diving_dir_phone);
			
			
			Picasso.with(context).load(query.getImage_url()).into(imgDivingDirPic);
			txtDivingDirName.setText(query.getName());
			txtDivingDirLocation.setText(query.getArea());
			txtDivingDirPhone.setText(query.getPhone());
			
			txtDivingDirName.setTypeface(medium);
			txtDivingDirLocation.setTypeface(medium);
			txtDivingDirPhone.setTypeface(medium);
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					AppConstant.mDivingDirectoryInfo=divingDirList.get(position);
					StartActivity.toActivity(con, DrivingDirDetailActivity.class);
					
				}
			});
			
			return convertView;
		}
		
	}
}
