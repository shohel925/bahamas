package com.theislandofthebahamas;


import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.aapbd.utils.nagivation.StartActivity;
import com.aapbd.utils.storage.PersistData;
import com.theislandofthebahamas.datamodel.IslandList;
import com.theislandofthebahamas.datamodel.TempIslandInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.util.AppConstant;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class IslandListActivity extends Activity {
	Typeface tf ;
	Context con,context;
	private DatabaseHandler db;
	private ImageButton btBack;
	List<TempIslandInfo>tempList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_island_list);
		
		con = context = this;
		tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		TextView tvTitle = (TextView) findViewById(R.id.tv_appName_islandList);
		tvTitle.setTypeface(tf, Typeface.BOLD);
		btBack = (ImageButton)findViewById(R.id.imgBt_back_islandList);

		db = new DatabaseHandler(context);
		tempList=new ArrayList<TempIslandInfo>();
		if(tempList!=null){
			tempList.clear();
		}
		tempList=AppConstant.tempIslandList();
		ListView lvIslandList = (ListView) findViewById(R.id.lv_islandList);
		lvIslandList.setAdapter(new IslandListAdapter());
		
		lvIslandList.setOnItemClickListener(new OnItemClickListener() {
			  @Override
			  public void onItemClick(AdapterView<?> parent, View view,
			    int position, long id) {
				  TempIslandInfo data = tempList.get(position);
				  PersistData.setStringData(con, AppConstant.islandID,data.getId());
				  StartActivity.toActivity(context, TransportActivity.class);
			  }
			}); 
		
		
	}
	public void btBackOnClick(View v)
	{
		this.finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.island_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	private class IslandListAdapter extends BaseAdapter{

		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return tempList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return tempList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_island_list_item, parent, false);
			}
			TempIslandInfo query=tempList.get(position);
			TextView txtIslandName = (TextView) convertView.findViewById(R.id.island_row_iteamName);

			txtIslandName.setText(query.getName());
			txtIslandName.setTextColor(Color.parseColor(query.getColor()));
			txtIslandName.setTypeface(tf);

			return convertView;
		}
		
	}
}
