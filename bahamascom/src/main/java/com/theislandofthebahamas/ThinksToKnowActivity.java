package com.theislandofthebahamas;

import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.theislandofthebahamas.adapter.ThinksToKnowAdapter;
import com.theislandofthebahamas.datamodel.GeneralActvityThinksToKnowInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.util.AppConstant;

public class ThinksToKnowActivity extends Activity {


	DatabaseHandler db;	
	Context context, con;
	Vector<GeneralActvityThinksToKnowInfo>tempImageList;
	ViewPager viewpagerGeneralDetails;
	LinearLayout indicator;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.thinks_to_know);
		getActionBar().hide();
		context = con = this;
		db=new DatabaseHandler(con);
		if(tempImageList!=null){
			tempImageList.removeAllElements();
		}
					tempImageList=AppConstant.generalThinksToKnow;
					viewpagerGeneralDetails=(ViewPager)findViewById(R.id.viewpagerThinks);
					indicator=(LinearLayout)findViewById(R.id.linearIndicator);
					viewpagerGeneralDetails.setAdapter(new ThinksToKnowAdapter(con, tempImageList));
					viewpagerGeneralDetails.setCurrentItem(0);
					controlDots(0);
					
					viewpagerGeneralDetails.setOnPageChangeListener(new OnPageChangeListener() {

						@Override
						public void onPageSelected(int pos) {
							// TODO Auto-generated method stub

							controlDots(pos);
						}

						@Override
						public void onPageScrolled(int arg0, float arg1, int arg2) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onPageScrollStateChanged(int arg0) {
							// TODO Auto-generated method stub

						}
					});
					
				
	
	}
	protected void controlDots(int newPageNumber) {

		try {
			indicator.removeAllViews(); // simple linear layout

			final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(

			android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT);

			layoutParams.setMargins(7, 2, 0, 2);

			final ImageView[] b = new ImageView[tempImageList.size()];

			// Log.e("Shop size ", ">>>" + db.getAllShoppingName().size());

			for (int i1 = 0; i1 < tempImageList.size(); i1++) {

				b[i1] = new ImageView(con);

				b[i1].setId(1000 + i1);

				if (newPageNumber == i1) {

					b[i1].setBackgroundResource(R.drawable.things_to_know_06);

				} else {

					b[i1].setBackgroundResource(R.drawable.things_to_know_05);

				}

				b[i1].setLayoutParams(layoutParams);

				indicator.addView(b[i1]);

			}

		} catch (final Exception e) {

		}

	}
	public void closeGeneralActivityDetail(View v){
		finish();
		
	}


}
