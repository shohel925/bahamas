package com.theislandofthebahamas.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.Executors;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aapbd.utils.nagivation.StartActivity;
import com.aapbd.utils.network.AAPBDHttpClient;
import com.aapbd.utils.network.NetInfo;
import com.aapbd.utils.notification.AlertMessage;
import com.aapbd.utils.notification.BusyDialog;
import com.aapbd.utils.storage.PersistData;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.TransportActivity;
import com.theislandofthebahamas.adapter.GeneralDetailsPagerAdapter;
import com.theislandofthebahamas.adapter.WeatherPagerAdapter;
import com.theislandofthebahamas.datamodel.IslandImageInfo;
import com.theislandofthebahamas.datamodel.MapInfo;
import com.theislandofthebahamas.datamodel.TransportInfo;
import com.theislandofthebahamas.datamodel.TransportIslandInfo;
import com.theislandofthebahamas.datamodel.TransportResult;
import com.theislandofthebahamas.datamodel.TransportTableData;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.TransportResponse;
import com.theislandofthebahamas.response.WeatherResponse;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.CustomDurationViewPager;

public class IslandMoreFragment extends Fragment{

	private static View v;
	GoogleMap mGoogleMap;
	MapView mapView;
	Context con;
	WeatherResponse mWeatherResponse;
	CustomDurationViewPager mainWeatherViewPager;
	List<MapInfo> mData;
	MapView mvShowLoc;
	private boolean mapsSupported = true;
	RelativeLayout RelativeTransportation;
	TextView textviewTransPortation,islandOverview,textviewDiscoverMore,textviewMoreDetails;
	
	//variable for transport activity
	TransportResponse mTransportResponse;
	DatabaseHandler db;
	ProgressBar progerssIslandMore;
	Handler handler;
	Runnable Update;
	Timer swipeTimer;
	boolean isTimerRunning=false;
	ImageView imageViewLogo;
	ViewPager viewpagerMore;
	LinearLayout linearIndicator;
	Vector<IslandImageInfo>tempImageList;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if(v != null){
			ViewGroup parent = (ViewGroup) v.getParent();
			if(parent != null){
				parent.removeView(v);
			}
		}
		try{
			v = inflater.inflate(R.layout.fragment_island_more, container, false);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		con=getActivity();
		mapView = (MapView) v.findViewById(R.id.mvShowLoc);
		
		db = new DatabaseHandler(con);
		linearIndicator=(LinearLayout)v.findViewById(R.id.linearIndicator);
		viewpagerMore=(ViewPager)v.findViewById(R.id.viewpagerMore);
		textviewMoreDetails=(TextView)v.findViewById(R.id.textviewMoreDetails);
		imageViewLogo=(ImageView)v.findViewById(R.id.imageViewLogo);
		textviewDiscoverMore=(TextView)v.findViewById(R.id.textviewDiscoverMore);
		islandOverview=(TextView)v.findViewById(R.id.islandOverview);
		progerssIslandMore=(ProgressBar)v.findViewById(R.id.progerssIslandMore);
		mainWeatherViewPager=(CustomDurationViewPager)v.findViewById(R.id.mainWeatherViewPager);
		Typeface tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		textviewTransPortation=(TextView)v.findViewById(R.id.textviewTransPortation);
		textviewMoreDetails.setTypeface(tf);
		textviewDiscoverMore.setTypeface(tf);
		textviewTransPortation.setTypeface(tf);
		RelativeTransportation=(RelativeLayout)v.findViewById(R.id.RelativeTransportation);
		if(!TextUtils.isEmpty(AppConstant.mIslandList.getLogo())){
			Picasso.with(con).load(AppConstant.mIslandList.getLogo()).into(imageViewLogo);
		}
		textviewMoreDetails.setText(AppConstant.mIslandList.getDescription().replaceAll("\\<[^>]*>",""));
		textviewDiscoverMore.setText(AppConstant.mIslandList.getDiscover_more().replaceAll("\\<[^>]*>",""));
		tempImageList=new Vector<IslandImageInfo>();
		if(tempImageList!=null){
			tempImageList.removeAllElements();
		}
		tempImageList=db.getAllIslandImage(PersistData.getStringData(con, AppConstant.islandID), "discover_more_images");
		viewpagerMore.setAdapter(new GeneralDetailsPagerAdapter(con, tempImageList));
		viewpagerMore.setCurrentItem(0);
		controlDots(0);
		
		viewpagerMore.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int pos) {
				// TODO Auto-generated method stub

				controlDots(pos);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		getWeatherData(AllURL.getWeatherUrl());
		RelativeTransportation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				final String time = "" + System.currentTimeMillis();

				if (!PersistData.getStringData(con, AppConstant.transportFirstHeat).equalsIgnoreCase("Yes")) {
					 getTransportData(AllURL.getTransportApiUrl());
					
					PersistData.setStringData(con, AppConstant.previousTime, time);
					Log.d("Tag","first time run");

				} else {
					final long timecurrent = Long.parseLong(time);
					final long previousTime = Long.parseLong(PersistData.getStringData(con, AppConstant.previousTime));

					if ((timecurrent - previousTime) > (30L*24L*60L*60L*1000L)) {
						PersistData.setStringData(con, AppConstant.previousTime, time);
						 getTransportData(AllURL.getTransportApiUrl());
						 Log.d("Tag","time greater than 30 day");
					}else{
						AppConstant.tempIsland=PersistData.getStringData(con, AppConstant.islandID);
						
						StartActivity.toActivity(con, TransportActivity.class);
						 Log.d("Tag","less than 30 day");
						
					}
				}
				
			}
		});
		
		mainWeatherViewPager.setScrollDurationFactor(AppConstant.IMAGE_SLIDE_ANIMATION_SPEED);
		mainWeatherViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int state) {

				if (state == ViewPager.SCROLL_STATE_DRAGGING) {
					isTimerRunning = false;
					swipeTimer.cancel();
					System.out.println("ViewPager: Timer stop");
				} else {
					if (!isTimerRunning) {
						createSwipeTimer();
						System.out.println("ViewPager: Timer start");
						isTimerRunning = true;
					}
				}
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
			}

		});
		
		return v;
	}
	
	protected void controlDots(int newPageNumber) {

		try {
			linearIndicator.removeAllViews(); // simple linear layout

			final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(

			android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT);

			layoutParams.setMargins(7, 2, 0, 2);

			final ImageView[] b = new ImageView[tempImageList.size()];

			// Log.e("Shop size ", ">>>" + db.getAllShoppingName().size());

			for (int i1 = 0; i1 < tempImageList.size(); i1++) {

				b[i1] = new ImageView(con);

				b[i1].setId(1000 + i1);

				if (newPageNumber == i1) {

					b[i1].setBackgroundResource(R.drawable.things_to_know_06);

				} else {

					b[i1].setBackgroundResource(R.drawable.things_to_know_05);

				}

				b[i1].setLayoutParams(layoutParams);

				linearIndicator.addView(b[i1]);

			}

		} catch (final Exception e) {

		}

	}
	 private void initializeMap() {
	        if (mGoogleMap == null && mapsSupported) {
	            mapView = (MapView) v.findViewById(R.id.mvShowLoc);
	            mGoogleMap = mapView.getMap();
	            //setup markers etc...
	        }
	    }
	
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
			
			MapsInitializer.initialize(getActivity());
		
		    if (mapView != null) {
		        mapView.onCreate(savedInstanceState);
		    }
		    initializeMap();
		    Handler hadler =new Handler();
		    hadler.postDelayed(new Runnable() {
				
				@Override
				public void run() {
				loadMapData();
				}
			}, 1000);
		}
	
	
	private void loadMapData() {
		if(mGoogleMap!=null){
			mGoogleMap.clear();
		}
		mData =new ArrayList<MapInfo>();
		if(mData!=null){
			mData.clear();
		}
		
		MapInfo mInfo= new MapInfo();
		mInfo.setLat(26.415563);
		mInfo.setLon(-80.788666);
		mData.add(mInfo);
		
		MapInfo mInfo2= new MapInfo();
		mInfo2.setLat(20.894128);
		mInfo2.setLon(-72.554750);
		mData.add(mInfo2);
		Log.e("map size", ">>"+mData.size());
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		for (int i = 0; i < mData.size(); i++) {
			LatLng currentPosition = new LatLng(mData.get(i).getLat(), mData.get(i).getLon());
//			 mGoogleMap.addMarker(new MarkerOptions().position(currentPosition));
			builder.include(currentPosition);
		}
		try {
			
				LatLngBounds bounds = builder.build();
				int padding = 100;
				CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
				mGoogleMap.moveCamera(cu);
				mGoogleMap.animateCamera(cu);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	 @Override
	    public void onResume() {
	        super.onResume();
	        mapView.onResume();
	        initializeMap();
	    }

	    @Override
	    public void onPause() {
	        super.onPause();
	        mapView.onPause();
	    }

	    @Override
	    public void onDestroy() {
	        super.onDestroy();
	        mapView.onDestroy();
	    }

	    @Override
	    public void onLowMemory() {
	        super.onLowMemory();
	        mapView.onLowMemory();
	    }
	    
	    @Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        mapView.onSaveInstanceState(outState);
	    }
	
	 
	    private void getWeatherData(final String url) {
			// TODO Auto-generated method stub
			 if (!NetInfo.isOnline(con)) {
					AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
						return;
					}
			 
			 	Log.e("URL : ", url);

//		       final BusyDialog busyNow = new BusyDialog(con, true,false);
//		        busyNow.show();
			 	progerssIslandMore.setVisibility(View.VISIBLE);

			        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
			        	
			        	String response="";
						
						@Override
						public void run() {
							try {
								response=AAPBDHttpClient.post(url).body();
							}
							catch (Exception e) {
								// TODO: handle exception
								 Log.e("MYAPP", "exception", e);
//								 if(busyNow!=null){
//									 busyNow.dismis();
//								 }
								 progerssIslandMore.setVisibility(View.GONE);
							}

							getActivity().runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
//									if(busyNow!=null){
//										 busyNow.dismis();
//									 }
									progerssIslandMore.setVisibility(View.GONE);
									try {
					                    Log.e("Response", ">>" + new String(response));
					                    if (!TextUtils.isEmpty(new String(response))) {
					    					Gson g = new Gson();
					    					mWeatherResponse = g.fromJson(new String(response),WeatherResponse.class);
					    					
					    					if (mWeatherResponse.getStatus().equalsIgnoreCase("true")) {
					    						
//					    						for(WeatherInfo wInfo:mWeatherResponse.getResult().getWeather()){
//					    							Log.e("temp", ">>"+wInfo.getTemp_f());
//					    							
//					    						}
					    						mainWeatherViewPager.setAdapter(new WeatherPagerAdapter(con, mWeatherResponse.getResult().getWeather()));
					    						mainWeatherViewPager.setCurrentItem(0);
					    						handler = new Handler();

					    						 Update = new Runnable() {
					    							@Override
					    							public void run() {

					    								if (mWeatherResponse.getResult().getWeather() != null && mWeatherResponse.getResult().getWeather().size() > 0) {
					    									int currentImg = mainWeatherViewPager.getCurrentItem();
					    									currentImg++;
					    									if (currentImg == mWeatherResponse.getResult().getWeather().size()) {
					    										currentImg = 0;
					    									}
					    									mainWeatherViewPager.setCurrentItem(currentImg, true);
					    								}
					    							}
					    						};
					    						createSwipeTimer();
					    						isTimerRunning=true;
												
											} else {
												AlertMessage.showMessage(con, getString(R.string.app_name), mWeatherResponse.getMessage() + "");

											}
					                    }


					                } catch (final Exception e) {

					                    e.printStackTrace();
					                }
									
									
								}
							});
						}
					});
			
			
		}
	 
		private void createSwipeTimer() {
			swipeTimer = new Timer();
			swipeTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					handler.post(Update);
				}
			}, 4000, 4000);
		}
	 public void gotoTransportActivity(View v)
	 {
		
	 }

	 private void getTransportData(final String url) {
			// TODO Auto-generated method stub
			if (!NetInfo.isOnline(con)) {
				AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
					return;
			}
	
		    Log.e("URL : ", url);
		    final BusyDialog busyNow = new BusyDialog(con, true,false);
	        busyNow.show();	        
				        
	        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
	            String response="";
	            @Override
	            public void run() {
	                
	                /* call API and Do background task.*/	
	            	try {
	            		response= AAPBDHttpClient.post(url).body();		                
		                Log.e("Response", ">>" + new String(response));
					} catch (Exception e) {
						// TODO: handle exception
					}
	                
	                if (!TextUtils.isEmpty(new String(response))) {
						Gson g = new Gson();
						
						mTransportResponse = g.fromJson(new String(response),TransportResponse.class);
						
						if (mTransportResponse.getStatus().equalsIgnoreCase("true"))
						{
							PersistData.setStringData(con, AppConstant.transportFirstHeat, "Yes");
							TransportResult result = mTransportResponse.getResult(); 
							db.deleteAllTransportInfo();
								
						
							//clearing database
							
							for(TransportIslandInfo til:result.getHtga())
							{
								String isLandId = til.getIslandid();
								List<TransportInfo> transportInfoList=til.getTransportlist();
								for(TransportInfo ti:transportInfoList)
								{
									String trInfo = ti.getTransportInfo();
									String trType = ti.getTransportType();
									//Log.d("data sending to db","htga "+isLandId+" "+trType+" "+trInfo);
									TransportTableData ttd = new TransportTableData("htga", isLandId, trType, trInfo);
									db.addTransportData(ttd);
									
								}
							}
							for(TransportIslandInfo til:result.getHtgh())
							{
								String isLandId = til.getIslandid();
								List<TransportInfo> transportInfoList=til.getTransportlist();
								for(TransportInfo ti:transportInfoList)
								{
									String trInfo = ti.getTransportInfo();
									String trType = ti.getTransportType();
									//Log.d("data sending to db","htgh "+isLandId+" "+trType+" "+trInfo);
									TransportTableData ttd = new TransportTableData("htgh", isLandId, trType, trInfo);
									db.addTransportData(ttd);
									
								}
							 }
						}
								
	
						
							
	                
	           
		                /* Back to main thread/UI*/
		                
		                getActivity().runOnUiThread(new Runnable() {
		                    @Override
		                    public void run() {	                        
		                        /* Update your UI*/
		                    	if (busyNow != null) {
		    	                    busyNow.dismis();
		    	                }
		                    	
		                    	AppConstant.tempIsland=PersistData.getStringData(con, AppConstant.islandID);
		                    	Log.d("island island island id",AppConstant.tempIsland);
		                    	
		                    	StartActivity.toActivity(con, TransportActivity.class);
		                        
		                    }
		                });
	                }
	                
	            }
	        });
			
		
	}
 
	
}
