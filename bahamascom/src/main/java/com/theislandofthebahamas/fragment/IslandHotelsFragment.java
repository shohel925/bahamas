package com.theislandofthebahamas.fragment;

import java.util.Vector;
import java.util.concurrent.Executors;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aapbd.utils.network.AAPBDHttpClient;
import com.aapbd.utils.network.NetInfo;
import com.aapbd.utils.notification.AlertMessage;
import com.aapbd.utils.notification.BusyDialog;
import com.aapbd.utils.storage.PersistData;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.IslandMainActivity;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.GeneralHotelInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.GeneralHotelResponse;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AppConstant;

public class IslandHotelsFragment extends Fragment {


	ListView listviewIslandHotel;
	
	Vector<GeneralHotelInfo> hotelList;
	
	Context context, con;
	DatabaseHandler db;
	MapView islandHotelMap;
	GoogleMap hGoogleMap;
	private boolean mapsSupported = true;
	View v;
	ProgressBar progressHotel;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 v = inflater.inflate(R.layout.fragment_island_hotels, container, false);
		context = con = getActivity();
		db=new DatabaseHandler(con);
		if(hotelList!=null){
			hotelList.removeAllElements();
		}
		progressHotel=(ProgressBar)v.findViewById(R.id.progressHotel);
		listviewIslandHotel = (ListView)v.findViewById(R.id.listviewIslandHotel);
		islandHotelMap = (MapView) v.findViewById(R.id.islandHotelMap);
		final String time = "" + System.currentTimeMillis();
		if (!PersistData.getBooleanData(con, AppConstant.isHotelHead)) {
			getHotelData(AllURL.getHotelListUrl());
			PersistData.setStringData(con, AppConstant.previousTime, time);

		} else {
			final long timecurrent = Long.parseLong(time);
			final long previousTime = Long.parseLong(PersistData.getStringData(con, AppConstant.previousTime));

			if ((timecurrent - previousTime) > (30L*24L*60L*60L*1000L)) {
				if(NetInfo.isOnline(con)){
					PersistData.setStringData(con, AppConstant.previousTime, time);
					getHotelData(AllURL.getHotelListUrl());
				}else{
					hotelList=db.getIslandHotelListFromIslandId(PersistData.getStringData(con, AppConstant.islandID));
					if(hotelList.size()>0){
						listviewIslandHotel.setAdapter(new HotelAdapter());
//						Helper.getListViewSize(listviewIslandHotel);
					}
				}
			}else{
				hotelList=db.getIslandHotelListFromIslandId(PersistData.getStringData(con, AppConstant.islandID));
				if(hotelList.size()>0){
					listviewIslandHotel.setAdapter(new HotelAdapter());
//					Helper.getListViewSize(listviewIslandHotel);
				}
				
			}
		}
		listviewIslandHotel.setOnItemClickListener(hotelClickListener);
		initUi();
		
		return v;
	}
	
	 private void getHotelData(final String url) {
			// TODO Auto-generated method stub
			 if (!NetInfo.isOnline(con)) {
					AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
						return;
					}
			 
			 	Log.e("URL : ", url);

//		       final BusyDialog busyNow = new BusyDialog(con, true,false);
//		        busyNow.show();
			 	progressHotel.setVisibility(View.VISIBLE);

			        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
			        	
			        	String response="";
						
						@Override
						public void run() {
							try {
								response=AAPBDHttpClient.post(url).body();
							}
							catch (Exception e) {
								// TODO: handle exception
								 Log.e("MYAPP", "exception", e);
//								 if(busyNow!=null){
//									 busyNow.dismis();
//								 }
								 progressHotel.setVisibility(View.GONE);
							}

							getActivity().runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									 progressHotel.setVisibility(View.GONE);
									try {
					                    Log.e("Response", ">>" + new String(response));
					                    if (!TextUtils.isEmpty(new String(response))) {
					    					Gson g = new Gson();
					    					final GeneralHotelResponse mGeneralHotelResponse = g.fromJson(new String(response),GeneralHotelResponse.class);
					    					
					    					if (mGeneralHotelResponse.getStatus().equalsIgnoreCase("true")) {
					    						PersistData.setBooleanData(con, AppConstant.isHotelHead, true);
					    						Log.e("Server hotel Size", ">>"+mGeneralHotelResponse.getResult().size());

												for(int i=0;i<mGeneralHotelResponse.getResult().size(); i++){
													if(!TextUtils.isEmpty(mGeneralHotelResponse.getResult().get(i).getId())){
										        	if(!db.ifHotelExist(mGeneralHotelResponse.getResult().get(i).getId())){
														db.addGeneralHotel(mGeneralHotelResponse.getResult().get(i));
													}
										        }
												}
												hotelList=db.getIslandHotelListFromIslandId(PersistData.getStringData(con, AppConstant.islandID));
												Log.e("hotel Size", ">>"+hotelList.size());
												listviewIslandHotel.setAdapter(new HotelAdapter());
//												Helper.getListViewSize(lstHotelList);
												
											} else {
												AlertMessage.showMessage(con, getString(R.string.app_name), mGeneralHotelResponse.getMessage() + "");

											}
					                    }


					                } catch (final Exception e) {

					                    e.printStackTrace();
					                }
									
									
								}
							});
						}
					});
			
			
		}
	
	
	private void initUi() {
		// TODO Auto-generated method stub
		IslandMainActivity.getInstance().islandMap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(listviewIslandHotel.getVisibility()==View.VISIBLE){
					IslandMainActivity.getInstance().islandMap.setImageResource(R.drawable.island_hotel_directions_map_06);;
					listviewIslandHotel.setVisibility(View.GONE);
					islandHotelMap.setVisibility(View.VISIBLE);
					Handler handler=new Handler();
					handler.postDelayed(new Runnable() {
						
						@Override
						public void run() {
							loadMapData();	
						}
					}, 1000);
					
				}else{
					IslandMainActivity.getInstance().islandMap.setImageResource(R.drawable.island_hotel_directions_map_06);;
					listviewIslandHotel.setVisibility(View.VISIBLE);
					islandHotelMap.setVisibility(View.GONE);
				}
				
			}
		});
	}
	
	 private void initializeMap() {
	        if (hGoogleMap == null && mapsSupported) {
	        	islandHotelMap = (MapView) v.findViewById(R.id.islandHotelMap);
	            hGoogleMap = islandHotelMap.getMap();
	            //setup markers etc...
	        }
	    }
	
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
			
			MapsInitializer.initialize(getActivity());
		
		    if (islandHotelMap != null) {
		    	islandHotelMap.onCreate(savedInstanceState);
		    }
		    initializeMap();
		}
	
	
	private void loadMapData() {
		if(hGoogleMap!=null){
			hGoogleMap.clear();
		}
		Log.e("map size", ">>"+hotelList.size());
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		for (int i = 0; i < hotelList.size(); i++) {
			if(((!hotelList.get(i).getLat().trim().equalsIgnoreCase("false"))&&(!hotelList.get(i).getLon().trim().equalsIgnoreCase("false")))){
				Log.e("map true", ">>"+hotelList.size());
				LatLng currentPosition = new LatLng(Double.parseDouble(hotelList.get(i).getLat()), Double.parseDouble(hotelList.get(i).getLon()));
				hGoogleMap.addMarker(new MarkerOptions().position(currentPosition).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
						.title(hotelList.get(i).getName()));
				builder.include(currentPosition);	
			}
		}
		try {
				LatLngBounds bounds = builder.build();
				int padding = 100;
				CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
				hGoogleMap.animateCamera(cu);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	 @Override
	    public void onResume() {
	        super.onResume();
	        islandHotelMap.onResume();
	        initializeMap();
	    }

	    @Override
	    public void onPause() {
	        super.onPause();
	        islandHotelMap.onPause();
	    }

	    @Override
	    public void onDestroy() {
	        super.onDestroy();
	        islandHotelMap.onDestroy();
	    }

	    @Override
	    public void onLowMemory() {
	        super.onLowMemory();
	        islandHotelMap.onLowMemory();
	    }
	    
	    @Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        islandHotelMap.onSaveInstanceState(outState);
	    }


	private class HotelAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return hotelList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return hotelList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.island_hotel_rows, parent, false);
			}
			
			GeneralHotelInfo query=hotelList.get(position);
			Typeface medium = Typeface.createFromAsset(context.getAssets(), "font/AvenirNextLTPro-Medium.otf");
			Typeface demiBold = Typeface.createFromAsset(context.getAssets(), "font/AvenirNextLTPro-Demi.otf");
			
			ImageView imageViewIslandHotelRows = (ImageView) convertView.findViewById(R.id.imageViewIslandHotelRows);
			TextView textViewIslandHotelTitle = (TextView) convertView.findViewById(R.id.textViewIslandHotelTitle);
			TextView textviewIslandAddress = (TextView) convertView.findViewById(R.id.textviewIslandAddress);
			TextView hotelPHone=(TextView)convertView.findViewById(R.id.hotelPHone);
			Picasso.with(context).load(query.getImage_url()).into(imageViewIslandHotelRows);
			textViewIslandHotelTitle.setText(query.getName());
			textviewIslandAddress.setText(query.getAddress());
			hotelPHone.setText(query.getPhone());
			textViewIslandHotelTitle.setTypeface(demiBold);
			textviewIslandAddress.setTypeface(medium);
			hotelPHone.setTypeface(medium);
			
			return convertView;
		}
		
	}
	
	private OnItemClickListener hotelClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			Toast.makeText(con, "Not Implemented", Toast.LENGTH_LONG).show();
//			Intent intent = new Intent(context, GeneralHotelDetailActivity.class);
//			intent.putExtra(AppConstant.SELECTED_HOTEL_ID, hotelList.get(position).getHotelId());
//			startActivity(intent);
		}
	};
}
