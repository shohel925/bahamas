package com.theislandofthebahamas.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aapbd.utils.nagivation.StartActivity;
import com.aapbd.utils.storage.PersistData;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.IslandMainActivity;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.IslandList;
import com.theislandofthebahamas.datamodel.TempIslandInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.util.AppConstant;

public class GeneralHomeFragment extends Fragment {

	ListView lstIslandList;
	Vector<IslandList> islandList;
	
	Context context, con;
	DatabaseHandler db;
	TextView textViewHomeFagment;
	//ViewGroup homeCustomLayout;
	List<TempIslandInfo>imagaList;
	View customView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View v = inflater.inflate(R.layout.fragment_general_home, container, false);
		
		context = con = getActivity();
		db=new DatabaseHandler(con);
		//homeCustomLayout = (ViewGroup) v.findViewById(R.id.homeCustomLayout);
		lstIslandList = (ListView)v.findViewById(R.id.lstIslandList);
		if(islandList!=null){
			islandList.removeAllElements();
		}
		imagaList=new ArrayList<TempIslandInfo>();
		if(imagaList!=null){
			imagaList.clear();
		}
		imagaList=AppConstant.tempIslandList();
		
		Typeface tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		
		islandList=db.getAllIsland();
		Log.e("islandList size", ">>"+islandList.size());

		
		View header = ((Activity) con).getLayoutInflater().inflate(R.layout.list_header, null);
		textViewHomeFagment=(TextView)header.findViewById(R.id.textViewHomeFagment);
		textViewHomeFagment.setTypeface(tf);
		lstIslandList.addHeaderView(header);
		lstIslandList.setAdapter(new IslandListAdapter());
		return v;
	}
	private class IslandListAdapter extends BaseAdapter{
		
		private class ViewHolder {
			ImageView imgVwIslandPic;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imagaList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return imagaList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			final ViewHolder holder;
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_island_list, parent, false);
				
				holder = new ViewHolder();
				holder.imgVwIslandPic = (ImageView)convertView.findViewById(R.id.imgVwIslandPic);
//				holder.imgVwIslandPic.setImageResource(imagaList.get(position));
//				
				convertView.setTag(holder);
			}else{
				holder = (ViewHolder) convertView.getTag();
			}
			Picasso.with(context).load(imagaList.get(position).getImage()).into(holder.imgVwIslandPic);
			
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					AppConstant.mIslandList=islandList.elementAt(position);
					PersistData.setStringData(con, AppConstant.islandID, islandList.elementAt(position).getId());
					AppConstant.islandColor=imagaList.get(position).getColor();
					StartActivity.toActivity(context, IslandMainActivity.class);

				}
			});
			
			return convertView;
		}
		
	}
//	
//	private OnItemClickListener islandClick = new OnItemClickListener() {
//
//		@Override
//		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//			// TODO Auto-generated method stub
//			AppConstant.islandId=islandList.elementAt(position).getId();
//			AppConstant.islandName=islandList.elementAt(position).getName();
//			StartActivity.toActivity(context, IslandMainActivity.class);
//		}
//	};
}
