package com.theislandofthebahamas.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Executors;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.aapbd.utils.nagivation.StartActivity;
import com.aapbd.utils.network.AAPBDHttpClient;
import com.aapbd.utils.network.NetInfo;
import com.aapbd.utils.notification.AlertMessage;
import com.aapbd.utils.notification.BusyDialog;
import com.aapbd.utils.storage.PersistData;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.DivingDirListActivity;
import com.theislandofthebahamas.FeatureIslandDetailActivity;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;
import com.theislandofthebahamas.datamodel.GeneralActivityIslandInfo;
import com.theislandofthebahamas.datamodel.SubcategoryInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.CategoryInfo;
import com.theislandofthebahamas.response.DivingDirectoryResponse;
import com.theislandofthebahamas.response.FilterRespone;
import com.theislandofthebahamas.response.GeneralActivityList;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AppConstant;

public class IslandActivitiesFragment extends Fragment {

	GridView gvActivities;
	
	Vector<GeneralActivityList> activityList,tempList;
	Context context, con;
	DatabaseHandler db;
	List<Integer> imgList;
	View view;
	DivingDirectoryResponse mDivingDirectoryResponse;
	Vector<GeneralActivityIslandInfo> getFeactureIslandList;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 view = inflater.inflate(R.layout.fragment_general_activities, container, false);
		context = con = getActivity();
		db=new DatabaseHandler(con);
	
		intiUi();
		
		return view;
	}
	
	private void intiUi() {
		// TODO Auto-generated method stub
		
		if(activityList!=null){
			activityList.removeAllElements();
		}
		activityList=db.getAllGeneralActivity();
		gvActivities = (GridView) view.findViewById(R.id.generalGridView);
		
		tempList=new Vector<GeneralActivityList>();
		if(tempList!=null){
			tempList.removeAllElements();
		}
		
		for(int i=0; i<activityList.size(); i++){
			if(activityList.get(i).getId().equalsIgnoreCase("52")){
				
			}else{
				if(activityList.get(i).getDescription().length()>0){
					tempList.add(activityList.get(i));
				}
			}
		}
		imgList=new ArrayList<Integer>();
		if(imgList!=null){
			imgList.clear();
		}
		imgList.add(R.drawable.img_home_activity_beaches);
		imgList.add(R.drawable.img_home_activity_boating);
		imgList.add(R.drawable.img_home_activity_convention);
		imgList.add(R.drawable.img_home_activity_dining);
		imgList.add(R.drawable.img_home_activity_diving);
		imgList.add(R.drawable.img_home_activity_ecoturism);
		imgList.add(R.drawable.img_home_activity_family_fun);
		imgList.add(R.drawable.img_home_activity_fishing);
		imgList.add(R.drawable.img_home_activity_honeymoons);
		imgList.add(R.drawable.img_home_activity_nightlife);
		imgList.add(R.drawable.img_home_activity_pampering);
		imgList.add(R.drawable.img_home_activity_private_flying);
		imgList.add(R.drawable.img_home_activity_romantic_gateways);
		imgList.add(R.drawable.img_home_activity_shoping);
		imgList.add(R.drawable.img_home_activity_sightseeing);
		imgList.add(R.drawable.img_home_activity_sports);
		imgList.add(R.drawable.img_home_activity_water_sports);
		imgList.add(R.drawable.img_home_activity_wedding);
		gvActivities.setAdapter(new ActivityAdapter());
		gvActivities.setOnItemClickListener(generalActivityClick);
		
	}

	private class ActivityAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return tempList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return tempList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_activity_list, parent, false);
			}
			
			ImageView imgActivityPic = (ImageView) convertView.findViewById(R.id.imgActivityPic);
			TextView txtActivityName = (TextView) convertView.findViewById(R.id.txtActivityName);
			
			Picasso.with(context).load(imgList.get(position)).into(imgActivityPic);
			txtActivityName.setText(tempList.get(position).getName().toUpperCase());
			
			return convertView;
		}
		
	}
	
	private OnItemClickListener generalActivityClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
//			Toast.makeText(con, "Not Implemented", Toast.LENGTH_LONG).show();
			GeneralActivityList query=tempList.get(position);
			AppConstant.mGeneralActivityList=query;
			getFeactureIslandList=new Vector<GeneralActivityIslandInfo>();
			getFeactureIslandList=db.getFeactureListFromActivityAndIslandId(query.getId(),PersistData.getStringData(con, AppConstant.islandID));
			if(getFeactureIslandList.size()>0){
				AppConstant.mGeneralActivityIslandInfo=getFeactureIslandList.get(0);
				StartActivity.toActivity(con, FeatureIslandDetailActivity.class);
			}else{
				final String time = "" + System.currentTimeMillis();
				if (!PersistData.getStringData(con, AppConstant.directoryFirstHeat).equalsIgnoreCase("Yes")) {
					getFilterData(AllURL.getFilterApiUrl());
					PersistData.setStringData(con, AppConstant.previousTime, time);

				} else {
					final long timecurrent = Long.parseLong(time);
					final long previousTime = Long.parseLong(PersistData.getStringData(con, AppConstant.previousTime));

					if ((timecurrent - previousTime) > (30L*24L*60L*60L*1000L)) {
						if(NetInfo.isOnline(con)){
							PersistData.setStringData(con, AppConstant.previousTime, time);
							getFilterData(AllURL.getFilterApiUrl());
						}else{
							StartActivity.toActivity(con, DivingDirListActivity.class);
						}
						
					}else{
						/// for local ============
						StartActivity.toActivity(con, DivingDirListActivity.class);
						
					}
				}
				
			}
			
		}
	};
	
	protected void getDivingDirectoryData(final String url) 
	 {
		 if (!NetInfo.isOnline(con)) {
				AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
					return;
			}

	     Log.e("URL : ", url);
	    final BusyDialog busyNow = new BusyDialog(con, true,false);
	     busyNow.show();	        
				        
	     Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
	         String response="";
	         @Override
	         public void run() {
	             
	             /* call API and Do background task.*/	
	         	try {
		         		response= AAPBDHttpClient.post(url).body();		                
			                Log.e("Response", ">>" + new String(response));
						} catch (Exception e) {
							// TODO: handle exception
					}
	             
		             if (!TextUtils.isEmpty(new String(response))) {
							
		            	 try 
		            	 {
			                    
		            		Log.e("Response", ">>" + new String(response));
							Gson g = new Gson();
							
							mDivingDirectoryResponse = g.fromJson(new String(response),DivingDirectoryResponse.class);
							
			                    
			            } 
		            	catch (final Exception e)
		            	{
		
			               e.printStackTrace();
			            }
	            	 	
		                /* Back to main thread/UI*/
		                
		                getActivity().runOnUiThread(new Runnable() {
		                    @Override
		                    public void run() {	                        
		                        /* Update your UI*/
		                    	if (busyNow != null) {
		    	                    busyNow.dismis();
		    	                }
		                    	if (mDivingDirectoryResponse.getStatus().equalsIgnoreCase("true")) {
		                    		PersistData.setStringData(con, AppConstant.directoryFirstHeat, "Yes");
									for(DivingDirectoryInfo dlist:mDivingDirectoryResponse.getResult()){
										if(!db.ifDivingDirExist(dlist.getId())){
											db.addDivingDir(dlist);
											for(String activityId:dlist.getActivity_ids()){
												db.addCategoryDirectory(dlist.getId(), activityId);
											}
										}
									}
									
									StartActivity.toActivity(con, DivingDirListActivity.class);
								} 
								else {
									AlertMessage.showMessage(con, getString(R.string.app_name), mDivingDirectoryResponse.getMessage() + "");
								}
		                    	
		                        
		                    }
		                });
	             }
	             
	         }
	     });
			
	 }
	
	private void getFilterData(final String url) {
		// TODO Auto-generated method stub
		 if (!NetInfo.isOnline(con)) {
				AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
					return;
				}

		        Log.e("URL : ", url);
		        final BusyDialog busyNow = new BusyDialog(con, true,false);
		        busyNow.show();
		        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
		        	
		        	String response="";
					
					@Override
					public void run() {
						try {
							response=AAPBDHttpClient.post(url).body();
						}
						catch (Exception e) {
							// TODO: handle exception
							 Log.e("MYAPP", "exception", e);
							 if(busyNow!=null){
								 busyNow.dismis();
							 }
						}

						getActivity().runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								if(busyNow!=null){
									 busyNow.dismis();
								 }
								Log.e("response",">>"+response);
								Gson g=new Gson();
								FilterRespone mFilterRespone=g.fromJson(response, FilterRespone.class);
								if(mFilterRespone.getStatus().equalsIgnoreCase("true")){
									for(CategoryInfo cInfo:mFilterRespone.getResult()){
										if(!db.ifCategoryExist(cInfo.getId())){
											db.addCategory(cInfo.getId(), "", cInfo.getName());
											for(SubcategoryInfo sInfo:cInfo.getSubcategorylist()){
												db.addCategory(sInfo.getId(),cInfo.getId(),sInfo.getSubcategoryname());
											}
										}
									}
									getDivingDirectoryData(AllURL.getActivityDirectoryUrl());
								}else{
									AlertMessage.showMessage(con, getString(R.string.app_name), mFilterRespone.getMessage());
								}
								
								
								
							}
						});
					}
				});
		
		
	}
	
}
