package com.theislandofthebahamas.fragment;

import java.util.Vector;
import java.util.concurrent.Executors;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aapbd.utils.network.AAPBDHttpClient;
import com.aapbd.utils.network.NetInfo;
import com.aapbd.utils.notification.AlertMessage;
import com.aapbd.utils.notification.BusyDialog;
import com.aapbd.utils.storage.PersistData;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theislandofthebahamas.GeneralHotelDetailActivity;
import com.theislandofthebahamas.MainActivity;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.SplashActivity;
import com.theislandofthebahamas.datamodel.GeneralHotelInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.GeneralActivityList;
import com.theislandofthebahamas.response.GeneralActivityResponse;
import com.theislandofthebahamas.response.GeneralHotelResponse;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.Helper;

public class GeneralHotelsFragment extends Fragment {

ListView lstHotelList;
	
	Vector<GeneralHotelInfo> hotelList;
	
	Context context, con;
	TextView textViewHotel;
	Typeface tf,gothamBold;
	DatabaseHandler db;
	ProgressBar progressHotel;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fragment_general_hotels, container, false);
		context = con = getActivity();
		 db=new DatabaseHandler(con);
		 tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		 gothamBold = Typeface.createFromAsset(con.getAssets(), "font/ufonts.com_gotham-bold.ttf");
		 if(hotelList!=null){
			 hotelList.removeAllElements();
		 }
		progressHotel=(ProgressBar)v.findViewById(R.id.progressHotel);
		textViewHotel=(TextView)v.findViewById(R.id.textViewHotel);
		textViewHotel.setTypeface(tf);
		lstHotelList = (ListView)v.findViewById(R.id.lstHotelList);
		lstHotelList.setOnItemClickListener(hotelClickListener);
		final String time = "" + System.currentTimeMillis();

		if (!PersistData.getBooleanData(con, AppConstant.isHotelHead)) {
			getHotelData(AllURL.getHotelListUrl());
			PersistData.setStringData(con, AppConstant.previousTime, time);

		} else {
			final long timecurrent = Long.parseLong(time);
			final long previousTime = Long.parseLong(PersistData.getStringData(con, AppConstant.previousTime));

			if ((timecurrent - previousTime) > (30L*24L*60L*60L*1000L)) {
				if(NetInfo.isOnline(con)){
					PersistData.setStringData(con, AppConstant.previousTime, time);
					getHotelData(AllURL.getHotelListUrl());
				}else{
					
					hotelList=db.getHotelListFromFeature("true");
					if(hotelList.size()>0){
						lstHotelList.setAdapter(new HotelAdapter());
						Helper.getListViewSize(lstHotelList);
					}
					
				}
			}else{
				hotelList=db.getHotelListFromFeature("true");
				if(hotelList.size()>0){
					lstHotelList.setAdapter(new HotelAdapter());
					Helper.getListViewSize(lstHotelList);
				}
				
			}
		}
		
		
		return v;
	}
	
	 
	 private void getHotelData(final String url) {
			// TODO Auto-generated method stub
			 if (!NetInfo.isOnline(con)) {
					AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
						return;
					}
			 
			 	Log.e("URL : ", url);

//		       final BusyDialog busyNow = new BusyDialog(con, true,false);
//		        busyNow.show();
			 	progressHotel.setVisibility(View.VISIBLE);

			        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
			        	
			        	String response="";
						
						@Override
						public void run() {
							try {
								response=AAPBDHttpClient.post(url).body();
							}
							catch (Exception e) {
								// TODO: handle exception
								 Log.e("MYAPP", "exception", e);
//								 if(busyNow!=null){
//									 busyNow.dismis();
//								 }
								 progressHotel.setVisibility(View.GONE);
							}

							getActivity().runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
//									if(busyNow!=null){
//										 busyNow.dismis();
//									 }
									 progressHotel.setVisibility(View.GONE);
									try {
					                    Log.e("Response", ">>" + new String(response));
					                    if (!TextUtils.isEmpty(new String(response))) {
					    					Gson g = new Gson();
					    					final GeneralHotelResponse mGeneralHotelResponse = g.fromJson(new String(response),GeneralHotelResponse.class);
					    					
					    					if (mGeneralHotelResponse.getStatus().equalsIgnoreCase("true")) {
					    						PersistData.setBooleanData(con, AppConstant.isHotelHead, true);
					    						Log.e("Server hotel Size", ">>"+mGeneralHotelResponse.getResult().size());

												for(int i=0;i<mGeneralHotelResponse.getResult().size(); i++){
													if(!TextUtils.isEmpty(mGeneralHotelResponse.getResult().get(i).getId())){
										        	if(!db.ifHotelExist(mGeneralHotelResponse.getResult().get(i).getId())){
														db.addGeneralHotel(mGeneralHotelResponse.getResult().get(i));
													}
										        }
												}
												hotelList=db.getHotelListFromFeature("true");
												Log.e("hotel Size", ">>"+hotelList.size());
												lstHotelList.setAdapter(new HotelAdapter());
												Helper.getListViewSize(lstHotelList);
												
											} else {
												AlertMessage.showMessage(con, getString(R.string.app_name), mGeneralHotelResponse.getMessage() + "");

											}
					                    }


					                } catch (final Exception e) {

					                    e.printStackTrace();
					                }
									
									
								}
							});
						}
					});
			
			
		}
	
	
	private class HotelAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return hotelList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return hotelList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_hotel_list, parent, false);
			}
			GeneralHotelInfo query=hotelList.get(position);
			ImageView imgHotelPic = (ImageView) convertView.findViewById(R.id.imgHotelPic);
			TextView txtHotelName = (TextView) convertView.findViewById(R.id.txtHotelName);
			TextView txtHotelAddress = (TextView) convertView.findViewById(R.id.txtHotelAddress);
			
			
			Picasso.with(context).load(query.getImage_url()).into(imgHotelPic);
			txtHotelName.setText(query.getName());
			txtHotelAddress.setText(query.getAddress());
			txtHotelName.setTypeface(gothamBold);
			txtHotelAddress.setTypeface(tf);
			
			return convertView;
		}
		
	}
	
	private OnItemClickListener hotelClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			GeneralHotelInfo query=hotelList.get(position);
			AppConstant.mGeneralHotelInfo=query;
			Intent intent = new Intent(context, GeneralHotelDetailActivity.class);
			startActivity(intent);
		}
	};
}
