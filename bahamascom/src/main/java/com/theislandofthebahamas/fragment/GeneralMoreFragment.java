package com.theislandofthebahamas.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

import android.os.Handler;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aapbd.utils.nagivation.StartActivity;
import com.aapbd.utils.network.AAPBDHttpClient;
import com.aapbd.utils.network.NetInfo;
import com.aapbd.utils.notification.AlertMessage;
import com.aapbd.utils.notification.BusyDialog;
import com.aapbd.utils.storage.PersistData;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.theislandofthebahamas.IslandListActivity;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.TransportActivity;
import com.theislandofthebahamas.WebActivity;
import com.theislandofthebahamas.adapter.WeatherPagerAdapter;
import com.theislandofthebahamas.datamodel.MapInfo;
import com.theislandofthebahamas.datamodel.TransportInfo;
import com.theislandofthebahamas.datamodel.TransportIslandInfo;
import com.theislandofthebahamas.datamodel.TransportResult;
import com.theislandofthebahamas.datamodel.TransportTableData;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.TransportResponse;
import com.theislandofthebahamas.response.WeatherInfo;
import com.theislandofthebahamas.response.WeatherResponse;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.CustomDurationViewPager;

public class GeneralMoreFragment extends Fragment{

	private static View v;
	GoogleMap mGoogleMap;
	MapView mapView;
	TextView textviewAboutUs,textviewGovement,textviewHistory,textviewholidays,textviewLocalCustoms,textviewLanguage
	,textviewPeople,textviewProximity,textviewTravelTips,tvIslandInfo,tvUsefullTxt;
	Context con;
	RelativeLayout aboutUs,govement,history,holidays,local_customs,language,people,proximity,travel_tips,layIslandInfo;
	WeatherResponse mWeatherResponse;
	CustomDurationViewPager mainWeatherViewPager;
	List<MapInfo> mData;
	MapView mvShowLoc;
	private boolean mapsSupported = true;
	TransportResponse mTransportResponse;
	DatabaseHandler db;
	ProgressBar moreProgress;
	Handler handler;
	Runnable Update;
	Timer swipeTimer;
	boolean isTimerRunning=false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if(v != null){
			ViewGroup parent = (ViewGroup) v.getParent();
			if(parent != null){
				parent.removeView(v);
			}
		}
		try{
			v = inflater.inflate(R.layout.fragment_general_more, container, false);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		con=getActivity();
		db=new DatabaseHandler(con);
		 mapView = (MapView) v.findViewById(R.id.mvShowLoc);
		 moreProgress=(ProgressBar)v.findViewById(R.id.moreProgress);
		mainWeatherViewPager=(CustomDurationViewPager)v.findViewById(R.id.mainWeatherViewPager);
		Typeface tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		textviewAboutUs=(TextView)v.findViewById(R.id.textviewAboutUs);
		textviewGovement=(TextView)v.findViewById(R.id.textviewGovement);
		textviewHistory=(TextView)v.findViewById(R.id.textviewHistory);
		textviewholidays=(TextView)v.findViewById(R.id.textviewholidays);
		textviewLocalCustoms=(TextView)v.findViewById(R.id.textviewLocalCustoms);
		textviewLanguage=(TextView)v.findViewById(R.id.textviewLanguage);
		textviewPeople=(TextView)v.findViewById(R.id.textviewPeople);
		textviewProximity=(TextView)v.findViewById(R.id.textviewProximity);
		textviewTravelTips=(TextView)v.findViewById(R.id.textviewTravelTips);
		tvIslandInfo = (TextView)v.findViewById(R.id.tvIslandInfo);
		textviewAboutUs.setTypeface(tf);
		textviewGovement.setTypeface(tf);
		textviewHistory.setTypeface(tf);
		textviewholidays.setTypeface(tf);
		textviewLocalCustoms.setTypeface(tf);
		textviewLanguage.setTypeface(tf);
		textviewPeople.setTypeface(tf);
		textviewProximity.setTypeface(tf);
		textviewTravelTips.setTypeface(tf);
		tvIslandInfo.setTypeface(tf);
        tvUsefullTxt = (TextView) v.findViewById(R.id.usefull_txt);
        tvUsefullTxt.setTypeface(tf);
		
		setOnclick();
		getWeatherData(AllURL.getWeatherUrl());
		
		mainWeatherViewPager.setScrollDurationFactor(AppConstant.IMAGE_SLIDE_ANIMATION_SPEED);

		mainWeatherViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int state) {

				if (state == ViewPager.SCROLL_STATE_DRAGGING) {
					isTimerRunning = false;
					swipeTimer.cancel();
					System.out.println("ViewPager: Timer stop");
				} else {
					if (!isTimerRunning) {
						createSwipeTimer();
						System.out.println("ViewPager: Timer start");
						isTimerRunning = true;
					}
				}
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
			}

		});

		
		return v;
	}
	 private void initializeMap() {
	        if (mGoogleMap == null && mapsSupported) {
	            mapView = (MapView) v.findViewById(R.id.mvShowLoc);
	            mGoogleMap = mapView.getMap();
	            //setup markers etc...
	        }
	    }
	
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onActivityCreated(savedInstanceState);
			
			MapsInitializer.initialize(getActivity());
		
		    if (mapView != null) {
		        mapView.onCreate(savedInstanceState);
		    }
		    initializeMap();
		    Handler hadler =new Handler();
		    hadler.postDelayed(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					loadMapData();	
				}
			}, 1000);
		}
	
	
	private void loadMapData() {
		if(mGoogleMap!=null){
			mGoogleMap.clear();
		}
		mData =new ArrayList<MapInfo>();
		if(mData!=null){
			mData.clear();
		}
		
		MapInfo mInfo= new MapInfo();
		mInfo.setLat(26.415563);
		mInfo.setLon(-80.788666);
		mData.add(mInfo);
		
		MapInfo mInfo2= new MapInfo();
		mInfo2.setLat(20.894128);
		mInfo2.setLon(-72.554750);
		mData.add(mInfo2);
		Log.e("map size", ">>"+mData.size());
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		for (int i = 0; i < mData.size(); i++) {
			LatLng currentPosition = new LatLng(mData.get(i).getLat(), mData.get(i).getLon());
//			 mGoogleMap.addMarker(new MarkerOptions().position(currentPosition));
			builder.include(currentPosition);
		}
		try {
			LatLngBounds bounds = builder.build();
			int padding = 50;
			CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
			mGoogleMap.animateCamera(cu);
		
	} catch (Exception ex) {
		ex.printStackTrace();
	}

	}
	
	 @Override
	    public void onResume() {
	        super.onResume();
	        mapView.onResume();
	        initializeMap();
	    }

	    @Override
	    public void onPause() {
	        super.onPause();
	        mapView.onPause();
	    }

	    @Override
	    public void onDestroy() {
	        super.onDestroy();
	        mapView.onDestroy();
	    }

	    @Override
	    public void onLowMemory() {
	        super.onLowMemory();
	        mapView.onLowMemory();
	    }
	    
	    @Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        mapView.onSaveInstanceState(outState);
	    }
	
	 private void setOnclick() {
		// TODO Auto-generated method stub
		 aboutUs=(RelativeLayout)v.findViewById(R.id.aboutUs);
		 aboutUs.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.more_discription="about_us.html";
				StartActivity.toActivity(con, WebActivity.class);
			}
		});
		 
		 govement=(RelativeLayout)v.findViewById(R.id.govement);
		 govement.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.more_discription="government.html";
				StartActivity.toActivity(con, WebActivity.class);
			}
		});
		 
		 history=(RelativeLayout)v.findViewById(R.id.history);
		 history.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.more_discription="history.html";
				StartActivity.toActivity(con, WebActivity.class);
			}
		});
		 
		 holidays=(RelativeLayout)v.findViewById(R.id.holidays);
		 holidays.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.more_discription="holidays.html";
				StartActivity.toActivity(con, WebActivity.class);
			}
		});
		 
		 local_customs=(RelativeLayout)v.findViewById(R.id.local_customs);
		 local_customs.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.more_discription="local_customs.html";
				StartActivity.toActivity(con, WebActivity.class);
			}
		});
		 
		 language=(RelativeLayout)v.findViewById(R.id.language);
		 language.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.more_discription="language.html";
				StartActivity.toActivity(con, WebActivity.class);
			}
		});
		 
		 people=(RelativeLayout)v.findViewById(R.id.people);
		 people.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.more_discription="people.html";
				StartActivity.toActivity(con, WebActivity.class);
			}
		});
		 proximity=(RelativeLayout)v.findViewById(R.id.proximity);
		 proximity.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.more_discription="proximity.html";
				StartActivity.toActivity(con, WebActivity.class);
			}
		});
		 
		 
		 travel_tips=(RelativeLayout)v.findViewById(R.id.travel_tips);
		 travel_tips.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppConstant.more_discription="travel_tips.html";
				StartActivity.toActivity(con, WebActivity.class);
			}
		});
		 
		 
		 layIslandInfo = (RelativeLayout)v.findViewById(R.id.islandInfo);
		 layIslandInfo.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					final String time = "" + System.currentTimeMillis();

					if (!PersistData.getStringData(con, AppConstant.transportFirstHeat).equalsIgnoreCase("Yes")) {
						 getTransportData(AllURL.getTransportApiUrl());
						
						PersistData.setStringData(con, AppConstant.previousTime, time);
						Log.d("Tag","first time run");

					} else {
						final long timecurrent = Long.parseLong(time);
						final long previousTime = Long.parseLong(PersistData.getStringData(con, AppConstant.previousTime));

						if ((timecurrent - previousTime) > (30L*24L*60L*60L*1000L)) {
							PersistData.setStringData(con, AppConstant.previousTime, time);
							 getTransportData(AllURL.getTransportApiUrl());
							 Log.d("Tag","time greater than 30 day");
						}else{
							
							StartActivity.toActivity(con, IslandListActivity.class);
							 Log.d("Tag","less than 30 day");
							
						}
					}
				}
			});
		 
		
	}
	
	 private void getTransportData(final String url) {
			// TODO Auto-generated method stub
			if (!NetInfo.isOnline(con)) {
				AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
					return;
			}
	
		    Log.e("URL : ", url);
		    final BusyDialog busyNow = new BusyDialog(con, true,false);
	        busyNow.show();	        
				        
	        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
	            String response="";
	            @Override
	            public void run() {
	                
	                /* call API and Do background task.*/	
	            	try {
	            		response= AAPBDHttpClient.post(url).body();		                
		                Log.e("Response", ">>" + new String(response));
					} catch (Exception e) {
						// TODO: handle exception
					}
	                
	                if (!TextUtils.isEmpty(new String(response))) {
						Gson g = new Gson();
						
						mTransportResponse = g.fromJson(new String(response),TransportResponse.class);
						
						if (mTransportResponse.getStatus().equalsIgnoreCase("true"))
						{
							PersistData.setStringData(con, AppConstant.transportFirstHeat, "Yes");
							TransportResult result = mTransportResponse.getResult(); 
							db.deleteAllTransportInfo();
								
						
							//clearing database
							
							for(TransportIslandInfo til:result.getHtga())
							{
								String isLandId = til.getIslandid();
								List<TransportInfo> transportInfoList=til.getTransportlist();
								for(TransportInfo ti:transportInfoList)
								{
									String trInfo = ti.getTransportInfo();
									String trType = ti.getTransportType();
									//Log.d("data sending to db","htga "+isLandId+" "+trType+" "+trInfo);
									TransportTableData ttd = new TransportTableData("htga", isLandId, trType, trInfo);
									db.addTransportData(ttd);
									
								}
							}
							for(TransportIslandInfo til:result.getHtgh())
							{
								String isLandId = til.getIslandid();
								List<TransportInfo> transportInfoList=til.getTransportlist();
								for(TransportInfo ti:transportInfoList)
								{
									String trInfo = ti.getTransportInfo();
									String trType = ti.getTransportType();
									//Log.d("data sending to db","htgh "+isLandId+" "+trType+" "+trInfo);
									TransportTableData ttd = new TransportTableData("htgh", isLandId, trType, trInfo);
									db.addTransportData(ttd);
									
								}
							 }
						}
	           
		                /* Back to main thread/UI*/
		                
		                getActivity().runOnUiThread(new Runnable() {
		                    @Override
		                    public void run() {	                        
		                        /* Update your UI*/
		                    	if (busyNow != null) {
		    	                    busyNow.dismis();
		    	                }
		                    	StartActivity.toActivity(con, IslandListActivity.class);
		                        
		                    }
		                });
	                }
	                
	            }
	        });
			
		
	}
	 
	 private void getWeatherData(final String url) {
			// TODO Auto-generated method stub
			 if (!NetInfo.isOnline(con)) {
					AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
						return;
					}
			 
			 	Log.e("URL : ", url);

//		       final BusyDialog busyNow = new BusyDialog(con, true,false);
//		        busyNow.show();
			 	moreProgress.setVisibility(View.VISIBLE);

			        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
			        	
			        	String response="";
						
						@Override
						public void run() {
							try {
								response=AAPBDHttpClient.post(url).body();
							}
							catch (Exception e) {
								// TODO: handle exception
								 Log.e("MYAPP", "exception", e);
//								 if(busyNow!=null){
//									 busyNow.dismis();
//								 }
								 moreProgress.setVisibility(View.GONE);
							}

							getActivity().runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									 moreProgress.setVisibility(View.GONE);
//									if(busyNow!=null){
//										 busyNow.dismis();
//									 }
									try {
					                    Log.e("Response", ">>" + new String(response));
					                    if (!TextUtils.isEmpty(new String(response))) {
					    					Gson g = new Gson();
					    					mWeatherResponse = g.fromJson(new String(response),WeatherResponse.class);
					    					
					    					if (mWeatherResponse.getStatus().equalsIgnoreCase("true")) {
					    						
//					    						for(WeatherInfo wInfo:mWeatherResponse.getResult().getWeather()){
//					    							Log.e("temp", ">>"+wInfo.getTemp_f());
//					    							
//					    						}
					    						mainWeatherViewPager.setAdapter(new WeatherPagerAdapter(con, mWeatherResponse.getResult().getWeather()));
					    						mainWeatherViewPager.setCurrentItem(0);
					    						handler = new Handler();

					    						 Update = new Runnable() {
					    							@Override
					    							public void run() {

					    								if (mWeatherResponse.getResult().getWeather() != null && mWeatherResponse.getResult().getWeather().size() > 0) {
					    									int currentImg = mainWeatherViewPager.getCurrentItem();
					    									currentImg++;
					    									if (currentImg == mWeatherResponse.getResult().getWeather().size()) {
					    										currentImg = 0;
					    									}
					    									mainWeatherViewPager.setCurrentItem(currentImg, true);
					    								}
					    							}
					    						};
					    						createSwipeTimer();
					    						isTimerRunning = true;
												
											} else {
												AlertMessage.showMessage(con, getString(R.string.app_name), mWeatherResponse.getMessage() + "");

											}
					                    }


					                } catch (final Exception e) {

					                    e.printStackTrace();
					                }
									
									
								}
							});
						}
					});
			
			
		}
	 
		private void createSwipeTimer() {
			swipeTimer = new Timer();
			swipeTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					handler.post(Update);
				}
			}, 4000, 4000);
		}

	
//	private void loadUserfulInfoInUI(){
//		View customView;
//		llUserfulInfoHolder.removeAllViews();
//		for (MoreInformationDM moreInformationDM : moreInfoList) {
//			customView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_more_info_list, llUserfulInfoHolder, false);
//			TextView txtMoreInfoName = (TextView) customView.findViewById(R.id.txtMoreInfoName);
//			txtMoreInfoName.setText(moreInformationDM.getMoreInfoName());
//			customView.setTag(moreInformationDM.getMoreInfoId());
//			
//			customView.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					int morInfoId = (Integer) v.getTag();
//					Intent intent = new Intent(context, GeneralMoreInfoDetailActivity.class);
//					intent.putExtra(AppConstant.SELECTED_MORE_INFO_ID, morInfoId);
//					startActivity(intent);
//				}
//			});
//			
//			llUserfulInfoHolder.addView(customView);
//		}
//	}
	
//	private OnMapReadyCallback mapReadyCallback = new OnMapReadyCallback() {
//		
//		@Override
//		public void onMapReady(GoogleMap googleMap) {
//			// TODO Auto-generated method stub
//			googleMap.setMyLocationEnabled(true);
//			localMap = googleMap;
//		}
//	};
	
}
