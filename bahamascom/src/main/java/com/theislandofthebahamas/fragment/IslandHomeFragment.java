package com.theislandofthebahamas.fragment;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.aapbd.utils.storage.PersistData;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.adapter.IslandHomePagerAdapter;
import com.theislandofthebahamas.datamodel.IslandDetailsList;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.util.AppConstant;
import com.theislandofthebahamas.util.CustomDurationViewPager;

public class IslandHomeFragment extends Fragment {

	ListView lstIslandList;
	Vector<IslandDetailsList> islandList;
	
	Context context, con;
	View v;
	CustomDurationViewPager islandHomeViewPager;
	LinearLayout indicator, signIn_signUp;
	DatabaseHandler db;
	boolean isTimerRunning=false;
	Handler handler;
	Runnable Update;
	Timer swipeTimer;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		 v = inflater.inflate(R.layout.island_home, container, false);
		 context = con = getActivity();
		 db=new DatabaseHandler(con);
		 initUI();
		return v;
	}

	private void initUI() {
		// TODO Auto-generated method stub
		indicator=(LinearLayout)v.findViewById(R.id.mainIndicator);
		islandHomeViewPager=(CustomDurationViewPager)v.findViewById(R.id.islandHomeViewPager);
		if(islandList!=null){
			islandList.removeAllElements();
		}
		islandList=db.getAllIslandDetailsByIslandId(PersistData.getStringData(con, AppConstant.islandID));
		islandHomeViewPager.setAdapter(new IslandHomePagerAdapter(con, islandList));
		islandHomeViewPager.setCurrentItem(0);
		controlDots(0);
		
		handler = new Handler();

		 Update = new Runnable() {
			@Override
			public void run() {

				if (islandList != null && islandList.size() > 0) {
					int currentImg = islandHomeViewPager.getCurrentItem();
					currentImg++;
					if (currentImg == islandList.size()) {
						currentImg = 0;
					}
					islandHomeViewPager.setCurrentItem(currentImg, true);
				}
			}
		};
		createSwipeTimer();
		isTimerRunning=true;
		
		islandHomeViewPager.setScrollDurationFactor(AppConstant.IMAGE_SLIDE_ANIMATION_SPEED);
		islandHomeViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int state) {

				if (state == ViewPager.SCROLL_STATE_DRAGGING) {
					isTimerRunning = false;
					swipeTimer.cancel();
					System.out.println("ViewPager: Timer stop");
				} else {
					if (!isTimerRunning) {
						createSwipeTimer();
						System.out.println("ViewPager: Timer start");
						isTimerRunning = true;
					}
				}
			}

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
				controlDots(position);
			}

		});
	}
	
	private void createSwipeTimer() {
		swipeTimer = new Timer();
		swipeTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				handler.post(Update);
			}
		}, 4000, 4000);
	}
	
	protected void controlDots(int newPageNumber) {

		try {
			indicator.removeAllViews(); // simple linear layout

			final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(

			android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT);

			layoutParams.setMargins(7, 2, 0, 2);

			final ImageView[] b = new ImageView[islandList.size()];

			// Log.e("Shop size ", ">>>" + db.getAllShoppingName().size());

			for (int i1 = 0; i1 < islandList.size(); i1++) {

				b[i1] = new ImageView(con);

				b[i1].setId(1000 + i1);

				if (newPageNumber == i1) {

					b[i1].setBackgroundResource(R.drawable.things_to_know_06);

				} else {

					b[i1].setBackgroundResource(R.drawable.things_to_know_05);

				}

				b[i1].setLayoutParams(layoutParams);

				indicator.addView(b[i1]);

			}

		} catch (final Exception e) {

		}

	}
}
