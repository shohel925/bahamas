package com.theislandofthebahamas.fragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.Executors;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aapbd.utils.network.AAPBDHttpClient;
import com.aapbd.utils.network.NetInfo;
import com.aapbd.utils.notification.AlertMessage;
import com.aapbd.utils.notification.BusyDialog;
import com.aapbd.utils.storage.PersistData;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.theislandofthebahamas.EventDetailActivity;
import com.theislandofthebahamas.MainActivity;
import com.theislandofthebahamas.R;
import com.theislandofthebahamas.SplashActivity;
import com.theislandofthebahamas.datamodel.GeneralEventInfo;
import com.theislandofthebahamas.db.DatabaseHandler;
import com.theislandofthebahamas.response.GeneralActivityList;
import com.theislandofthebahamas.response.GeneralActivityResponse;
import com.theislandofthebahamas.response.GeneralEventResponse;
import com.theislandofthebahamas.util.AllURL;
import com.theislandofthebahamas.util.AppConstant;

public class GeneralEventsFragment extends Fragment {

	
	ListView lstEventList;
	
	Vector<GeneralEventInfo> eventList;
	
	Context context, con;
	Typeface tf,tf3;
	DatabaseHandler db;
	ImageView imageviewRightArrow,imageviewLeftArrow;
	TextView textviewMonth;
	Calendar calStart,calEnd;
	Date maxDate,minDate;
	long longStartDate,longEndDate;
	int month=0;
	ProgressBar progressEvent;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fragment_general_events, container, false);
		context = con = getActivity();
		db=new DatabaseHandler(con);
		progressEvent=(ProgressBar)v.findViewById(R.id.progressEvent);
		textviewMonth=(TextView)v.findViewById(R.id.textviewMonth);
		imageviewRightArrow=(ImageView)v.findViewById(R.id.imageviewRightArrow);
		imageviewLeftArrow=(ImageView)v.findViewById(R.id.imageviewLeftArrow);
		tf = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Medium.otf");
		tf3 = Typeface.createFromAsset(con.getAssets(), "font/ufonts.com_gotham-bold.ttf");
		textviewMonth.setTypeface(tf3);
		lstEventList = (ListView)v.findViewById(R.id.lstEventList);
		lstEventList.setOnItemClickListener(eventClick);
		// ========= Get End Time in miliseceond ==============
		calEnd=Calendar.getInstance();
		int days=calEnd.getActualMaximum(Calendar.DAY_OF_MONTH);
		calEnd.set(Calendar.DAY_OF_MONTH, days); 
		maxDate=calEnd.getTime();
		longEndDate=maxDate.getTime();
		longEndDate=longEndDate/1000;
		// ========= Get Start Time in miliseceond ==============
		calStart=Calendar.getInstance();
		calStart.set(Calendar.DAY_OF_MONTH, calStart.getActualMinimum(Calendar.DAY_OF_MONTH));
		minDate=calStart.getTime();
		longStartDate=minDate.getTime();
		longStartDate=longStartDate/1000;
		
		month=calEnd.get(Calendar.MONTH);
		SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
		String month_name = month_date.format(calStart.getTime());
		textviewMonth.setText(month_name.toUpperCase());
		
		if(NetInfo.isOnline(con)){
			getEventData(AllURL.getEventListUrl());
		}else{
			setDataInListview();
		}
		
		imageviewLeftArrow.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(month>0){
						month=month-1;
						int days=calEnd.getActualMaximum(Calendar.DAY_OF_MONTH);
						calEnd.set(Calendar.DAY_OF_MONTH, days); 
						calEnd.set(Calendar.MONTH, month);
						maxDate=calEnd.getTime();
						longEndDate=maxDate.getTime();
						longEndDate=longEndDate/1000;
						
						calStart.set(Calendar.MONTH, month);
						calStart.set(Calendar.DAY_OF_MONTH, calStart.getActualMinimum(Calendar.DAY_OF_MONTH));
						minDate=calStart.getTime();
						longStartDate=minDate.getTime();
						longStartDate=longStartDate/1000;
						//=== Set Month =====
						SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
						String month_name = month_date.format(calStart.getTime());
						textviewMonth.setText(month_name.toUpperCase());
						setDataInListview();
						
					}
					
				}
			});
		
		imageviewRightArrow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(month<11){
					month=month+1;
					int days=calEnd.getActualMaximum(Calendar.DAY_OF_MONTH);
					calEnd.set(Calendar.DAY_OF_MONTH, days); 
					calEnd.set(Calendar.MONTH, month);
					maxDate=calEnd.getTime();
					longEndDate=maxDate.getTime();
					longEndDate=longEndDate/1000;
					
					calStart.set(Calendar.MONTH, month);
					calStart.set(Calendar.DAY_OF_MONTH, calStart.getActualMinimum(Calendar.DAY_OF_MONTH));
					minDate=calStart.getTime();
					longStartDate=minDate.getTime();
					longStartDate=longStartDate/1000;
					
					//=== Set Month =====
					SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
					String month_name = month_date.format(calStart.getTime());
					textviewMonth.setText(month_name.toUpperCase());
					
					setDataInListview();
				}
				
			}
		});
		
	
		
		return v;
	}
	 protected void setDataInListview() {
		// TODO Auto-generated method stub
			if(eventList!=null){
				eventList.removeAllElements();
			}
		 	eventList=db.getEventListFromStartAndEndDateInMonth(longStartDate, longEndDate);
			Log.e("event SIze", ">>"+eventList.size());
			lstEventList.setAdapter(new EventAdapter());
		
	}
//	protected void getEventData(final String url) {
//		 
//		 if (!NetInfo.isOnline(con)) {
//				AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
//					return;
//				}
//
//	        Log.e("URL : ", url);
//
//	      final BusyDialog busyNow = new BusyDialog(con, true,false);
//	        busyNow.show();
//
//	        final AsyncHttpClient client = new AsyncHttpClient();
//	        client.post(url, new AsyncHttpResponseHandler() {
//
//	            @Override
//	            public void onStart() {
//	                // called before request is started
//	            }
//
//	            @Override
//	            public void onSuccess(int statusCode, Header[] headers,
//	                                  byte[] response) {
//	                // called when response HTTP status is "200 OK"
//	                if (busyNow != null) {
//	                    busyNow.dismis();
//	                }
//
//	                try {
//	                    Log.e("Response", ">>" + new String(response));
//	                    if (!TextUtils.isEmpty(new String(response))) {
//	    					Gson g = new Gson();
//	    					final GeneralEventResponse mGeneralHotelResponse = g.fromJson(new String(response),GeneralEventResponse.class);
//	    					
//	    					if (mGeneralHotelResponse.getStatus().equalsIgnoreCase("true")) {
//								for(GeneralEventInfo hlist:mGeneralHotelResponse.getResult()){
//									if(!db.ifEventExist(hlist.getId())){
//										String phoneNo="";
//										if(hlist.getPhone().size()>0){
//											phoneNo=AppConstant.convertArrayToString(hlist.getPhone());
//										}
//										db.addGeneralEvent(hlist,phoneNo);
//									}
//								
//								}
//								
//								setDataInListview();
//								
//								
//							} else {
//								AlertMessage.showMessage(con, getString(R.string.app_name), mGeneralHotelResponse.getMessage() + "");
//
//							}
//	                    }
//
//
//	                } catch (final Exception e) {
//
//	                    e.printStackTrace();
//	                }
//
//	            }
//
//	            @Override
//	            public void onFailure(int statusCode, Header[] headers,
//	                                  byte[] errorResponse, Throwable e) {
//	                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
//
//	                if (busyNow != null) {
//	                    busyNow.dismis();
//	                }
//	            }
//
//	            @Override
//	            public void onRetry(int retryNo) {
//	                // called when request is retried
//	            }
//
//			
//	        });
//	    }
	
	 private void getEventData(final String url) {
			// TODO Auto-generated method stub
			 if (!NetInfo.isOnline(con)) {
					AlertMessage.showMessage(con, getString(R.string.app_name),getString(R.string.NoInternet));
						return;
					}
			 
			 	Log.e("URL : ", url);

//		        final BusyDialog busyNow = new BusyDialog(con, true,false);
//		        busyNow.show();
			 	progressEvent.setVisibility(View.VISIBLE);

			        Executors.newSingleThreadScheduledExecutor().submit(new Runnable() {
			        	
			        	String response="";
						
						@Override
						public void run() {
							try {
								response=AAPBDHttpClient.post(url).body();
							}
							catch (Exception e) {
								// TODO: handle exception
								 Log.e("MYAPP", "exception", e);
								 progressEvent.setVisibility(View.GONE);
//								 if(busyNow!=null){
//									 busyNow.dismis();
//								 }
							}

							getActivity().runOnUiThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									 progressEvent.setVisibility(View.GONE);
//									if(busyNow!=null){
//										 busyNow.dismis();
//									 }
									try {
					                    Log.e("Response", ">>" + new String(response));
					                    if (!TextUtils.isEmpty(new String(response))) {
					    					Gson g = new Gson();
					    					final GeneralEventResponse mGeneralHotelResponse = g.fromJson(new String(response),GeneralEventResponse.class);
					    					
					    					if (mGeneralHotelResponse.getStatus().equalsIgnoreCase("true")) {
												for(GeneralEventInfo hlist:mGeneralHotelResponse.getResult()){
													if(!db.ifEventExist(hlist.getId())){
														String phoneNo="";
														if(hlist.getPhone().size()>0){
															phoneNo=AppConstant.convertArrayToString(hlist.getPhone());
														}
														db.addGeneralEvent(hlist,phoneNo);
													}
												
												}
												
												setDataInListview();
												
												
											} else {
												AlertMessage.showMessage(con, getString(R.string.app_name), mGeneralHotelResponse.getMessage() + "");

											}
					                    }


					                } catch (final Exception e) {

					                    e.printStackTrace();
					                }
									
									
								}
							});
						}
					});
			
			
		}
	private class EventAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return eventList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return eventList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null){
				convertView = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_event_list, parent, false);
			}
			GeneralEventInfo query=eventList.get(position);
			
			TextView txtEventName = (TextView)convertView.findViewById(R.id.txtEventName);
			TextView txtEventrowFrom = (TextView)convertView.findViewById(R.id.txtEventrowFrom);
			
			txtEventName.setText(query.getName());
			if(query.getYearly().trim().equalsIgnoreCase("true")){
				txtEventrowFrom.setText(AppConstant.getDaysFromDateAndTime(query.getStartdate())+" / "+getString(R.string.Allyearround));	
			}else{
				if(AppConstant.checkHours(query.getStartdate(),query.getEnddate())>24){
					txtEventrowFrom.setText(AppConstant.dateConvert(query.getStartdate())+" - "+AppConstant.dateConvertWithYear(query.getEnddate()));		
				}else{
					txtEventrowFrom.setText(""+AppConstant.dateConvertWithYear(query.getStartdate()));		
				}
				
			}
			
			txtEventName.setTypeface(tf);
			txtEventrowFrom.setTypeface(tf);
			
			return convertView;
		}
	}
	
	

	
	private OnItemClickListener eventClick = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			GeneralEventInfo query=eventList.get(position);
			AppConstant.mGeneralEventInfo=query;
			Intent intent = new Intent(context, EventDetailActivity.class);
			startActivity(intent);
		}
	};
	
}
