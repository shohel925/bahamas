package com.theislandofthebahamas;

import java.util.ArrayList;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.theislandofthebahamas.datamodel.ActivityDM;
import com.theislandofthebahamas.datamodel.EventDM;
import com.theislandofthebahamas.datamodel.HotelDM;
import com.theislandofthebahamas.datamodel.IslandDM;
import com.theislandofthebahamas.datamodel.MoreInfoDetailDM;
import com.theislandofthebahamas.datamodel.MoreInformationDM;
import com.theislandofthebahamas.datamodel.holder.AllActivity;
import com.theislandofthebahamas.datamodel.holder.AllEvents;
import com.theislandofthebahamas.datamodel.holder.AllHotels;
import com.theislandofthebahamas.datamodel.holder.AllIslands;
import com.theislandofthebahamas.datamodel.holder.AllMoreInfo;
import com.theislandofthebahamas.fragment.GeneralActivitiesFragment;
import com.theislandofthebahamas.fragment.GeneralEventsFragment;
import com.theislandofthebahamas.fragment.GeneralHomeFragment;
import com.theislandofthebahamas.fragment.GeneralHotelsFragment;
import com.theislandofthebahamas.fragment.GeneralMoreFragment;
import com.theislandofthebahamas.fragment.IslandActivitiesFragment;
import com.theislandofthebahamas.fragment.IslandEventsFragment;
import com.theislandofthebahamas.fragment.IslandHomeFragment;
import com.theislandofthebahamas.fragment.IslandHotelsFragment;
import com.theislandofthebahamas.fragment.IslandMoreFragment;
import com.theislandofthebahamas.util.AppConstant;

public class IslandMainActivity extends Activity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the sections. We use a {@link FragmentPagerAdapter} derivative,
	 * which will keep every loaded fragment in memory. If this becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v13.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	Context context, con;

	// Fragment list
	IslandHomeFragment islandHomeFragment;
	IslandActivitiesFragment mIslandActivitiesFragment;
	IslandHotelsFragment mIslandHotelsFragment;
	IslandEventsFragment mIslandEventsFragment;
	IslandMoreFragment mIslandMoreFragment;

	Button btnOpenHome;
	Button btnOpenActivities;
	Button btnOpenHotels;
	Button btnOpenEvents;
	Button btnOpenMore;

	LatLng[] latLngs = new LatLng[20];
	Typeface tf2;
	public ImageView islandMap;

	
	 public static IslandMainActivity instance;

	 public static IslandMainActivity getInstance() {
	  return instance;
	 }
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = con = this;
		instance=this;
		updateCustomActionBar();

		loadDummyData();

		btnOpenHome = (Button) findViewById(R.id.btnOpenHome);
		btnOpenActivities = (Button) findViewById(R.id.btnOpenActivities);
		btnOpenHotels = (Button) findViewById(R.id.btnOpenHotels);
		btnOpenEvents = (Button) findViewById(R.id.btnOpenEvents);
		btnOpenMore = (Button) findViewById(R.id.btnOpenMore);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.islandMainPager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOnPageChangeListener(mainPageChangeListener);

		// Demo
		btnOpenHome.setBackgroundResource(R.drawable.events_yellow_bg);
		btnOpenActivities.setBackgroundResource(0x0);
		btnOpenHotels.setBackgroundResource(0x0);
		btnOpenEvents.setBackgroundResource(0x0);
		btnOpenMore.setBackgroundResource(0x0);
		tf2 = Typeface.createFromAsset(con.getAssets(), "font/AvenirNextLTPro-Demi.otf");
		btnOpenHome.setTypeface(tf2);
		btnOpenActivities.setTypeface(tf2);
		btnOpenHotels.setTypeface(tf2);
		btnOpenEvents.setTypeface(tf2);
		btnOpenMore.setTypeface(tf2);
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.main, menu); return true; }
	 * 
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { // Handle action bar item clicks here. The action bar will // automatically handle clicks
	 * on the Home/Up button, so long // as you specify a parent activity in AndroidManifest.xml. int id = item.getItemId(); if (id == R.id.action_settings) {
	 * return true; } return super.onOptionsItemSelected(item); }
	 */

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).

			switch (position) {
			case 0:
				if (islandHomeFragment == null) {
					islandHomeFragment = new IslandHomeFragment();
				}
				return islandHomeFragment;
			case 1:
				if (mIslandActivitiesFragment == null) {
					mIslandActivitiesFragment = new IslandActivitiesFragment();
				}
				return mIslandActivitiesFragment;
			case 2:
				if (mIslandHotelsFragment == null) {
					mIslandHotelsFragment = new IslandHotelsFragment();
				}
				return mIslandHotelsFragment;
			case 3:
				if (mIslandEventsFragment == null) {
					mIslandEventsFragment = new IslandEventsFragment();
				}
				return mIslandEventsFragment;
			case 4:
				if (mIslandMoreFragment == null) {
					mIslandMoreFragment = new IslandMoreFragment();
				}
				return mIslandMoreFragment;

			default:
				return null;
			}
			// return PlaceholderFragment.newInstance(position + 1);

		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 5;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			case 3:
				return "a";
			case 4:
				return "b";
			}
			return null;
		}
	}

	public void openMainHome(View v) {
		mViewPager.setCurrentItem(0);
	}

	public void openMainActivities(View v) {
		mViewPager.setCurrentItem(1);
	}

	public void openMainHotels(View v) {
		mViewPager.setCurrentItem(2);
	}

	public void openMainEvents(View v) {
		mViewPager.setCurrentItem(3);
	}

	public void openMainMore(View v) {
		mViewPager.setCurrentItem(4);
	}

	private OnPageChangeListener mainPageChangeListener = new OnPageChangeListener() {

		@Override
		public void onPageSelected(int position) {
			// TODO Auto-generated method stub

			switch (position) {
			case 0:
				btnOpenHome.setBackgroundResource(R.drawable.events_yellow_bg);
				btnOpenActivities.setBackgroundResource(0x0);
				btnOpenHotels.setBackgroundResource(0x0);
				btnOpenEvents.setBackgroundResource(0x0);
				btnOpenMore.setBackgroundResource(0x0);
				break;
			case 1:
				btnOpenHome.setBackgroundResource(0x0);
				btnOpenActivities.setBackgroundResource(R.drawable.events_red_bg);
				btnOpenHotels.setBackgroundResource(0x0);
				btnOpenEvents.setBackgroundResource(0x0);
				btnOpenMore.setBackgroundResource(0x0);
				break;
			case 2:
				btnOpenHome.setBackgroundResource(0x0);
				btnOpenActivities.setBackgroundResource(0x0);
				btnOpenHotels.setBackgroundResource(R.drawable.events_green_bg);
				btnOpenEvents.setBackgroundResource(0x0);
				btnOpenMore.setBackgroundResource(0x0);
				break;
			case 3:
				btnOpenHome.setBackgroundResource(0x0);
				btnOpenActivities.setBackgroundResource(0x0);
				btnOpenHotels.setBackgroundResource(0x0);
				btnOpenEvents.setBackgroundResource(R.drawable.events_bg);
				btnOpenMore.setBackgroundResource(0x0);
				break;
			case 4:
				btnOpenHome.setBackgroundResource(0x0);
				btnOpenActivities.setBackgroundResource(0x0);
				btnOpenHotels.setBackgroundResource(0x0);
				btnOpenEvents.setBackgroundResource(0x0);
				btnOpenMore.setBackgroundResource(R.drawable.events_brick_bg);
				break;
			default:
				btnOpenHome.setBackgroundResource(0x0);
				btnOpenActivities.setBackgroundResource(0x0);
				btnOpenHotels.setBackgroundResource(0x0);
				btnOpenEvents.setBackgroundResource(0x0);
				btnOpenMore.setBackgroundResource(0x0);
				break;
			}
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}
	};

	private void updateCustomActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.ab_island_main_home);
		ImageView islandBack = (ImageView) actionBar.getCustomView().findViewById(R.id.islandBack);
		islandMap = (ImageView) actionBar.getCustomView().findViewById(R.id.islandMap);
		TextView islandTv = (TextView) actionBar.getCustomView().findViewById(R.id.islandTv);
		islandTv.setText(AppConstant.mIslandList.getName());
		islandTv.setTextColor(Color.parseColor(AppConstant.islandColor));
		islandTv.setTypeface(tf2);
		ImageView islandSearch = (ImageView) actionBar.getCustomView().findViewById(R.id.islandSearch);

		islandBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		islandSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(context, "Will open search option", Toast.LENGTH_SHORT).show();
			}
		});
		

	}

	private void loadDummyData() {
		ArrayList<IslandDM> islandList = new ArrayList<IslandDM>();
		IslandDM island;

		ArrayList<ActivityDM> activityList = new ArrayList<ActivityDM>();
		ActivityDM activity;

		ArrayList<HotelDM> hotelList = new ArrayList<HotelDM>();
		HotelDM hotel;

		ArrayList<EventDM> eventList = new ArrayList<EventDM>();
		EventDM event;

		int[] islandIds = { 1, 2, 3, 4, 5 };
		loadDemoLatLng();

		for (int i = 0; i < 10; i++) {
			island = new IslandDM();
			island.setIslandId(i);
			island.setIslandName("Island: " + i);
			island.setIslandPicId(R.drawable.demo_island_pic);
			island.setIslandLogoId(R.drawable.demo_island_icon);
			islandList.add(island);

			activity = new ActivityDM();
			activity.setActivityId(i);
			activity.setActivityName("Activity: " + i);
			activity.setActivityPicId(R.drawable.demo_beach_pro_pic2);
			activity.setActivityDesc("When it comes to exploring nature, The Bahamas has plenty of offer. We play host to some of the most intriguing, exotic and mysterious natural phenomena on the planet. Here you'll find the world's deepest blue hole, the third-largest fringing barrier reef int eh world, miles of pink-sand beaches, the world's largest colony of pink flamingos, the endangered Bahama Parrot, the usual Andros iguana, a wild horse preserve and countless nature preserves. You'll also find a growing number of eco-friendly hotels and resorts in The Bahamas offering vacation packages that support ecotourism and ecotravelers.");
			activity.setIslandIds(islandIds);
			activityList.add(activity);

			hotel = new HotelDM();
			hotel.setHotelId(i);
			hotel.setHotelName("Hotel Name: " + i);
			hotel.setHotelAddress("Hotel location in: " + i);
			hotel.setHotelPicId(R.drawable.demo_hotel);
			hotel.setHotelAddressDetail("Hotel location in detail: " + i + " " + i);
			hotel.setPhoneNumber("04982342544");
			hotel.setWebAddress("http://www.google.com");
			hotel.setLat(latLngs[i % latLngs.length].latitude);
			hotel.setLng(latLngs[i % latLngs.length].longitude);
			hotelList.add(hotel);

			event = new EventDM();
			event.setEventId(i);
			event.setEventName("Event name: " + i);
			event.setEventStartDate("Thu, July 9, 2015");
			event.setEventEndDate("Sat, July 11, 2015");
			event.setEventDescription("Traditional Bahamian wooden sloops are featured at this event, and the highlight will be the Class A, B, & C boat race. Venue is the Regatta Village in Morgan's Bluff, North Andros, which is 20 miles from the San Andros Airport.");
			event.setPhoneNumber("(242)329-2278");
			eventList.add(event);

		}

		AllIslands.setIslandList(islandList);
		AllActivity.setActivityList(activityList);
		AllHotels.setAllHotels(hotelList);
		AllEvents.setAllEvents(eventList);
		loadMoreInfoList();
	}

	private void loadMoreInfoList() {
		ArrayList<MoreInformationDM> moreInfoList = new ArrayList<MoreInformationDM>();
		ArrayList<MoreInfoDetailDM> moreInfoDetails = new ArrayList<MoreInfoDetailDM>();
		MoreInfoDetailDM moreInfoDetail = new MoreInfoDetailDM();
		moreInfoDetail.setMoreInfoDetailId(1);
		moreInfoDetail.setMoreInfoTitle("Remembering our past");
		moreInfoDetail
				.setMoreInfoDescription("Even the most experienced explorers has gotten lost in our abundant natural beauty. For centuries, out islands captivated settlers, traders and invaders, while our shipping cahnnel enchanted pirates who quickly discovered all of our great hiding places. To this day, there are still tales of treasure. However, the real treasure is our people. Bahamians may live for today, but we never forget our past.");
		moreInfoDetails.add(moreInfoDetail);
		moreInfoDetail = new MoreInfoDetailDM();
		moreInfoDetail.setMoreInfoDetailId(2);
		moreInfoDetail.setMoreInfoTitle("Remembering our past");
		moreInfoDetail
				.setMoreInfoDescription("Even the most experienced explorers has gotten lost in our abundant natural beauty. For centuries, out islands captivated settlers, traders and invaders, while our shipping cahnnel enchanted pirates who quickly discovered all of our great hiding places. To this day, there are still tales of treasure. However, the real treasure is our people. Bahamians may live for today, but we never forget our past.");
		moreInfoDetails.add(moreInfoDetail);
		moreInfoDetail = new MoreInfoDetailDM();
		moreInfoDetail.setMoreInfoDetailId(3);
		moreInfoDetail.setMoreInfoTitle("Remembering our past");
		moreInfoDetail
				.setMoreInfoDescription("Even the most experienced explorers has gotten lost in our abundant natural beauty. For centuries, out islands captivated settlers, traders and invaders, while our shipping cahnnel enchanted pirates who quickly discovered all of our great hiding places. To this day, there are still tales of treasure. However, the real treasure is our people. Bahamians may live for today, but we never forget our past.");
		moreInfoDetails.add(moreInfoDetail);
		moreInfoDetail = new MoreInfoDetailDM();
		moreInfoDetail.setMoreInfoDetailId(4);
		moreInfoDetail.setMoreInfoTitle("Remembering our past");
		moreInfoDetail
				.setMoreInfoDescription("Even the most experienced explorers has gotten lost in our abundant natural beauty. For centuries, out islands captivated settlers, traders and invaders, while our shipping cahnnel enchanted pirates who quickly discovered all of our great hiding places. To this day, there are still tales of treasure. However, the real treasure is our people. Bahamians may live for today, but we never forget our past.");
		moreInfoDetails.add(moreInfoDetail);
		moreInfoDetail = new MoreInfoDetailDM();
		moreInfoDetail.setMoreInfoDetailId(5);
		moreInfoDetail.setMoreInfoTitle("Remembering our past");
		moreInfoDetail
				.setMoreInfoDescription("Even the most experienced explorers has gotten lost in our abundant natural beauty. For centuries, out islands captivated settlers, traders and invaders, while our shipping cahnnel enchanted pirates who quickly discovered all of our great hiding places. To this day, there are still tales of treasure. However, the real treasure is our people. Bahamians may live for today, but we never forget our past.");
		moreInfoDetails.add(moreInfoDetail);
		moreInfoDetail = new MoreInfoDetailDM();
		moreInfoDetail.setMoreInfoDetailId(6);
		moreInfoDetail.setMoreInfoTitle("Remembering our past");
		moreInfoDetail
				.setMoreInfoDescription("Even the most experienced explorers has gotten lost in our abundant natural beauty. For centuries, out islands captivated settlers, traders and invaders, while our shipping cahnnel enchanted pirates who quickly discovered all of our great hiding places. To this day, there are still tales of treasure. However, the real treasure is our people. Bahamians may live for today, but we never forget our past.");
		moreInfoDetails.add(moreInfoDetail);
		moreInfoDetail = new MoreInfoDetailDM();
		moreInfoDetail.setMoreInfoDetailId(7);
		moreInfoDetail.setMoreInfoTitle("Remembering our past");
		moreInfoDetail
				.setMoreInfoDescription("Even the most experienced explorers has gotten lost in our abundant natural beauty. For centuries, out islands captivated settlers, traders and invaders, while our shipping cahnnel enchanted pirates who quickly discovered all of our great hiding places. To this day, there are still tales of treasure. However, the real treasure is our people. Bahamians may live for today, but we never forget our past.");
		moreInfoDetails.add(moreInfoDetail);
		moreInfoDetail = new MoreInfoDetailDM();
		moreInfoDetail.setMoreInfoDetailId(8);
		moreInfoDetail.setMoreInfoTitle("Remembering our past");
		moreInfoDetail
				.setMoreInfoDescription("Even the most experienced explorers has gotten lost in our abundant natural beauty. For centuries, out islands captivated settlers, traders and invaders, while our shipping cahnnel enchanted pirates who quickly discovered all of our great hiding places. To this day, there are still tales of treasure. However, the real treasure is our people. Bahamians may live for today, but we never forget our past.");
		moreInfoDetails.add(moreInfoDetail);
		moreInfoDetail = new MoreInfoDetailDM();
		moreInfoDetail.setMoreInfoDetailId(9);
		moreInfoDetail.setMoreInfoTitle("Remembering our past");
		moreInfoDetail
				.setMoreInfoDescription("Even the most experienced explorers has gotten lost in our abundant natural beauty. For centuries, out islands captivated settlers, traders and invaders, while our shipping cahnnel enchanted pirates who quickly discovered all of our great hiding places. To this day, there are still tales of treasure. However, the real treasure is our people. Bahamians may live for today, but we never forget our past.");
		moreInfoDetails.add(moreInfoDetail);
		moreInfoDetail = new MoreInfoDetailDM();
		moreInfoDetail.setMoreInfoDetailId(10);
		moreInfoDetail.setMoreInfoTitle("Remembering our past");
		moreInfoDetail
				.setMoreInfoDescription("Even the most experienced explorers has gotten lost in our abundant natural beauty. For centuries, out islands captivated settlers, traders and invaders, while our shipping cahnnel enchanted pirates who quickly discovered all of our great hiding places. To this day, there are still tales of treasure. However, the real treasure is our people. Bahamians may live for today, but we never forget our past.");
		moreInfoDetails.add(moreInfoDetail);
		MoreInformationDM moreInfo = new MoreInformationDM();
		moreInfo.setMoreInfoId(1);
		moreInfo.setMoreInfoName(getResources().getString(R.string.str_more_info_government));
		moreInfo.setMoreInfoDetailList(moreInfoDetails);
		moreInfo.setMoreInfoPicId(R.drawable.demo_more_detail_pic);
		moreInfoList.add(moreInfo);
		moreInfo = new MoreInformationDM();
		moreInfo.setMoreInfoId(2);
		moreInfo.setMoreInfoName(getResources().getString(R.string.str_more_info_history));
		moreInfo.setMoreInfoDetailList(moreInfoDetails);
		moreInfo.setMoreInfoPicId(R.drawable.demo_more_detail_pic);
		moreInfoList.add(moreInfo);
		moreInfo = new MoreInformationDM();
		moreInfo.setMoreInfoId(3);
		moreInfo.setMoreInfoName(getResources().getString(R.string.str_more_info_holidays));
		moreInfo.setMoreInfoDetailList(moreInfoDetails);
		moreInfo.setMoreInfoPicId(R.drawable.demo_more_detail_pic);
		moreInfoList.add(moreInfo);
		moreInfo = new MoreInformationDM();
		moreInfo.setMoreInfoId(4);
		moreInfo.setMoreInfoName(getResources().getString(R.string.str_more_info_local_custome));
		moreInfo.setMoreInfoDetailList(moreInfoDetails);
		moreInfo.setMoreInfoPicId(R.drawable.demo_more_detail_pic);
		moreInfoList.add(moreInfo);
		moreInfo = new MoreInformationDM();
		moreInfo.setMoreInfoId(5);
		moreInfo.setMoreInfoName(getResources().getString(R.string.str_more_info_language));
		moreInfo.setMoreInfoDetailList(moreInfoDetails);
		moreInfo.setMoreInfoPicId(R.drawable.demo_more_detail_pic);
		moreInfoList.add(moreInfo);
		moreInfo = new MoreInformationDM();
		moreInfo.setMoreInfoId(6);
		moreInfo.setMoreInfoName(getResources().getString(R.string.str_more_info_people));
		moreInfo.setMoreInfoDetailList(moreInfoDetails);
		moreInfo.setMoreInfoPicId(R.drawable.demo_more_detail_pic);
		moreInfoList.add(moreInfo);
		moreInfo = new MoreInformationDM();
		moreInfo.setMoreInfoId(7);
		moreInfo.setMoreInfoName(getResources().getString(R.string.str_more_info_proximity));
		moreInfo.setMoreInfoDetailList(moreInfoDetails);
		moreInfo.setMoreInfoPicId(R.drawable.demo_more_detail_pic);
		moreInfoList.add(moreInfo);
		moreInfo = new MoreInformationDM();
		moreInfo.setMoreInfoId(8);
		moreInfo.setMoreInfoName(getResources().getString(R.string.str_more_info_travel_tips));
		moreInfo.setMoreInfoDetailList(moreInfoDetails);
		moreInfo.setMoreInfoPicId(R.drawable.demo_more_detail_pic);
		moreInfoList.add(moreInfo);

		AllMoreInfo.setMoreInfoList(moreInfoList);
	}

	private void loadDemoLatLng() {
		LatLng tempLt = new LatLng(24.659621, -77.921609);
		latLngs[0] = tempLt;
		tempLt = new LatLng(24.902441, -78.127602);
		latLngs[1] = tempLt;
		tempLt = new LatLng(24.650260, -78.286217);
		latLngs[2] = tempLt;
		tempLt = new LatLng(24.145694, -77.603784);
		latLngs[3] = tempLt;
		tempLt = new LatLng(25.074441, -77.342687);
		latLngs[4] = tempLt;
		tempLt = new LatLng(25.065423, -77.381397);
		latLngs[5] = tempLt;
		tempLt = new LatLng(25.031831, -77.359510);
		latLngs[6] = tempLt;
		tempLt = new LatLng(25.036342, -77.266641);
		latLngs[7] = tempLt;
		tempLt = new LatLng(25.087268, -77.231536);
		latLngs[8] = tempLt;
		tempLt = new LatLng(25.107749, -77.149568);
		latLngs[9] = tempLt;
		tempLt = new LatLng(24.815482, -76.328314);
		latLngs[10] = tempLt;

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mViewPager.getCurrentItem() == 0) {
			super.onBackPressed();
		}
		else {
			mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
		}
	}
}
