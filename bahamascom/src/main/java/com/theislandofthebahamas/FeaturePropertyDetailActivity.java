//package com.theislandofthebahamas;
//
//import java.util.Arrays;
//import java.util.List;
//
//import android.app.ActionBar;
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.net.Uri;
//import android.os.Bundle;
//import android.text.Html;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.aapbd.utils.nagivation.StartActivity;
//import com.aapbd.utils.storage.PersistData;
//import com.squareup.picasso.Picasso;
//import com.theislandofthebahamas.util.AppConstant;
//import com.theislandofthebahamas.util.BaseURL;
//
//public class FeaturePropertyDetailActivity extends Activity {
//
//	
//	Context context, con;
//	ImageView imageviewTop;
//	Button buttonCall,buttonShareFB;
//	Typeface medium,gotham,demiBold;
//	TextView directoryDetails;
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.feature_property_detail);
//		context = con = this;
//		updateCustomActionbar();
//		initUi();	
//		
//	}
//	
//	private void initUi() {
//		// TODO Auto-generated method stub
//		directoryDetails=(TextView)findViewById(R.id.directoryDetails);
//		buttonShareFB=(Button)findViewById(R.id.buttonShareFB);
//		buttonCall=(Button)findViewById(R.id.buttonCall);
//		imageviewTop=(ImageView)findViewById(R.id.imageviewTop);
//		if((AppConstant.mFeaturePropertyInfo.getPhone().trim().equals("-"))||(AppConstant.mFeaturePropertyInfo.getPhone().trim().equals("false"))){
//			buttonCall.setVisibility(View.GONE);
//		}else{
//			buttonCall.setVisibility(View.VISIBLE);
//			buttonCall.setText(getString(R.string.call)+AppConstant.mFeaturePropertyInfo.getPhone());
//		}
//		if(!TextUtils.isEmpty(AppConstant.mFeaturePropertyInfo.getImage())){
//			imageviewTop.setVisibility(View.VISIBLE);
//			Picasso.with(context).load(AppConstant.mFeaturePropertyInfo.getImage()).into(imageviewTop);
//		}else{
//			imageviewTop.setVisibility(View.GONE);
//		}
//		
//		buttonCall.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Intent call = new Intent(Intent.ACTION_DIAL);
//				  call.setData(Uri.parse("tel:" + AppConstant.mFeaturePropertyInfo.getPhone()));
//				  startActivity(call);
//			}
//		});
//		
//		medium = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Medium.otf");
//		gotham = Typeface.createFromAsset(getAssets(), "font/ufonts.com_gotham-bold.ttf");
//		demiBold = Typeface.createFromAsset(getAssets(), "font/AvenirNextLTPro-Demi.otf");
//		buttonCall.setTypeface(demiBold);
//		buttonShareFB.setTypeface(demiBold);
//		directoryDetails.setTypeface(medium);
//		directoryDetails.setText(AppConstant.mFeaturePropertyInfo.getAddress());
//	}
//
//	private void updateCustomActionbar(){
//		ActionBar actionBar = getActionBar();
//		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//		actionBar.setCustomView(R.layout.ab_hotel_detail);
//		
//		Button btnCloseHotelDetail = (Button)actionBar.getCustomView().findViewById(R.id.btnCloseHotelDetail);
//		Button btnEditHotelInfo = (Button)actionBar.getCustomView().findViewById(R.id.btnEditHotelInfo);
//		TextView txtHotelName = (TextView)actionBar.getCustomView().findViewById(R.id.txtHotelName);
//		btnEditHotelInfo.setVisibility(View.GONE);
//		
//		try{
//			txtHotelName.setText(AppConstant.mFeaturePropertyInfo.getTitle());
//			txtHotelName.setTypeface(gotham);
//		}
//		catch(Exception ex){
//			ex.printStackTrace();
//		}
//		
//		btnCloseHotelDetail.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				finish();
//			}
//		});
//		
////		btnEditHotelInfo.setOnClickListener(new OnClickListener() {
////			
////			@Override
////			public void onClick(View v) {
////				// TODO Auto-generated method stub
////				AppConstant.islandOrDirectoryId=AppConstant.mDivingDirectoryInfo.getId();
////				AppConstant.objectType="directory";
////				StartActivity.toActivity(con, ReportActivity.class);
////			}
////		});
//		
//	}
//	
//	public void visitHotelWebsite(View v){
//		//Load web address
//		Intent intent = new Intent(Intent.ACTION_VIEW);
//		intent.setData(Uri.parse("https://google.com"));
//		startActivity(intent);
//	}
//	
//	public void shareHotelOnFB(View v){
//		Toast.makeText(context, "Share hotel on fb", Toast.LENGTH_SHORT).show();
//	}
//	
//}
