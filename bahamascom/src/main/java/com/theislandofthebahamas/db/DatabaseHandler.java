package com.theislandofthebahamas.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.theislandofthebahamas.datamodel.CategoryDirectoryInfo;
import com.theislandofthebahamas.datamodel.DivingDirectoryInfo;
import com.theislandofthebahamas.datamodel.FeaturePropertyInfo;
import com.theislandofthebahamas.datamodel.GeneralActivityIslandInfo;
import com.theislandofthebahamas.datamodel.GeneralActivityTypesOfActivityInfo;
import com.theislandofthebahamas.datamodel.GeneralActvityThinksToKnowInfo;
import com.theislandofthebahamas.datamodel.GeneralEventInfo;
import com.theislandofthebahamas.datamodel.GeneralHotelInfo;
import com.theislandofthebahamas.datamodel.IslandDetailsList;
import com.theislandofthebahamas.datamodel.IslandImageInfo;
import com.theislandofthebahamas.datamodel.IslandList;
import com.theislandofthebahamas.datamodel.SubcategoryInfo;
import com.theislandofthebahamas.datamodel.TransportTableData;
import com.theislandofthebahamas.response.GeneralActivityList;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "BahamasDB";

	// allChannel table name
	private static final String TABLE_Island = "Island";
	private static final String TABLE_Island_Image = "Island_Image";
	private static final String TABLE_Island_Details = "Island_Details";
	private static final String TABLE_General_Activity = "General_Activity";
	private static final String TABLE_General_Activity_Feature_island = "General_Activity_Feature_island";
	private static final String TABLE_General_Activity_Things_to_Know = "General_Activity_Things_to_Know";
	private static final String TABLE_General_Activity_types_of_activity = "General_Activity_types_of_activity";
	private static final String TABLE_General_Activity_Hotel = "General_Activity_Hotel";
	private static final String TABLE_General_Event = "general_Event";
	private static final String TABLE_General_More = "general_More";
	private static final String TABLE_Category = "Category";
	private static final String TABLE_Category_directory = "Category_directory";
	// DIVING DIRECTORY Table Name
	private static final String TABLE_DIVING_DIR = "DivingDirectoryDB";
	private static final String TABLE_featureProperty = "featureProperty";
	// Transport Table Name and key
	
	private static final String TABLE_TRANSPORT = "transport";
	// ========= for Public Channel ==========================

	private static final String KEY_id = "id";
	private static final String KEY_name = "name";
	private static final String KEY_image_url = "image_url";
	private static final String KEY_logo = "logo";
	private static final String KEY_description = "description";
	private static final String KEY_discover_more = "discover_more";
	private static final String KEY_discover_more_main_image = "discover_more_main_image";
	private static final String KEY_serialid = " serialid";
	private static final String KEY_type = " type";
	private static final String KEY_islandDetaisId = " islandDetaisId";
	private static final String KEY_title = " title";
	private static final String KEY_Activit_realId = " Activit_realId";
	private static final String KEY_image_title = " image_title";
	private static final String KEY_island_id = "island_id";
	private static final String KEY_island_name = "island_name";
	private static final String KEY_island_activity_id = "island_activity_id";
	private static final String KEY_island_activity_title = "island_activity_title";
	private static final String KEY_island_activity_image = "island_activity_image";
	private static final String KEY_island_activity_description = "island_activity_description";
	private static final String KEY_thinks_to_know_Id = "thinks_to_know_Id";
	private static final String KEY_types_of_activity_Id = "types_of_activity_Id";
	private static final String KEY_address = "address";
	private static final String KEY_phone = "phone";
	private static final String KEY_lat = "lat";
	private static final String KEY_lon = "lon";
	private static final String KEY_isFeature = "isFeature";
	private static final String KEY_startDate = "startDate";
	private static final String KEY_endDate = "endDate";
	private static final String KEY_yearly = "yearly";
	private static final String KEY_islandNo = "KEY_islandNo";


	// For Diving Directory Table
	private static final String KEY_dd_id = "dd_id";
	private static final String KEY_dd_name = "dd_name";
	private static final String KEY_dd_description = "dd_description";
	private static final String KEY_dd_address = "dd_address";
	private static final String KEY_dd_image_url = "dd_image_url";
	// private static final String KEY_dd_activity_ids= "dd_activity_ids";
	private static final String KEY_dd_area = "dd_area";
	private static final String KEY_dd_city = "dd_city";
	private static final String KEY_dd_lat = "dd_lat";
	private static final String KEY_dd_lon = "dd_lon";
	private static final String KEY_dd_email = "dd_email";
	private static final String KEY_dd_fax = "dd_fax";
	private static final String KEY_dd_phone = "dd_phone";
	private static final String KEY_dd_postal_code = "dd_postal_code";
	private static final String KEY_dd_primary_contact = "dd_primary_contact";
	private static final String KEY_dd_rates = "dd_rates";
	private static final String KEY_dd_website = "dd_website";
	private static final String KEY_dd_islandid = "islandid";
	private static final String KEY_parentId = "parentId";


	
	private static final String KEY_trans_keyId = "transport_id";
	private static final String KEY_trans_spilt_type = "spilt_type";
	private static final String KEY_trans_island_id = "island_id";
	private static final String KEY_trans_transport_type = "transport_type";
	private static final String KEY_trans_transport_info = "transport_info";
	private static final String KEY_categoryId = "categoryId";
	private static final String KEY_directoryId = "directoryId";
	private static final String KEY_activityId = "activityId";
	private static final String KEY_islandId = "islandId";
	private SQLiteDatabase db = null;

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	final String CREATE_featureProperty_TABLE = "CREATE TABLE "
			+ TABLE_featureProperty + "(" + KEY_activityId + " INTEGER  , " + KEY_islandId + " INTEGER  , " + KEY_id
			+ " INTEGER  , "+ KEY_title + " TEXT , " + KEY_address
			+ " TEXT , " + KEY_phone + " TEXT , " + KEY_image_url
			+ " TEXT " + ")";
	
	final String CREATE_Category_TABLE = "CREATE TABLE "
			+ TABLE_Category + "(" + KEY_id + " INTEGER , "
			+ KEY_parentId + " INTEGER , " + KEY_title + " TEXT " + ")";
	
	final String CREATE_Category__Directory_TABLE = "CREATE TABLE "
			+ TABLE_Category_directory + "(" + KEY_directoryId + " INTEGER , "
			+ KEY_categoryId + " INTEGER " + ")";
	
	final String CREATE_General_EVENT_TABLE = "CREATE TABLE "
			+ TABLE_General_Event + "(" + KEY_id + " INTEGER PRIMARY KEY , "
			+ KEY_name + " TEXT , " + KEY_description + " TEXT , "
			+ KEY_island_id + " TEXT , " + KEY_isFeature + " TEXT , "
			+ KEY_startDate + " INTEGER , " + KEY_endDate + " INTEGER , "
			+ KEY_yearly + " TEXT , " + KEY_phone + " TEXT " + ")";



	final String CREATE_General_HOTEL_TABLE = "CREATE TABLE "
			+ TABLE_General_Activity_Hotel + "(" + KEY_id
			+ " INTEGER PRIMARY KEY , " + KEY_name + " TEXT , " + KEY_image_url
			+ " TEXT , " + KEY_address + " TEXT , " + KEY_phone + " TEXT , "
			+ KEY_lat + " TEXT , " + KEY_lon + " TEXT , " + KEY_island_id
			+ " TEXT , " + KEY_isFeature + " TEXT " + ")";

	final String CREATE_ISLAND_TABLE = "CREATE TABLE " + TABLE_Island + "("
			+ KEY_id + " INTEGER PRIMARY KEY , " + KEY_name + " TEXT , "
			+ KEY_image_url + " TEXT , " + KEY_logo + " TEXT , "
			+ KEY_description + " TEXT , " + KEY_discover_more + " TEXT , "
			+ KEY_discover_more_main_image + " TEXT , " + KEY_islandNo
			+ " INTEGER " + ")";

	final String CREATE_ISLAND_IMAGE_TABLE = "CREATE TABLE "
			+ TABLE_Island_Image + "(" + KEY_id + " INTEGER  , "
			+ KEY_image_url + " TEXT , " + KEY_serialid + " INTEGER , "
			+ KEY_type + " TEXT " + ")";

	final String CREATE_ISLAND_DETAILS_TABLE = "CREATE TABLE "
			+ TABLE_Island_Details + "(" + KEY_id + " INTEGER  , "
			+ KEY_islandDetaisId + " TEXT , " + KEY_title + " TEXT , "
			+ KEY_description + " TEXT , " + KEY_image_url + " TEXT , "
			+ KEY_serialid + " INTEGER " + ")";

	final String CREATE_General_Activity_TABLE = "CREATE TABLE "
			+ TABLE_General_Activity + "(" + KEY_id + " INTEGER , "
			+ KEY_name + " TEXT , " + KEY_image_url + " TEXT , "
			+ KEY_image_title + " TEXT , " + KEY_description + " TEXT , "
			+ KEY_Activit_realId + " TEXT " + ")";

	final String CREATE_General_Activity_Feature_Island_TABLE = "CREATE TABLE "
			+ TABLE_General_Activity_Feature_island + "(" + KEY_id
			+ " INTEGER , " + KEY_island_id + " TEXT , " + KEY_island_name
			+ " TEXT , " + KEY_island_activity_id + " TEXT , "
			+ KEY_island_activity_title + " TEXT , "
			+ KEY_island_activity_image + " TEXT , "
			+ KEY_island_activity_description + " TEXT " + ")";

	final String CREATE_General_Activity_Thinks_To_Know_TABLE = "CREATE TABLE "
			+ TABLE_General_Activity_Things_to_Know + "(" + KEY_id
			+ " INTEGER  , " + KEY_thinks_to_know_Id + " TEXT , " + KEY_title
			+ " TEXT , " + KEY_description + " TEXT , " + KEY_image_url
			+ " TEXT " + ")";

	final String CREATE_General_Activity_types_of_activity_TABLE = "CREATE TABLE "
			+ TABLE_General_Activity_types_of_activity
			+ "("
			+ KEY_id
			+ " INTEGER  , "
			+ KEY_types_of_activity_Id
			+ " TEXT , "
			+ KEY_title
			+ " TEXT , "
			+ KEY_description
			+ " TEXT , "
			+ KEY_image_url + " TEXT " + ")";

	// Creating String for DIVING DIRECTORY SQL
	final String CREATE_DIVING_DIRECTORY_TABLE = "CREATE TABLE "
			+ TABLE_DIVING_DIR + " ( " + KEY_dd_id + " INTEGER , "
			+ KEY_dd_name + " TEXT , " + KEY_dd_description + " TEXT , "
			+ KEY_dd_address + " TEXT , " + KEY_dd_image_url + " TEXT , "
			+ KEY_dd_area + " TEXT , " + KEY_dd_city + " TEXT , " + KEY_dd_lat
			+ " TEXT , " + KEY_dd_lon + " TEXT , " + KEY_dd_email + " TEXT , "
			+ KEY_dd_fax + " TEXT , " + KEY_dd_phone + " TEXT , "
			+ KEY_dd_postal_code + " TEXT , " + KEY_dd_primary_contact
			+ " TEXT , " + KEY_dd_rates + " TEXT , " + KEY_dd_website
			+ " TEXT , " + KEY_dd_islandid + " TEXT ); ";
	//Transport table SQL create
		final String CREATE_TRANPORT_TABLE = "CREATE TABLE "
				+ TABLE_TRANSPORT + "(" + KEY_trans_keyId + " INTEGER PRIMARY KEY , "+ KEY_trans_spilt_type + " TEXT , "
				+ KEY_trans_island_id + " TEXT , " + KEY_trans_transport_type + " TEXT , "
				+ KEY_trans_transport_info + " TEXT " + ")";
	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(CREATE_ISLAND_TABLE);
		db.execSQL(CREATE_ISLAND_IMAGE_TABLE);
		db.execSQL(CREATE_ISLAND_DETAILS_TABLE);
		db.execSQL(CREATE_General_Activity_TABLE);
		db.execSQL(CREATE_General_Activity_Feature_Island_TABLE);
		db.execSQL(CREATE_General_Activity_Thinks_To_Know_TABLE);
		db.execSQL(CREATE_General_Activity_types_of_activity_TABLE);
		db.execSQL(CREATE_General_HOTEL_TABLE);
		db.execSQL(CREATE_General_EVENT_TABLE);
		db.execSQL(CREATE_TRANPORT_TABLE);
		db.execSQL(CREATE_Category_TABLE);
		db.execSQL(CREATE_DIVING_DIRECTORY_TABLE);
		db.execSQL(CREATE_Category__Directory_TABLE);
		db.execSQL(CREATE_featureProperty_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Island);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Island_Image);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Island_Details);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Activity);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Activity_Feature_island);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Activity_Things_to_Know);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Activity_types_of_activity);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Activity_Hotel);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_General_Event);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DIVING_DIR);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSPORT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Category);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_Category_directory);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_featureProperty);
		// Create tables again
		onCreate(db);
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */
	
	public void addFeatureProperty(FeaturePropertyInfo generralInfo,String activityId,String islandId) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_activityId, activityId);
		values.put(KEY_islandId, islandId);
		values.put(KEY_id, generralInfo.getId());
		values.put(KEY_title, generralInfo.getTitle());
		values.put(KEY_address, generralInfo.getAddress());
		values.put(KEY_phone, generralInfo.getPhone());
		values.put(KEY_image_url, generralInfo.getImage());
		// Inserting Row
		db.insert(TABLE_featureProperty, null, values);
		db.close(); // Closing database connection
	}
	
	public Vector<FeaturePropertyInfo> getAllFeatureProperty(String activityId,String islandId) {
		final Vector<FeaturePropertyInfo> mIslandList = new Vector<FeaturePropertyInfo>();
		mIslandList.removeAllElements();
		final String selectQuery = "SELECT * FROM " + TABLE_featureProperty+" WHERE "+KEY_activityId+"="+activityId+" AND "+KEY_islandId+"="+islandId;

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				FeaturePropertyInfo pInfo = new FeaturePropertyInfo();
				pInfo.setActivityId(cursor.getString(0));
				pInfo.setIslandId(cursor.getString(1));
				pInfo.setId(cursor.getString(2));
				pInfo.setTitle(cursor.getString(3));
				pInfo.setAddress(cursor.getString(4));
				pInfo.setPhone(cursor.getString(5));
				pInfo.setImage(cursor.getString(6));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}
		return mIslandList;
	}
	
	public void addCategoryDirectory(String directoryId,String categoryId) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_directoryId, directoryId);
		values.put(KEY_categoryId, categoryId);
		// Inserting Row
		db.insert(TABLE_Category_directory, null, values);
		db.close(); // Closing database connection
	}
	
	public Vector<CategoryDirectoryInfo> getCategoryDirectoryList() {
		final Vector<CategoryDirectoryInfo> mIslandList = new Vector<CategoryDirectoryInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Category_directory;
		Log.e("selectQuery", ">> "+selectQuery);

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				CategoryDirectoryInfo pInfo = new CategoryDirectoryInfo();
				pInfo.setDirectoryId(cursor.getString(0));
				pInfo.setCategoryId(cursor.getString(1));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}
	
	public void addCategory(String categoryId,String parentId,String title) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, categoryId);
		values.put(KEY_parentId, parentId);
		values.put(KEY_title, title);
		// Inserting Row
		db.insert(TABLE_Category, null, values);
		db.close(); // Closing database connection
	}
	
	public Vector<SubcategoryInfo> getCategoryListFromParentId(String activityId) {
		final Vector<SubcategoryInfo> mIslandList = new Vector<SubcategoryInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Category+" WHERE "+KEY_parentId+"='"+activityId+"'";
		Log.e("selectQuery", ">> "+selectQuery);

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				SubcategoryInfo pInfo = new SubcategoryInfo();
				pInfo.setId(cursor.getString(0));
				pInfo.setParentId(cursor.getString(1));
				pInfo.setSubcategoryname(cursor.getString(2));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}
	
	public String getCategoryIdFromTitle(String title) {
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Category+" WHERE "+KEY_title+"='"+title+"'";
		Log.e("selectQuery", ">> "+selectQuery);

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);
		String id = "";
		if (cursor.moveToFirst()) {
			
			id=cursor.getString(0);
		}
		return id;
	}

	public void addGeneralEvent(GeneralEventInfo generralInfo,String phoneNo) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, generralInfo.getId());
		values.put(KEY_name, generralInfo.getName());
		values.put(KEY_description, generralInfo.getDescription());
		values.put(KEY_island_id, generralInfo.getIslandid());
		values.put(KEY_isFeature, generralInfo.getIsfeature());
		values.put(KEY_startDate, generralInfo.getStartdate());
		values.put(KEY_endDate, generralInfo.getEnddate());
		values.put(KEY_yearly, generralInfo.getYearly());
		values.put(KEY_phone, phoneNo);
		// Inserting Row
		db.insert(TABLE_General_Event, null, values);
		db.close(); // Closing database connection
	}

	public Vector<GeneralEventInfo> getIslandEventListFromIslandIdAndDate(long startDate,long endDate,String islandId) {
		final Vector<GeneralEventInfo> mIslandList = new Vector<GeneralEventInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_General_Event
				+ " WHERE " + KEY_startDate + " >=" + startDate + " AND "+KEY_endDate+"<=" +endDate+" AND " + KEY_island_id + "='" + islandId + "'";
		Log.e("selectQuery", ">> "+selectQuery);

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GeneralEventInfo pInfo = new GeneralEventInfo();
				pInfo.setId(cursor.getString(0));
				pInfo.setName(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setIslandid(cursor.getString(3));
				pInfo.setIsfeature(cursor.getString(4));
				pInfo.setStartdate(cursor.getString(5));
				pInfo.setEnddate(cursor.getString(6));
				pInfo.setYearly(cursor.getString(7));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}
	
	public Vector<GeneralEventInfo> getEventListFromStartAndEndDateInMonth(long startDate,long endDate) {
		final Vector<GeneralEventInfo> mIslandList = new Vector<GeneralEventInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_General_Event
				+ " WHERE " + KEY_startDate + ">=" + startDate + " AND "+KEY_endDate+"<=" +endDate;
		Log.e("selectQuery", ">> "+selectQuery);

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GeneralEventInfo pInfo = new GeneralEventInfo();
				pInfo.setId(cursor.getString(0));
				pInfo.setName(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setIslandid(cursor.getString(3));
				pInfo.setIsfeature(cursor.getString(4));
				pInfo.setStartdate(cursor.getString(5));
				pInfo.setEnddate(cursor.getString(6));
				pInfo.setYearly(cursor.getString(7));
				pInfo.setPhoneStr(cursor.getString(8));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}

	public void addGeneralHotel(GeneralHotelInfo generralInfo) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, "" + generralInfo.getId());
		values.put(KEY_name, "" + generralInfo.getName());
		values.put(KEY_image_url, "" + generralInfo.getImage_url());
		values.put(KEY_address, "" + generralInfo.getAddress());
		values.put(KEY_phone, "" + generralInfo.getPhone());
		values.put(KEY_lat, "" + generralInfo.getLat());
		values.put(KEY_lon, "" + generralInfo.getLon());
		values.put(KEY_island_id, "" + generralInfo.getIslandid());
		values.put(KEY_isFeature, "" + generralInfo.getIsfeature());
		// Inserting Row
		db.insert(TABLE_General_Activity_Hotel, null, values);
		db.close(); // Closing database connection
	}

	public Vector<GeneralHotelInfo> getHotelListFromFeature(String feature) {
		final Vector<GeneralHotelInfo> mIslandList = new Vector<GeneralHotelInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM "
				+ TABLE_General_Activity_Hotel + " WHERE " + KEY_isFeature
				+ " = '" + feature + "'";
		Log.e("selectQuery", ">>"+selectQuery);
		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GeneralHotelInfo pInfo = new GeneralHotelInfo();
				pInfo.setId(cursor.getString(0));
				pInfo.setName(cursor.getString(1));
				pInfo.setImage_url(cursor.getString(2));
				pInfo.setAddress(cursor.getString(3));
				pInfo.setPhone(cursor.getString(4));
				pInfo.setLat(cursor.getString(5));
				pInfo.setLon(cursor.getString(6));
				pInfo.setIslandid(cursor.getString(7));
				pInfo.setIsfeature(cursor.getString(8));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}
	
	public Vector<GeneralHotelInfo> getIslandHotelListFromIslandId(String islandId) {
		final Vector<GeneralHotelInfo> mIslandList = new Vector<GeneralHotelInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM "
				+ TABLE_General_Activity_Hotel + " WHERE " + KEY_island_id
				+ " ='" + islandId + "'";
		Log.e("selectQuery", ">>"+selectQuery);

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GeneralHotelInfo pInfo = new GeneralHotelInfo();
				pInfo.setId(cursor.getString(0));
				pInfo.setName(cursor.getString(1));
				pInfo.setImage_url(cursor.getString(2));
				pInfo.setAddress(cursor.getString(3));
				pInfo.setPhone(cursor.getString(4));
				pInfo.setLat(cursor.getString(5));
				pInfo.setLon(cursor.getString(6));
				pInfo.setIslandid(cursor.getString(7));
				pInfo.setIsfeature(cursor.getString(8));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}

	public void addThinksToKnow(GeneralActvityThinksToKnowInfo detaislInfo) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, detaislInfo.getActivityId());
		values.put(KEY_thinks_to_know_Id, detaislInfo.getId());
		values.put(KEY_title, detaislInfo.getTitle());
		values.put(KEY_description, detaislInfo.getDescription());
		values.put(KEY_image_url, detaislInfo.getImage_url());
		// Inserting Row
		db.insert(TABLE_General_Activity_Things_to_Know, null, values);
		db.close(); // Closing database connection
	}

	public Vector<GeneralActvityThinksToKnowInfo> getThinksToKnowListFromActivityId(
			String activityId) {
		final Vector<GeneralActvityThinksToKnowInfo> mIslandList = new Vector<GeneralActvityThinksToKnowInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM "
				+ TABLE_General_Activity_Things_to_Know + " WHERE " + KEY_id
				+ " = '" + activityId + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GeneralActvityThinksToKnowInfo pInfo = new GeneralActvityThinksToKnowInfo();
				pInfo.setActivityId(cursor.getString(0));
				pInfo.setId(cursor.getString(1));
				pInfo.setTitle(cursor.getString(2));
				pInfo.setDescription(cursor.getString(3));
				pInfo.setImage_url(cursor.getString(4));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}

	public void addTypesOfActivity(
			GeneralActivityTypesOfActivityInfo detaislInfo) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, detaislInfo.getActivityId());
		values.put(KEY_types_of_activity_Id, detaislInfo.getId());
		values.put(KEY_title, detaislInfo.getTitle());
		values.put(KEY_description, detaislInfo.getDescription());
		values.put(KEY_image_url, detaislInfo.getImage());
		// Inserting Row
		db.insert(TABLE_General_Activity_types_of_activity, null, values);
		db.close(); // Closing database connection
	}

	public Vector<GeneralActivityTypesOfActivityInfo> getTypesOfActivityListFromActivityId(
			String activityId) {
		final Vector<GeneralActivityTypesOfActivityInfo> mIslandList = new Vector<GeneralActivityTypesOfActivityInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM "
				+ TABLE_General_Activity_types_of_activity + " WHERE " + KEY_id
				+ " = '" + activityId + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GeneralActivityTypesOfActivityInfo pInfo = new GeneralActivityTypesOfActivityInfo();
				pInfo.setActivityId(cursor.getString(0));
				pInfo.setId(cursor.getString(1));
				pInfo.setTitle(cursor.getString(2));
				pInfo.setDescription(cursor.getString(3));
				pInfo.setImage(cursor.getString(4));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}

	public void addGeneralActivityFeatureIsland(GeneralActivityIslandInfo generralInfo) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, generralInfo.getActivityId());
		values.put(KEY_island_id, generralInfo.getIsland_id());
		values.put(KEY_island_name, generralInfo.getIsland_name());
		values.put(KEY_island_activity_id, generralInfo.getIsland_activity_id());
		values.put(KEY_island_activity_title,generralInfo.getIsland_activity_title());
		values.put(KEY_island_activity_image,generralInfo.getIsland_activity_image());
		values.put(KEY_island_activity_description,generralInfo.getIsland_activity_description());
		// Inserting Row
		db.insert(TABLE_General_Activity_Feature_island, null, values);
		db.close(); // Closing database connection
	}

	public Vector<GeneralActivityIslandInfo> getFeactureListFromActivityId(
			String activityId) {
		final Vector<GeneralActivityIslandInfo> mIslandList = new Vector<GeneralActivityIslandInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM "
				+ TABLE_General_Activity_Feature_island + " WHERE " + KEY_id
				+ " = '" + activityId + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GeneralActivityIslandInfo pInfo = new GeneralActivityIslandInfo();
				pInfo.setActivityId(cursor.getString(0));
				pInfo.setIsland_id(cursor.getString(1));
				pInfo.setIsland_name(cursor.getString(2));
				pInfo.setIsland_activity_id(cursor.getString(3));
				pInfo.setIsland_activity_title(cursor.getString(4));
				pInfo.setIsland_activity_image(cursor.getString(5));
				pInfo.setIsland_activity_description(cursor.getString(6));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}
	
	public Vector<GeneralActivityIslandInfo> getFeactureListFromActivityAndIslandId(String activityId,String islndId) {
		final Vector<GeneralActivityIslandInfo> mIslandList = new Vector<GeneralActivityIslandInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM "
				+ TABLE_General_Activity_Feature_island + " WHERE " + KEY_id
				+ " = '" + activityId + "' AND "+KEY_island_id+"='"+islndId+"'";
		Log.e("selectQuery", ">>"+selectQuery);

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GeneralActivityIslandInfo pInfo = new GeneralActivityIslandInfo();
				pInfo.setActivityId(cursor.getString(0));
				pInfo.setIsland_id(cursor.getString(1));
				pInfo.setIsland_name(cursor.getString(2));
				pInfo.setIsland_activity_id(cursor.getString(3));
				pInfo.setIsland_activity_title(cursor.getString(4));
				pInfo.setIsland_activity_image(cursor.getString(5));
				pInfo.setIsland_activity_description(cursor.getString(6));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}

	public void addGeneralActivity(GeneralActivityList generralInfo) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, generralInfo.getId());
		values.put(KEY_name, generralInfo.getName());
		values.put(KEY_image_url, generralInfo.getImage_url());
		values.put(KEY_image_title, generralInfo.getImage_title());
		values.put(KEY_description, generralInfo.getDescription());
		// Inserting Row
		db.insert(TABLE_General_Activity, null, values);
		db.close(); // Closing database connection
	}

	public void addIsland(IslandList pInfo) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, pInfo.getId());
		values.put(KEY_name, pInfo.getName());
		values.put(KEY_image_url, pInfo.getImage_url());
		values.put(KEY_logo, pInfo.getLogo());
		values.put(KEY_description, pInfo.getDescription());
		values.put(KEY_discover_more, pInfo.getDiscover_more());
		values.put(KEY_discover_more_main_image,
				pInfo.getDiscover_more_main_image());
		values.put(KEY_islandNo, pInfo.getIslandNo());
		// Inserting Row
		db.insert(TABLE_Island, null, values);
		db.close(); // Closing database connection
	}

	public void addIslandImage(String islandId, String imageUrl,
			Integer serialId, String type) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, islandId);
		values.put(KEY_image_url, imageUrl);
		values.put(KEY_serialid, serialId);
		values.put(KEY_type, type);
		// Inserting Row
		db.insert(TABLE_Island_Image, null, values);
		db.close(); // Closing database connection
	}

	public void addIslandDetails(IslandDetailsList detaislInfo) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_id, detaislInfo.getIslandId());
		values.put(KEY_islandDetaisId, detaislInfo.getId());
		values.put(KEY_title, detaislInfo.getTitle());
		values.put(KEY_description, detaislInfo.getDescription());
		values.put(KEY_image_url, detaislInfo.getImage_url());
		values.put(KEY_serialid, detaislInfo.getSerialid());
		// Inserting Row
		db.insert(TABLE_Island_Details, null, values);
		db.close(); // Closing database connection
	}

	public Vector<GeneralActivityList> getAllGeneralActivity() {
		final Vector<GeneralActivityList> mIslandList = new Vector<GeneralActivityList>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_General_Activity;

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				GeneralActivityList pInfo = new GeneralActivityList();
				pInfo.setId(cursor.getString(0));
				pInfo.setName(cursor.getString(1));
				pInfo.setImage_url(cursor.getString(2));
				pInfo.setImage_title(cursor.getString(3));
				pInfo.setDescription(cursor.getString(4));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}

	public Vector<IslandList> getAllIsland() {
		final Vector<IslandList> mIslandList = new Vector<IslandList>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT  * FROM " + TABLE_Island
				+ " order By " + KEY_islandNo + " ASC";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				IslandList pInfo = new IslandList();
				pInfo.setId(cursor.getString(0));
				pInfo.setName(cursor.getString(1));
				pInfo.setImage_url(cursor.getString(2));
				pInfo.setLogo(cursor.getString(3));
				pInfo.setDescription(cursor.getString(4));
				pInfo.setDiscover_more(cursor.getString(5));
				pInfo.setDiscover_more_main_image(cursor.getString(6));
				pInfo.setIslandNo(Integer.parseInt(cursor.getString(7)));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}
	
	public Vector<IslandList> getIsland(String islandId) {
		final Vector<IslandList> mIslandList = new Vector<IslandList>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT  * FROM " + TABLE_Island
				+ " WHERE " + KEY_id + "="+islandId;

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				IslandList pInfo = new IslandList();
				pInfo.setId(cursor.getString(0));
				pInfo.setName(cursor.getString(1));
				pInfo.setImage_url(cursor.getString(2));
				pInfo.setLogo(cursor.getString(3));
				pInfo.setDescription(cursor.getString(4));
				pInfo.setDiscover_more(cursor.getString(5));
				pInfo.setDiscover_more_main_image(cursor.getString(6));
				pInfo.setIslandNo(Integer.parseInt(cursor.getString(7)));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}

	public Vector<IslandImageInfo> getAllIslandImage(String islandOrActivtyId,
			String type) {
		final Vector<IslandImageInfo> mIslandList = new Vector<IslandImageInfo>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Island_Image
				+ " WHERE " + KEY_id + " = '" + islandOrActivtyId + "' AND "
				+ KEY_type + " = '" + type + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				IslandImageInfo pInfo = new IslandImageInfo();
				pInfo.setId(cursor.getString(0));
				pInfo.setImageUrl(cursor.getString(1));
				pInfo.setSerialId(cursor.getString(2));
				pInfo.setType(cursor.getString(3));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}

	public Vector<IslandDetailsList> getAllIslandDetailsByIslandId(
			String islandId) {
		final Vector<IslandDetailsList> mIslandList = new Vector<IslandDetailsList>();
		mIslandList.removeAllElements();
		// Select All Queryb
		final String selectQuery = "SELECT * FROM " + TABLE_Island_Details
				+ " WHERE " + KEY_id + " = '" + islandId + "'";

		final SQLiteDatabase db = getWritableDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				IslandDetailsList pInfo = new IslandDetailsList();
				pInfo.setIslandId(cursor.getString(0));
				pInfo.setId(cursor.getString(1));
				pInfo.setTitle(cursor.getString(2));
				pInfo.setDescription(cursor.getString(3));
				pInfo.setImage_url(cursor.getString(4));
				pInfo.setSerialid(cursor.getString(5));
				mIslandList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}

		// return contact list
		return mIslandList;
	}

	public boolean ifIslandExist(String islandId) {
		boolean isChannel = false;

		final SQLiteDatabase db = getWritableDatabase();

		final String selectQuery = " SELECT " + " * FROM "
				+ DatabaseHandler.TABLE_Island + " WHERE " + KEY_id + " = '"
				+ islandId + "'";

		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.getCount() > 0) {

			isChannel = true;

		} else {
			isChannel = false;
		}

		db.close(); // Closing database connection

		return isChannel;

	}

	public boolean ifHotelExist(String hotelId) {
		boolean isChannel = false;

		final SQLiteDatabase db = getWritableDatabase();

		final String selectQuery = " SELECT " + " * FROM "
				+ DatabaseHandler.TABLE_General_Activity_Hotel + " WHERE "
				+ KEY_id + " = '" + hotelId + "'";

		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.getCount() > 0) {

			isChannel = true;

		} else {
			isChannel = false;
		}

		db.close(); // Closing database connection

		return isChannel;

	}

	public boolean ifEventExist(String eventId) {
		boolean isChannel = false;

		final SQLiteDatabase db = getWritableDatabase();

		final String selectQuery = " SELECT " + " * FROM "
				+ DatabaseHandler.TABLE_General_Event + " WHERE " + KEY_id
				+ " = '" + eventId + "'";

		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.getCount() > 0) {

			isChannel = true;

		} else {
			isChannel = false;
		}

		db.close(); // Closing database connection

		return isChannel;

	}

	public boolean ifGeneralActivityExist(String islandId) {
		boolean isChannel = false;

		final SQLiteDatabase db = getWritableDatabase();

		final String selectQuery = " SELECT " + " * FROM "
				+ DatabaseHandler.TABLE_General_Activity + " WHERE " + KEY_id
				+ " = '" + islandId + "'";

		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.getCount() > 0) {

			isChannel = true;

		} else {
			isChannel = false;
		}

		db.close(); // Closing database connection

		return isChannel;

	}
	
	public boolean ifCategoryExist(String categoryId) {
		boolean result = false;

		openDatabase();

		final String selectQuery = " SELECT " + " * FROM "
				+ DatabaseHandler.TABLE_Category + " WHERE " + KEY_id
				+ " = '" + categoryId + "'";

		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.getCount() > 0) {

			result = true;

		} else {
			result = false;
		}

		closeDatabase();

		return result;

	}
	
	public boolean ifFeatureProperyExist(String dirId) {
		boolean result = false;

		openDatabase();

		final String selectQuery = " SELECT " + " * FROM "
				+ DatabaseHandler.TABLE_featureProperty + " WHERE " + KEY_id
				+ " = '" + dirId + "'";

		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.getCount() > 0) {

			result = true;

		} else {
			result = false;
		}

		closeDatabase();

		return result;

	}

	public boolean ifDivingDirExist(String dirId) {
		boolean result = false;

		openDatabase();

		final String selectQuery = " SELECT " + " * FROM "
				+ DatabaseHandler.TABLE_DIVING_DIR + " WHERE " + KEY_dd_id
				+ " = '" + dirId + "'";

		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.getCount() > 0) {

			result = true;

		} else {
			result = false;
		}

		closeDatabase();

		return result;

	}

	

	private void openDatabase() {
		// TODO Auto-generated method stub
		db = getWritableDatabase();

	}
	private void closeDatabase() {
		// TODO Auto-generated method stub
		if (db != null) {
			db.close();
		}// Closing database connection

	}

	public void addDivingDir(DivingDirectoryInfo dlist) {
		final SQLiteDatabase db = getWritableDatabase();

		final ContentValues values = new ContentValues();
		values.put(KEY_dd_id, dlist.getId());
		values.put(KEY_dd_name, dlist.getName());
		values.put(KEY_dd_description, dlist.getDescription());
		values.put(KEY_dd_address, dlist.getAddress());
		values.put(KEY_dd_image_url, dlist.getImage_url());
		values.put(KEY_dd_area, dlist.getArea());
		values.put(KEY_dd_city, dlist.getCity());
		values.put(KEY_dd_lat, dlist.getLat());
		values.put(KEY_dd_lon, dlist.getLon());
		values.put(KEY_dd_email, dlist.getEmail());
		values.put(KEY_dd_fax, dlist.getFax());
		values.put(KEY_dd_phone, dlist.getPhone());
		values.put(KEY_dd_postal_code, dlist.getPostal_code());
		values.put(KEY_dd_primary_contact, dlist.getPrimary_contact());
		values.put(KEY_dd_rates, dlist.getRates());
		values.put(KEY_dd_website, dlist.getWebsite());
		values.put(KEY_dd_islandid, dlist.getIslandid());

		// Inserting Row
		db.insert(TABLE_DIVING_DIR, null, values);
		//printing message in log to ensure
		//print.message("Data Inserted");
		db.close(); // Closing database connection
	}

	public Vector<DivingDirectoryInfo> getDivingDirFromDB() {

		Vector<DivingDirectoryInfo> mDivingDirDetailList = new Vector<DivingDirectoryInfo>();
		mDivingDirDetailList.removeAllElements();

		// Select All Query
		final String selectQuery = "SELECT * FROM " + TABLE_DIVING_DIR ;

		openDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				DivingDirectoryInfo pInfo = new DivingDirectoryInfo();

				pInfo.setId(cursor.getString(0));
				pInfo.setName(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setAddress(cursor.getString(3));
				pInfo.setImage_url(cursor.getString(4));
				pInfo.setArea(cursor.getString(5));
				pInfo.setCity(cursor.getString(6));
				pInfo.setLat(cursor.getString(7));
				pInfo.setLon(cursor.getString(8));
				pInfo.setEmail(cursor.getString(9));
				pInfo.setFax(cursor.getString(10));
				pInfo.setPhone(cursor.getString(11));
				pInfo.setPostal_code(cursor.getString(12));
				pInfo.setPrimary_contact(cursor.getString(13));
				pInfo.setRates(cursor.getString(14));
				pInfo.setWebsite(cursor.getString(15));
				pInfo.setIslandid(cursor.getString(16));

				mDivingDirDetailList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}
		
		closeDatabase();

		// return contact list
		return mDivingDirDetailList;
	}
	
	public Vector<DivingDirectoryInfo> getDivingListFrmAcitivityId(String activityId) {

		Vector<DivingDirectoryInfo> mDivingDirDetailList = new Vector<DivingDirectoryInfo>();
		mDivingDirDetailList.removeAllElements();
		final SQLiteDatabase db = getWritableDatabase();

		// Select All Query
		final String selectQuery = "SELECT  d.* FROM "+TABLE_DIVING_DIR+" as d,"+TABLE_Category_directory +" as cat_dir WHERE cat_dir."+KEY_categoryId+"='"+activityId+"' AND cat_dir."+KEY_directoryId+"=d."+KEY_dd_id;
//		final String selectQuery = "SELECT * FROM "+TABLE_DIVING_DIR+" INNER JOIN "
//				+TABLE_Category_directory +" ON "+TABLE_DIVING_DIR+"."+KEY_dd_id+"="+TABLE_Category_directory+"."+KEY_directoryId+" WHERE "+
//				TABLE_Category_directory+"."+KEY_categoryId+"='"+activityId+"'";
				
//		SELECT * FROM DivingDirectoryDB
//		INNER JOIN Category_directory ON 
//		DivingDirectoryDB.dd_id = Category_directory.directoryId
//		WHERE  Category_directory.categoryId = '1'
		Log.e("selectQuery", ">>"+selectQuery);

//		openDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				DivingDirectoryInfo pInfo = new DivingDirectoryInfo();

				pInfo.setId(cursor.getString(0));
				pInfo.setName(cursor.getString(1));
				pInfo.setDescription(cursor.getString(2));
				pInfo.setAddress(cursor.getString(3));
				pInfo.setImage_url(cursor.getString(4));
				pInfo.setArea(cursor.getString(5));
				pInfo.setCity(cursor.getString(6));
				pInfo.setLat(cursor.getString(7));
				pInfo.setLon(cursor.getString(8));
				pInfo.setEmail(cursor.getString(9));
				pInfo.setFax(cursor.getString(10));
				pInfo.setPhone(cursor.getString(11));
				pInfo.setPostal_code(cursor.getString(12));
				pInfo.setPrimary_contact(cursor.getString(13));
				pInfo.setRates(cursor.getString(14));
				pInfo.setWebsite(cursor.getString(15));
				pInfo.setIslandid(cursor.getString(16));

				mDivingDirDetailList.addElement(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}
		
		if (db != null) {
			db.close();
		}

		// return contact list
		return mDivingDirDetailList;
	}
	
	
	public void addTransportData(TransportTableData ttd) {
		
		openDatabase();	
		final ContentValues values = new ContentValues();
		values.put(KEY_trans_spilt_type, ttd.getSplittype());
		values.put(KEY_trans_island_id, ttd.getIslandid());
		values.put(KEY_trans_transport_type, ttd.getTransport_type());
		values.put(KEY_trans_transport_info, ttd.getTransport_info());
		
		Log.d("save>>>>>>>>>>>>>>>>",KEY_trans_island_id);
		//Inserting Row
		db.insert(TABLE_TRANSPORT, null, values);
		
		
		closeDatabase();
	}
	public List<TransportTableData> getTransportData(String type,String islandId)
	{
		List<TransportTableData> transportDataList = new ArrayList<TransportTableData>();
		
		// Select * from table where spilt_type = key 
		final String selectQuery = "SELECT * FROM " + TABLE_TRANSPORT+ " WHERE " 
		+ KEY_trans_spilt_type +" = '" +type+ "' AND "+ KEY_trans_island_id +" = '" +islandId+ "'";
		Log.d("islandId>>>>>>>>>>>>>>>>",islandId);
		openDatabase();
		final Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TransportTableData pInfo = new TransportTableData();


				pInfo.setSplittype(cursor.getString(1));
				pInfo.setIslandid(cursor.getString(2));
				pInfo.setTransport_type(cursor.getString(3));
				pInfo.setTransport_info(cursor.getString(4));

				transportDataList.add(pInfo);
				pInfo = null;
			} while (cursor.moveToNext());
		}
		
		closeDatabase();

		// return contact list
		return transportDataList;
		
	}


	 public void deleteAllTransportInfo() {
	
		 openDatabase();
		 final String deleteAll = "DELETE " + "FROM "
		 + TABLE_TRANSPORT + " WHERE "
		 + KEY_trans_keyId + ">-1";
		
		 db.execSQL(deleteAll);
		 close();
	 }
	 
//	 public void deleteAllFeatureProperty() {
//			
//		 openDatabase();
//		 final String deleteAll = "DELETE " + "FROM " + TABLE_featureProperty + " WHERE " + KEY_id + ">-1";
//		
//		 db.execSQL(deleteAll);
//		 close();
//	 }
	


}
