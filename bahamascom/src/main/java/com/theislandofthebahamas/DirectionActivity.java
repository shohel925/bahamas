package com.theislandofthebahamas;
import java.util.ArrayList;
import java.util.Vector;

import org.apache.http.Header;

import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aapbd.taxappuser.model.GeneralResponse;
import com.aapbd.taxidriver.utils.AllURL;
import com.aapbd.taxidriver.utils.AppConstant;
import com.aapbd.taxidriver.utils.GMapV2GetRouteDirection;
import com.aapbd.utils.geolocation.GPSTracker;
import com.aapbd.utils.nagivation.StartActivity;
import com.aapbd.utils.network.NetInfo;
import com.aapbd.utils.notification.AlertMessage;
import com.aapbd.utils.notification.BusyDialog;
import com.aapbd.utils.storage.PersistData;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.pkmmte.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

public class DirectionActivity extends Activity{
	
	Context con;
	private GoogleMap googleMap;
	double alatitude = 23.822258 ;
	double alongitude = 90.366797;
	Double lat, lng;
	
	ImageView ImgMassage,imgPhone;
	GeneralResponse generalResponse;
	TextView tvArrived,tvName_use,tvUserRatting,tvUserCarname,tvTelUser;
	LinearLayout infoView;
	///========== for Draw A path =====================
	LatLng fromPosition;
	LatLng toPosition;
	GMapV2GetRouteDirection v2GetRouteDirection;
	org.w3c.dom.Document document;
	CircularImageView infoImgUser;
	GPSTracker gps;
	private final Handler mHandler = new Handler();
	ImageView imgNavigate;
	 EndTripBroadcastListener endTropBroadcast;
	public static DirectionActivity instance;
	public static DirectionActivity getInstance(){
		return instance;
	}
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.arraived);
		con = this;
		instance=this;
		gps=new GPSTracker(con);
		actionBarSetup();
		 initUI();
	}

	
	private void initUI() {
		tvArrived = (TextView)findViewById(R.id.tvArrived);
		tvName_use = (TextView)findViewById(R.id.tvName_use);
		tvUserRatting = (TextView)findViewById(R.id.tvUserRatting);
		tvUserCarname = (TextView)findViewById(R.id.tvUserCarname);
		tvTelUser = (TextView)findViewById(R.id.tvTelUser);
		infoView = (LinearLayout)findViewById(R.id.infoView);		
		ImgMassage = (ImageView)findViewById(R.id.ImgMassage);
		imgPhone = (ImageView)findViewById(R.id.imgPhone);
		infoImgUser = (CircularImageView)findViewById(R.id.infoImgUser);
		imgNavigate=(ImageView)findViewById(R.id.imgNavigate);
		
		imgNavigate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loadRoutData();	
			}
		});	
				Log.e("Img URI", "image: "+PersistData.getStringData(con, AppConstant.userPicture));
				
				if(TextUtils.isEmpty(PersistData.getStringData(con, AppConstant.userPicture).trim())){
					Picasso.with(con).load(R.drawable.ic_launcher).into(infoImgUser);
				}
				else{
				Picasso.with(con).load(PersistData.getStringData(con, AppConstant.userPicture)).into(infoImgUser);
				}
		
		imgPhone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent call = new Intent(Intent.ACTION_DIAL);
				call.setData(Uri.parse("tel:" + AppConstant.user_phone));
				startActivity(call);
			}
		});
		
		ImgMassage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartActivity.toActivity(con, ChatActivity.class);
			}
		});
		
		tvArrived.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getDriverArraived(AllURL.getDriverArraivedUrl(PersistData.getStringData(con, AppConstant.driverId), PersistData.getStringData(con, AppConstant.order_id)));
			}
		});
			
		MapFragment fm = (MapFragment) getFragmentManager().findFragmentById(
				R.id.map_locationArraived);

		// Getting GoogleMap object from the fragment
		googleMap = fm.getMap();
		
		mHandler.removeCallbacks(mUpdateTimeTask);
		mHandler.postDelayed(mUpdateTimeTask, 100);
		loadRoutData();

	}
	
	final Runnable mUpdateTimeTask = new Runnable() {
		@Override
		public void run() {
			if (gps.canGetLocation()) {
				PersistData.setStringData(con, AppConstant.currentLat, ""+gps.getLatitude());
				PersistData.setStringData(con, AppConstant.currentLng, ""+gps.getLongitude());	
				Log.e("lat ", ":"+PersistData.getStringData(con, AppConstant.currentLat));
				Log.e("lng ", ":"+PersistData.getStringData(con, AppConstant.currentLng));
				loadRoutData();
				} else {	
					gps.showSettingsAlert();
				}
			
			mHandler.postDelayed(this, 30000);
		}
	};	
	
	private void loadRoutData() {
		
		fromPosition = new LatLng(Double.parseDouble(PersistData.getStringData(con, AppConstant.currentLat)),Double.parseDouble(PersistData.getStringData(con, AppConstant.currentLng)));
		toPosition = new LatLng(Double.parseDouble(PersistData.getStringData(con, AppConstant.pickup_lat)), Double.parseDouble(PersistData.getStringData(con, AppConstant.pickup_lng)));
		v2GetRouteDirection = new GMapV2GetRouteDirection();
		GetRouteTask getRoute = new GetRouteTask();
		getRoute.execute();
	}
	
	
	private void actionBarSetup() {
		// TODO Auto-generated method stub
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.actionbar_layout);

		

		TextView actionTitle = (TextView) actionBar.getCustomView()
				.findViewById(R.id.actionTitle);
		
		ImageView backImg = (ImageView) actionBar.getCustomView()
				.findViewById(R.id.backImg);
		
		 TextView tvInfo = (TextView) actionBar.getCustomView()
				.findViewById(R.id.tvInfo);
		 
		 
		 
		 tvInfo.setVisibility(View.VISIBLE);
		
		backImg.setVisibility(View.VISIBLE);
		
		actionTitle.setVisibility(View.VISIBLE);
		actionTitle.setText("ALEX");
		
		tvInfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(infoView.getVisibility()==View.GONE){
					infoView.setVisibility(View.VISIBLE);
				}else if (infoView.getVisibility()==View.VISIBLE){
					infoView.setVisibility(View.GONE);
				}
				
				
			}
		});
		
		backImg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	 //BusyDialog busyDialog;
	
	private class GetRouteTask extends AsyncTask<String, Void, String> {

		String response = "";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

//			busyDialog = new BusyDialog(con, true, false);
//			busyDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {
			document = v2GetRouteDirection.getDocument(fromPosition, toPosition, GMapV2GetRouteDirection.MODE_DRIVING);
			response = "Success";
			return response;
		}
		MarkerOptions marker1;
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			googleMap.clear();
			if (response.equalsIgnoreCase("Success")) {
				ArrayList<LatLng> directionPoint = v2GetRouteDirection.getDirection(document);
				PolylineOptions rectLine = new PolylineOptions().width(7).color(Color.BLUE);

				rectLine.add(fromPosition);
				for (int i = 0; i < directionPoint.size(); i++) {
					rectLine.add(directionPoint.get(i));
				}
				rectLine.add(toPosition);
				// Adding route on the map
				googleMap.addPolyline(rectLine);
				/*
				 * markerOptions.position(toPosition);
				 * googleMap.addMarker(markerOptions);
				 */

				final MarkerOptions marker = new MarkerOptions().position(fromPosition).title("Your are here");
				marker.draggable(true);
				marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_location_33));

				googleMap.addMarker(marker);
				marker1 = new MarkerOptions().position(toPosition).title(PersistData.getStringData(con, AppConstant.pickup_address));
				marker1.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_location_28));	
				Marker m=googleMap.addMarker(marker1);
				//m.showInfoWindow();

				Vector<LatLng> allp = new Vector<LatLng>();
				allp.addElement(fromPosition);
				allp.addElement(toPosition);
				
				LatLngBounds.Builder builder = new LatLngBounds.Builder();
				builder.include(fromPosition);
				builder.include(toPosition);
				
				LatLngBounds bounds = builder.build();
				int padding = 100;
				CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//				cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
				googleMap.animateCamera(cu);
				

			}
//			if (busyDialog != null) {
//				busyDialog.dismis();
//			}
			// showRout = false;
		}

	}
		

	public void showSelectedNumber(int type, String number) {
	    Toast.makeText(this, type + ": " + number, Toast.LENGTH_LONG).show();      
	}

	
	
	private void getDriverArraived(final String url) {

		if (!NetInfo.isOnline(con)) {
			AlertMessage.showMessage(con, getString(R.string.Status),
					getString(R.string.checkInternet));
			return;
		}
		
		final BusyDialog busyNow = new BusyDialog(con,true,false);
		busyNow.show();

		Log.e("login URL", url + "");

		final AsyncHttpClient client = new AsyncHttpClient();
		
//		
//		String credentials = Username + ":" + Password;  
//		String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);  
//		client.addHeader("Authorization", "Basic " + base64EncodedCredentials);
//		//client.addHeader("X-Auth-Token", 'api_token');
		
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {
				// called when response HTTP status is "200 OK"

				if (busyNow != null) {
					busyNow.dismis();
				}

				Log.e("Response", new String(response));

				 Gson g = new Gson();
				 generalResponse = g.fromJson(new String(response),
						 GeneralResponse.class);
				 
				
				 
				 
				 Log.e("status", ""+generalResponse.getStatus());
				

				if (generalResponse.getStatus().equalsIgnoreCase("true")) {
									 
					StartActivity.toActivity(con, BeginTripActivity.class);
					finish();

			} else {

					AlertMessage.showMessage(con, "Error", generalResponse.getMessage()+"");
					return;
				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
				// called when response HTTP status is "4XX" (eg. 401, 403, 404)

				Log.e("errorResponse", new String(errorResponse));
				if (busyNow != null) {
					busyNow.dismis();
				}
			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried

			}
		});

	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		AppConstant.isEndTripOpen = true;
		
		IntentFilter intentFilter = new IntentFilter(AppConstant.ENB_TAXI_DRIVER_BROADCAST_USER);
		intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
		endTropBroadcast = new EndTripBroadcastListener();
		registerReceiver(endTropBroadcast, intentFilter);

		
		String rating = PersistData.getStringData(con, AppConstant.userRating);
		Log.e("rating", ":" + rating);
		if (rating.equals("null") || rating.trim().isEmpty()) {
			rating = "0.0";
			tvUserRatting.setText(rating);
		}else {
			Log.e("after convert", ":" + rating.substring(0, 3));
			tvUserRatting.setText(rating.substring(0, 3));
		}
		tvName_use.setText(PersistData.getStringData(con, AppConstant.userFullName));
		tvUserCarname.setText(PersistData.getStringData(con, AppConstant.vehicle));
		tvTelUser.setText(PersistData.getStringData(con, AppConstant.user_phone));

		String imgUrl = PersistData.getStringData(con, AppConstant.userPicture);
		try {
			Log.e("Img URI", "image: " + imgUrl);

			if (TextUtils.isEmpty(imgUrl)) {
				Picasso.with(con).load(R.drawable.ic_launcher).into(infoImgUser);
			}
			else {
				Picasso.with(con).load(imgUrl).into(infoImgUser);
			}

		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		AppConstant.isEndTripOpen = false;
		
		unregisterReceiver(endTropBroadcast);
		
	}
	

	public class EndTripBroadcastListener extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String msg = intent.getStringExtra(AppConstant.GCM_MESSAGE_INTENT_USER);
			System.out.println(msg);
			
			if(!msg.isEmpty()){
				if(msg.equals("trip_end")){
					startActivity(new Intent(context, FareSummaryActivity.class));
					finish();
				}
			}
			
		}
		
	}
	
}
